#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from sanic import Sanic

from .settings import N_CORES, DEBUG, SERVER_PORT
from .web import public, prom, api, routines
from .handler import Handler

handler = Handler()


if __name__ == "__main__":
    app = Sanic(name="dicu.chat", strict_slashes=False)

    # Background tasks
    # app.add_task(handler.db.init)
    app.register_listener(handler.db.init, "main_process_start")

    app.add_task(handler.reminder_loop)
    app.add_task(handler.strings.load_everything)

    app.static("/tmp/app_tempfiles", "/tmp/app_tempfiles")
    app.blueprint(api.init(handler, handler.db))
    app.blueprint(prom.init(handler, handler.db))
    app.blueprint(public.init(handler, handler.db))
    app.blueprint(routines.init(handler, handler.db))

    app.run(host="0.0.0.0", port=SERVER_PORT or 8080, debug=DEBUG, access_log=DEBUG, workers=N_CORES)
