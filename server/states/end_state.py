#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from . import BaseState, Context, NOOP


class ENDState(BaseState):
    has_entry = False

    # @Important: This state purposely resets whole dialog
    async def process(self, context: Context):
        if self.user["context"].pop("enable_end_message", True):
            # End conversation message, no buttons
            context["message"]["text"] = self.strings["endConvo"]
            context["buttons"] = []
            self.send(context)

        # Reset the flow
        self.user["context"]["bq_state"] = 1
        # Reset QA flow
        if self.user["answers"].get("qa"):  self.user["answers"]["qa"]["curr_q"] = None
        return NOOP
