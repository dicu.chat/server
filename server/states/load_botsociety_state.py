#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from ..settings import BOTSOCIETY_API_KEY, ROOT_PATH, MAIN_BOTSOCIETY_FLOW
from . import BaseState, Context, OK, PermissionLevel
from ..utils import parse_api, save_file
from aiohttp import ClientSession
import logging


class LoadBotSocietyState(BaseState):
    has_entry = False
    # Allow only superadmins to access this.
    permission_required = PermissionLevel.MAX

    async def process(self, context: Context):
        raw_text = context["message"]["text"]
        inputs = raw_text.split()
        length = len(inputs)

        if length == 3:
            _, user_id, flow_id = raw_text.split()
            flow = MAIN_BOTSOCIETY_FLOW
            rewrite = True

        elif length == 4:
            _, user_id, flow_id, flow = raw_text.split()
            rewrite = False

        else:
            context["message"]["text"] = self.strings["genericBadAmountOfArgumentsToCommand"].format(
                # Note that we encode '<' and '>' here for html parsing.
                "/loadflow", 2, length - 1, "/loadflow &lt;user_id&gt; &lt;flow_id&gt; [&lt;flow_name&gt;]"
            )
            context["buttons"] = []
            self.send(context)
            return OK

        # Get user id and conv id for get request to the Botsociety API.
        api_url = f"https://app.botsociety.io/apisociety/2.0/npm/conversations/{flow_id}"  # @Robustness: this url is the same as in api endpoint for botsociety. Keep in sync.
        headers = {"Content-Type": "application/json", "api_key_public": BOTSOCIETY_API_KEY, "user_id": user_id}
        # Get data from api
        async with ClientSession(headers=headers) as session:
            async with session.get(api_url) as resp:
                data = await resp.json()

        if "status" in data:
            logging.error("BotSociety response (inside of 'LoadBotSocietyState'): %s ...", data["info"])

            context["message"]["text"] = self.strings["genericCommandError"].format(data["info"])
            context["buttons"] = []
            self.send(context)
            return OK

        logging.debug("Recieved valid data from botsociety using /loadflow command.")

        # Parse/Save data
        try:   save_file(parse_api(data["messages"]), flow, path=ROOT_PATH / "server" / "archive", rewrite=rewrite)
        except Exception as e:
            msg = f"Error during parsing the botsociety data: {e}"
            logging.exception(msg)

            context["message"]["text"] = self.strings["genericCommandError"].format(msg)
            context["buttons"] = []
            self.send(context)
            return OK

        self._handler.load_bots_file()

        msg = f"Loaded a flow (namespace '{flow}') successfully."
        logging.info("Command /loadflow success: '%s' ...", msg)

        context["message"]["text"] = self.strings["genericCommandSuccess"].format(msg)
        context["buttons"] = []
        self.send(context)
        return OK
