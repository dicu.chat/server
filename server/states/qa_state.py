#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from . import BaseState, Context, OK, END, GO_TO_STATE, NOOP
from ..utils import get_next, get_msg, encode
from ..settings import MAIN_BOTSOCIETY_FLOW
from typing import Optional, Union, Any
from asteval import Interpreter
from datetime import timedelta
from ..db import ServiceTypes
from ..strings import Button
from itertools import chain
from time import time
import datetime
import logging
import inspect
import copy
import re


class QAState(BaseState):
    EMBEDDED_ARGUMENTS = re.compile(r"\$\{(.+?)\}")
    interpreter = Interpreter(minimal=True)

    async def entry(self, context: Context):
        # Might be None if there is no botsociety data, so we exit the state early,
        # with default message showing available commands. Note, that the commands
        # in the default message are hardcoded. @KeepInSyncWithStringsJSON.
        if not self.bots_data:
            context["message"]["text"] = self.strings["defaultNoqaMessage"]
            context["buttons"] = []
            self.send(context)

            # No need to warn about this, because it's pretty obvious when it happens.
            logging.debug("No botsociety data is available at the moment, but QAState is accessed...")
            return NOOP

        # Prepare context data before doing anything.
        self.setup_context_data(context)

        # Shortened way to access input text, if you want read-only access.
        raw_answer = str(context["message"]["text"])

        # Get saved current question. Note, that this method is used here instead of
        # simple '.get' or '.pop', because it is trying to handle complex situations
        # like proper handling of '#partial' messages in history (returning to first
        # non-partial previous message).
        prev_q = self.get_relevant_q_and_trim_history()

        if "flow_config" not in self.user["answers"]["qa"]["stored_vars"]:
            await self.set_initial_configuration(prev_q)

        # Send 'current' question with some additional formatting applied.
        self.set_data_fmt(context, prev_q)
        # self.send(context)
        self.complex_send(context)

        # Handle commands that should run after sending the message.
        await self.run_aftermath_commands(prev_q, raw_answer, context)

        self.user["answers"]["qa"]["curr_q"] = prev_q["id"]
        self.user["answers"]["qa"]["qa_history"].append(prev_q["id"])

        # Handle special procedure #partial - recursive into this same method with the next question.
        if "partial" in prev_q["commands"]:
            context["message"]["text"] = ""
            return await self.process(context)
        return OK

    async def process(self, context: Context, back: bool = False):
        # Might be None if there is no botsociety data, so we exit the state early,
        # with default message showing available commands. Note, that the commands
        # in the default message are hardcoded. @KeepInSyncWithStringsJSON.
        if not self.bots_data:
            context["message"]["text"] = self.strings["defaultNoqaMessage"]
            context["buttons"] = []
            self.send(context)

            # No need to warn about this, because it's pretty obvious when it happens.
            logging.debug("No botsociety data is available at the moment, but QAState is accessed...")
            return NOOP

        # Prepare context data before doing anything.
        self.setup_context_data(context)

        # Shortened way to access input text, if you want read-only access.
        raw_answer = str(context["message"]["text"])

        # Get saved current question.
        prev_q = self.get_msg(self.user["answers"]["qa"]["curr_q"])

        # Parse button to have easy access to intent.
        is_truncated, truncation_size = self.is_truncated_button(raw_answer, context)
        button = self.parse_button(raw_answer, truncated=is_truncated, truncation_size=truncation_size, verify=self.get_button_keys(prev_q))

        # First, we check for the 'back' flag to be false, because when we are trying to go back, running commands from
        # the previous message is a bad idea. For example, if previous messages wants to write user input to some variable
        # through 'store' command, processing it here will trigger 'bad input' response since we replace user input with
        # an empty string when processing 'back' message. If 'back' flag is false, here we are running commands for the
        # previous message (the one which we received a response for).
        if not back and (ret_code := await self.run_commands_for_previous_message(prev_q, raw_answer, button, context)):
            return ret_code

        # Then, we handle a special case here for when 'process' is called with 'back' flag. In such case,
        # we want to treat 'prev_q' message as a new message, because we just popped it from the history
        # stack, so it needs to be processed and sent as a new one.
        #
        # Note(andrew): User input was trimmed before entering 'back' button processing, so be prepared that
        # 'button' here will always have no matches and will be empty. There must be a valid default fallback
        # for such case.
        next_q = prev_q if back else self.get_next(prev_q["id"], answer=button)

        # Finally, here we are running commands for the next message (the message that we are going to send by
        # the end of this processing code).
        if ret_code := await self.run_commands_for_next_message(next_q, prev_q, raw_answer, button, context):
            # Edge case, next question might not exist at all. This case is handled gracefuly
            # in 'run_commands_for_next_message', but here we kind of don't know about that and
            # this still has to be executed afterwards. So, here we are handling commands that
            # should run after sending the message.
            if next_q:  await self.run_aftermath_commands(next_q, raw_answer, context)

            return ret_code

        # Record the answer, if the answer is meaningful.
        if self.user["answers"]["qa"]["save_answer"] and raw_answer:
            self.user["answers"]["qa"]["qa_results"][prev_q["id"]] = raw_answer

        # Send next question
        self.set_data_fmt(context, next_q)
        # self.send(context)
        self.complex_send(context)

        self.user["answers"]["qa"]["curr_q"] = next_q["id"]
        self.user["answers"]["qa"]["qa_history"].append(next_q["id"])

        # Handle commands that should run after sending the message.
        await self.run_aftermath_commands(next_q, raw_answer, context)

        # Handle special procedure #partial - recursive into this same method with the next question.
        if "partial" in next_q["commands"]:
            # Put original user's text as current text, so input is preserved. (Maybe it will
            # be worth to copy the whole original context if there any commands that need it).
            context["message"]["text"] = raw_answer
            return await self.process(context)
        return OK

    # This method is used to setup context variables for this state, both consistent (modifying
    # user's state, where necessary), like current botsociety flow question id and history, and
    # re-setting some additional arguments for each call of 'entry' or 'process' run (e.g. bool
    # deciding whether or not user response needs to be recorded).
    def setup_context_data(self, context: Context):
        # Create qa cache storage if there was none (or force-recreate on /start).
        if "qa" not in self.user["answers"] or str(context["message"]["text"]).startswith("/start"):
            self.user["answers"]["qa"] = {
                "curr_q": None, "qa_results": {}, "qa_history": [],
                "multichoice_cache": {}, "score": 0,
                # Use consistent cache.
                "stored_vars": self.user["answers"].get("qa", {}).get("stored_vars", {}),
            }

        stored_vars = self.user["answers"]["qa"]["stored_vars"]
        # Updating default values in the namespace:
        stored_vars["back_ignore_partials_leaves"] = stored_vars.get("back_ignore_partials_leaves", {})
        stored_vars["skip_timeouts"] = stored_vars.get("skip_timeouts", {})

        # Note(andrew): Set default flow_name if the it's empty. This is done to allow having multiple
        # botsociety flows stored and accessible at any time. To switch between those flows, you just
        # need to change value of this variable in the user context from anywhere. Note though, that
        # this code is made failsafe, which makes the behavior confusing if you didn't read through it.
        # For example, if we couldn't find a flow with default name in the data, we are just setting any
        # name that we can (that exists in the data file), so the bot is still functional.  @Robustness.
        if self.user["context"].get("current_flow_name") is None:
            # We know for sure, that the flows are being stored now in updated format, as a dictionary
            # (since multiple flows in a single file are allowed), but for compatibility reasons, we
            # rewrite older flows into newer format as well. So, just in case older version of storage
            # file containing botsociety data was loaded, let's keep this assert for now, because this
            # will fail below anyway.  @Deprecated
            assert isinstance(self.bots_data, dict), "Botsociety data file was loaded incorrectly! It doesn't match expected format."

            # By default choose configured name flow name for the 'main' flow.
            flow_name = MAIN_BOTSOCIETY_FLOW

            # Default name is not in the botsociety data, that's bad! Let's just assign try assigning any key available.
            if flow_name not in self.bots_data:
                key = next(iter(self.bots_data))

                is_one = (len(self.bots_data) == 1)
                # If total flow has only one key, we can warn about it, but still choose that.
                if is_one:  logging.warning("Botsociety data contains one flow, but doesn't contain default name '%s'. Using '%s' instead ...",             flow_name, key)
                # If there is more than one key, and it's not default, we just pick first one. But report an error.
                else:       logging.error  ("Botsociety data contains multiple flows, but doesn't contain default name '%s'. Using unpacked name '%s' ...", flow_name, key)

                flow_name = key

            self.user["context"]["current_flow_name"] = flow_name

        # Find and set initial question (for current), if needed.
        # if self.user["answers"]["qa"].get("curr_q") is None:
        #     self.user["answers"]["qa"]["curr_q"] = self.get_next()["id"]

        # Always store unless this will be toggled off somewhere in the handling code.
        self.user["answers"]["qa"]["save_answer"] = True

    # Note(andrew): The problem with facebook is that it just decides to truncate your text buttons,
    #     if they are longer than certain size. To match it, we need to add checks for truncation in
    #     the button parser. This function will return data needed to decide whether we need to apply
    #     truncation or not (and what is the minimum size, meaning the shortest truncation size from
    #     all supported services).  @Deprecated @Robustness
    #                                                                                  - andrew, June 2021
    #
    # TODO: Somehow move this into facebook bridge. For example, by making 'is_truncated' a request
    #       context var, and figure out how to handle truncation for input there (e.g. storing the
    #       most recent values for each user).
    def is_truncated_button(self, text, context):  # @Deprecated
        # Note, this number is 3 characters short of a length that we return, because, when the text
        # is longer than 20 characters, facebook truncates it to 20 and prepends '...' to it.
        is_long = (len(str(text)) >= 23)
        # Here we check for all services that need truncation.
        bad_service = (context["serviceIn"] == ServiceTypes.FACEBOOK)  # @Deprecated

        if is_long and bad_service:
            return True, 20
        return False, 0

    # Used to retrieve previous messages from the history stack. Includes non-trivial checks to
    # ensure correctness when working with all kinds of edge cases (i.e. rollback from '#partial'
    # messages to the first non-partial for proper display, and more).
    def get_relevant_q_and_trim_history(self) -> dict:
        # Take first message by default. This gracefully handles case when history is empty.
        prev_q = self.get_next()

        # Note(andrew) Collect total amount of messages that we need to remove from the top of the history
        #     stack. We do this, instead of popping them in-place, because it feels much easier to reason
        #     about the algorithm searching the history stack without mutating it at the time of processing.
        #     In the end, we will just use this value to trim last 'total_removed' messages from history.
        total_removed = 0

        # Note(andrew): Take history stack from the user, skip if history is empty. We are not
        #     checking for 'None' though, so it always expected to be a list, just sometimes an
        #     empty one.
        if history := self.user["answers"]["qa"]["qa_history"]:
            # Take any valid last message on the top of the history stack. To make sure that the
            # message is indeed valid, we check for None. In case of the latter, we have to fall-
            # back and prefer a currently chosen one.
            if old_q := self.get_msg(history[-1]):
                # Note(andrew): There is an edge case here, when have additional directives applied
                #     to the messages in the '.get_next()', like 'skip_if' with a certain condition,
                #     but the condition became true after we stored messages into the history (user
                #     reached some leaf on the conversation tree that triggered change), so now we
                #     need to account for that here. If the user resets the flow history by command
                #     like '/start', the problem disappears, but that is far from typical use-case,
                #     since we have 'back' button.  @Important
                #
                #     So, we will try to deal with it here, and if it works - great.  @Robustness @Speed
                if not self.check_condition_to_skip_q(prev_q):  prev_q = old_q

                stored_vars = self.user["answers"]["qa"]["stored_vars"]
                back_ignore = stored_vars["back_ignore_partials_leaves"].pop(prev_q["id"], False)

                # Updating amount of removed messages from top of the history stack.
                total_removed += 1

                # If length of the history is bigger than one, we need to check if previous message
                # contained '#partial' command, because then it becomes important for us to jump to
                # the beginning (i.e. go back as long as previous messages contain '#partial').
                #
                # Note: Evaluation of history slice is sufficient to perform a check whether length
                #       of the history is bigger than one. We are re-using amount of total removed
                #       entries from the stack as an index, which we use to get to the neighbouring
                #       previous entry.
                while history[:-total_removed]:
                    # Since history evaluated to true, there are more messages in it. Therefore, here
                    # we are getting element of index 'total_removed - 1', meaning previous message
                    # to the one we already have in the 'prev_q'.
                    #
                    # Note: We are saving it to the 'old_q' variable instead of 'prev_q' because, at
                    #       this point, we are yet to figure out whether we want this previous entry.
                    if old_q := self.get_msg(history[-total_removed - 1]):

                        # Checking for 'partial' command in the older message, because when older message
                        # does contain it, we know we are not at the first message, so we need to go back.
                        if "partial" in old_q["commands"]:
                            # After we passed the check for 'partial' and we know it is something, we can
                            # accept this older message as a new oldest one.
                            if not self.check_condition_to_skip_q(old_q, back_ignore):  prev_q = old_q
                            total_removed += 1  # Move cursor.
                            continue

                    break

        # Finally, we actually want to trim all popped entries from history..
        self.user["answers"]["qa"]["qa_history"] = history[:-total_removed]
        # ..and return the best previous message that we decided on.

        return prev_q

    # Note(andrew): This method is a wrapper around the 'utils.get_next()' function. We use it to apply
    #     some additional constraints on the next message. For example, we want to ignore messages that
    #     are marked as 'skipped' under certain condition (provided by the designer of the flow through
    #     the frontend via directive), if that conditional expression evaluates to true.
    def get_next(self, question_id=None, **kwargs):
        flow = self.bots_data[self.user["context"]["current_flow_name"]]

        # First, get the next question from the data, and if there's no question, quit early.
        while next_q := get_next(flow, question_id, **kwargs):
            # If condition to skip question is not true, break from the loop.
            if not self.check_condition_to_skip_q(next_q):  break

            question_id = next_q["id"]

        return next_q  # Note, returned value might be 'None'.

    def check_condition_to_skip_q(self, question: dict, back_ignore_partials: bool = False):
        svars = self.user["answers"]["qa"]["stored_vars"]
        skip = False

        # Check whether #skip_if directive is present..
        if skip_args := question["commands"].get("skip_if"):
            # ..when it is, get expression from first args, so that we can execute it..
            if expr := skip_args[0].get("expr"):
                skip = self.eval_expr(expr)
            # ..alternatively, check for the timeout argument in the storage.. TODO: explain
            elif timeout := skip_args[0].get("timeout"):
                last_time = svars["skip_timeouts"].get(question["id"], 0)
                curr_time = int(time())
                if last_time + timeout < curr_time:
                    svars["skip_timeouts"][question["id"]] = curr_time
                else:
                    skip = True
            else:
                logging.warning("Directive 'skip_if' was provided without 'expr' or 'timeout' arguments, it's a noop...")

        # This is used for '#back_ignore_partials' directive functionality.
        if back_ignore_partials and "partial" in question["commands"]:
            skip = True

        return skip

    # Note(andrew): This method is a wrapper that allows you to get a message object from botsociety
    #     file, that corresponds to provided 'question_id'. If the 'question_id' was not provided, it
    #     will return initial (the first) message from the botsociety file.
    def get_msg(self, question_id: str = None, **kwargs):
        curr_flow = self.bots_data[self.user["context"]["current_flow_name"]]
        next_q_id = question_id or self.get_next()["id"]
        return get_msg(curr_flow, next_q_id)

    def get_button_keys(self, question: dict):
        extra = []

        if "no_back" not in question["commands"]:  extra.append(self.strings["back"])
        if "no_stop" not in question["commands"]:  extra.append(self.strings["stop"])
        if "no_main" not in question["commands"]:  extra.append(self.strings["mainMenu"])

        # Create an iterator for strings in the botsociety message that was passed to the method.
        normal = (self.strings[button["text_key"]] for button in question["buttons"])
        # Returning an iterator, which yields normal strings first and then extra in the end.
        return chain(normal, extra)

    # Using button's "text_key" (aka id) to find corresponding button object in the given question.
    def get_button_with_key(self, question: dict, text_key: Optional[str]):
        for btn in question["buttons"]:
            if btn["text_key"] == text_key:
                return btn
        return None

    # Note(andrew): Here we are searching through an array of buttons to figure out which one of them
    #     is one that should be treated as a special token, forcing next question.  @Speed
    def get_next_command_button_text(self, question):
        for btn in question["buttons"]:
            if "next" in btn["commands"]:
                return btn["text"]

        # This is basically an assert that the flow had an error.  @Robustness
        raise ValueError("None of the buttons have '#next' command in a multichoice question!")

    # Note(andrew): This is a wrapper around 'BaseState.set_data()', which adds our special
    #     formatting options and passed context variables from this state automatically.
    def set_data_fmt(self, context: Context, question: dict, avoid_buttons: list = None):
        stored_vars = self.user["answers"]["qa"]["stored_vars"]
        flow_config = stored_vars["flow_config"]

        back = True
        stop = True
        main = True

        # Note(andrew): we are popping values of the 'no_back' and 'no_stop' here, if present, because
        #     these values are meant to have lifespan of a single question. Keep in mind though, this
        #     works as expected right now with special cases like '#partial', because each message sets
        #     the 'no_back' and the 'no_stop' for itself *only*. But in the future, if you are implementing
        #     some other functionality, make sure to check here, because this might not be what you want.  @Robustness

        if "no_back" in question["commands"]:  back = False
        elif "default_back" in flow_config:    back = flow_config["default_back"]

        if "no_stop" in question["commands"]:  stop = False
        elif "default_stop" in flow_config:    stop = flow_config["default_stop"]

        # 'From anywhere to main menu' button.
        if "no_main" in question["commands"]:  main = False
        elif "default_main" in flow_config:    main = flow_config["default_main"]

        self.set_data(context, question, avoid_buttons, back_button=back, stop_button=stop, main_button=main)

        # Extra formatting function-helper.
        def format_variables(variables):
            f_context = variables.get("formats", {})

            formatted = {}
            for key, value in variables.items():
                if key == "formats":  continue

                if (format_obj := f_context.get(key)) and (format_type := format_obj["type"]):
                    if format_type == "time":
                        date  = datetime.datetime.fromisoformat(value)
                        value = date.strftime(format_obj["formatstr"])

                formatted[key] = value
            return formatted

        # Updating message text with extra formatting applied.
        context["message"]["text"] = \
            context["message"]["text"].format(
                # Here we are using special wrapper around user history, which acts like a 'promise',
                # meaning it doesn't evaluate values in the history (to strings) until it's explicitely
                # forced to do so via 'str()'. Which only happens when the message is ready to be sent.
                history=LazyHistoryGetter(self.user),
                # Additionaly, we insert the whole context of 'stored_vars' here, so anyone can insert
                # any of the existing (earlier created/modified) variables inside messages for display.
                #
                # Note: any variable names here are allowed, so you need to be careful, because they
                #       might start interfering with each-other or with other variables inside format.
                #       e.g. you shouldn't name any of your variables 'history', because it's reserved
                #       right above.
                **format_variables(stored_vars),
        )

    async def set_initial_configuration(self, prev_q: dict):
        # default = {"default_stop": False}
        default: dict[str, Any] = {"delay_chars_per_second": 40}

        assert prev_q is not None, "Set initial configuration must be used only with a valid prev_q!"

        if "config" not in prev_q["commands"]:
            self.user["answers"]["qa"]["stored_vars"]["flow_config"] = default
            return

        if config := prev_q["commands"]["config"][0]:
            default.update(config)

        self.user["answers"]["qa"]["stored_vars"]["flow_config"] = default

    async def run_commands_for_previous_message(self, previous_question: dict, raw_answer: str, button: Button, context: Context):
        # If `stop` button -> kill the conversation.
        if button == "stop":
            return END

        # Handle back button
        if button == "back":
            # At this point we got to the start of the 'previous' group of messages..
            self.get_relevant_q_and_trim_history()
            # ..but those messages are actually current ones, because they are the last ones
            # that we sent to get here. So, since 'back' command is activated, we want to go
            # one group deeper and find the beginning of an __actually__ previous group.
            prev_q = self.get_relevant_q_and_trim_history()

            # Now store relevant message as a current one..
            self.user["answers"]["qa"]["curr_q"] = prev_q["id"]
            # ..strip the input text here (avoiding infinite 'back' button recursion)..
            context["message"]["text"] = ""
            # ..and provide a flag to the 'process' method to treat 'curr_q' as a new one.
            return await self.process(context, back=True)

        if button == "mainMenu":
            context["message"]["text"] = "/start"
            return await self.entry(context)

        # Handle #multichoice procedyre here.
        if "multichoice" in previous_question["commands"]:
            btn_obj = self.get_button_with_key(previous_question, button.key)

            if previous_question["id"] not in self.user["answers"]["qa"]["qa_results"]:
                self.user["answers"]["qa"]["qa_results"][previous_question["id"]] = []
                self.user["answers"]["qa"]["multichoice_cache"][previous_question["id"]] = []

            # Handling answer to the multichoice here. Non-next means the user submitted some kind of answer.
            if "next" not in btn_obj["commands"]:
                # Handle repeating and bad answer:
                if raw_answer in self.user["answers"]["qa"]["qa_results"][previous_question["id"]] or button.key is None:
                    # Send invalid answer text
                    await self.handle_invalid_answer(raw_answer, context, self.strings["invalidAnswer"], history=[])
                    # self.send(context)
                    self.complex_send(context)

                    # Repeat the question
                    self.set_data_fmt(
                        context, previous_question,
                        avoid_buttons=self.user["answers"]["qa"]["multichoice_cache"][previous_question["id"]],
                    )
                    # self.send(context)
                    self.complex_send(context)
                    return OK

                # Record the answer in the temprorary storage.
                self.user["answers"]["qa"]["qa_results"][previous_question["id"]].append(raw_answer)

                # Make sure to store checked keys
                self.user["answers"]["qa"]["multichoice_cache"][previous_question["id"]].append(button.key)
                # Send special message with buttons that are left
                self.set_data_fmt(
                    context, previous_question,
                    avoid_buttons=self.user["answers"]["qa"]["multichoice_cache"][previous_question["id"]],
                )

                parsed_results = "\n".join(f"- {ra}" for ra in self.user["answers"]["qa"]["qa_results"][previous_question["id"]])
                context["message"]["text"] = self.strings["qaMulti"].format(
                    parsed_results or self.strings["qaNoChoice"],
                    self.get_next_command_button_text(previous_question),
                )
                # self.send(context)
                self.complex_send(context)
                return OK

            # Finally, here we know that the user submitted answers by clicking 'next' (or an
            # alternative to it). We override 'raw_answer' value here, so we dont have to change
            # the code, like storing user input to the history, later.
            raw_answer = ", ".join(self.user["answers"]["qa"]["qa_results"][previous_question["id"]])

        # Always run context commands after special buttons:
        if ret_code := await self.run_context_commands(previous_question, raw_answer, context, button, is_previous=True):
            return ret_code

        # Handle #remind procedure here.
        if "remind" in previous_question["commands"]:
            args = previous_question["commands"]["remind"][0]  # Rework this later, should be abstracted so there are no "[0]" all over the code..

            if (hours := args.get("in")) and (name := args.get("remind")):
                for item in self.bots_data[self.user["context"]["current_flow_name"]]:
                    if args.get("reminder_entry") == name:
                        self.user["context"]["remind_q"] = item["id"]
                        break
                else:
                    raise ValueError(f'Broken "remind" command, required values: hours={hours}, name={name}.')
                # TODO: custom changeable checkback text
                context["message"]["text"] = self.strings["checkback"]
                context["buttons"] = [{"text": self.strings["yes"]}, {"text": self.strings["no"]}]

                self.create_task(
                    self.db.create_checkback, self.user, copy.deepcopy(context.to_dict()),
                    timedelta(seconds=round(float(hours) * 60 * 60)),
                    checkback_type="one-time", state="CheckbackState"
                )
            else:
                raise ValueError(f'Broken "remind" command, required values: hours={hours}, name={name}.')

    async def run_commands_for_next_message(self, next_question: Optional[dict], previous_question: dict, raw_answer: str, button: Button, context: Context):
        # No next message -> assume wrong answer, repeat the question.  @Robustness
        if next_question is None:
            await self.handle_invalid_answer(raw_answer, context, self.strings["invalidAnswer"], history=[])
            # self.send(context)
            self.complex_send(context)

            # Repeat the question.
            self.set_data_fmt(context, previous_question)
            # self.send(context)
            self.complex_send(context)
            return OK

        # Run commands for the next question here, because we might leave the state at any point
        # below. Note that are running context commands with the value of 'is_previous' set to
        # 'False', to indicate that we only need to execute commands that are not intended for the
        # previous message.
        if ret_code := await self.run_context_commands(next_question, raw_answer, context, button, is_previous=False):
            return ret_code

        # Handle special procedure #end.
        if "end" in next_question["commands"]:
            # Set custom default message if one is configured.
            if next_question["commands"]["end"]:
                self.user["context"]["enable_end_message"] = next_question["commands"]["end"][0].get("default_message", True)

            # This handles potential problem with an empty text if someone used #end procedure as the whole message.
            if next_question["text"]:
                self.set_data_fmt(context, next_question)
                # self.send(context)
                self.complex_send(context)
            return END

        # Handle special procedure #ai.
        if "ai" in next_question["commands"]:
            # Set custom default message if one is configured.
            if (commands := next_question["commands"]["ai"]) and (command := commands[0]):
                self.user["context"]["enable_ai_message"] = command.get("default_message", True)

                # An optional argument ot randomize model, instead of using default or give it explicitly.
                if random := command.get("random_model"):  self.user["context"]["random_ai_model"] = random
                # An optional argument that allows us to choose from botsociety what model should be used.
                # Note: this is incompatible with 'random=True' (latter is higher priority, turns this into
                # 'NOOP' statement).
                elif model := command.get("model"):        self.user["context"]["preferred_ai_model"] = model

                # An optional argument that allows us to choose the amount of turns after which AI disengages the conversation.
                if limit := command.get("limit"):          self.user["context"]["message_ai_limit"] = limit

            # This handles potential problem with an empty text if someone used #ai procedure as the whole message.
            if next_question["text"]:
                self.set_data_fmt(context, next_question)
                # self.send(context)
                self.complex_send(context)
            return GO_TO_STATE("AIState")

    async def _run_context_commands(self, question: dict, raw_answer: str, context: Context, is_previous: bool = False):
        # Lets borrow some references here, because access is too long.
        stored_vars = self.user["answers"]["qa"]["stored_vars"]
        commands    = question["commands"]

        # Procedure: #back_ignore_partials
        # Arguments: ---------------------
        #
        # Description: This directive marks edge "leaf" (or "node"), and makes the 'back' button functionality to
        #              ignore any 'partial' messages before it, when going to that message from the bottom of the
        #              conversation tree. For example, lets say we have a conversation tree:
        #
        #                                                       Message 1
        #                                                ----------------------
        #                                                |      Init menu:    |
        #                                                ----------------------
        #                                                | Button A |   ...   |
        #                                                ----------------------
        #                                                     |
        #                                                 Message 2
        #                                         -------------------------
        #                                         | Some promo text/media |
        #                                         | #partial              |
        #                                         -------------------------
        #                                                     |
        #                                                 Message 3
        #                                         -------------------------
        #                                         |      Cool menu:       |
        #                                         | #back_ignore_partials |
        #                                         -------------------------
        #                                         | Button Z  |    ...    |
        #                                         -------------------------
        #                                               |
        #                                           Message 4
        #                                         -------------
        #                                         |   Done!   |
        #                                         -------------
        #                                         |   Back    |
        #                                         -------------
        #
        #              In this case, what '#back_ignore_partials' marks is that, when going from the top of the flow
        #              tree to the bottom - promo message (2) will be displayed:
        #
        #                      -------------                -------------    -------------
        #                      | Message 1 | --[Button A]-> | Message 2 | -> | Message 3 |
        #                      -------------                -------------    -------------
        #
        #              But when going up this tree structure - promo message (2) will be ignored:
        #
        #                      -------------            -------------
        #                      | Message 4 | --[Back]-> | Message 3 |
        #                      -------------            -------------
        #
        #              Note, that normal behavior for 'back' button in the Message 4 would be to go back to promo message
        #              and only then show the 'cool menu':
        #
        #                      -------------            -------------    -------------
        #                      | Message 4 | --[Back]-> | Message 2 | -> | Message 3 |
        #                      -------------            -------------    -------------
        #
        if not is_previous and "back_ignore_partials" in commands:
            stored_vars["back_ignore_partials_leaves"][question["id"]] = True

        # This procedure should run only for the "next" message.
        if not is_previous and (var_commands := commands.get("var")):
            # This command saves variable into the context storage. It rewrites storage, so you can both create and
            # update any variable with it.
            #
            # Procedure: #var
            # Arguments:
            #    * name
            #    * value
            #    * Optional[type] = "literal"
            #      -- This argument allows us to create an empty array, instead of storing a literal. EITHER this
            #         argument OR value are necessary.  @Robustness: Isn't it already capable of evaluating literal '[]'?
            for args in var_commands:
                _type = args.get("type", "literal")
                if _type == "literal":  stored_vars[args["name"]] = encode(args["value"])
                elif _type == "array":  stored_vars[args["name"]] = []

        # if append_command := commands.get("append"):
        #     # This procedure appends to the variable, if the variable is an array.
        #     #
        #     # Procedure: #append
        #     # Arguments:
        #     #    * name
        #     #    * value
        #     for args in append_command:
        #         self.user["answers"]["qa"]["stored_vars"][args["name"]].append(args["value"])

        validators = {
            "string": None,  # It could be a 'str', but instead lets just do nothing.
            "integer": int,
            "number": int,  # This is just an integer alias, but more user friendly name (non-techy).
            "float": float,
            # Array is a string separated by commas.
            "array": lambda s: list(ss.strip() for ss in s.split(",")),
            # Validate time through RASA NLU.
            "time": self.nlu.detect_datetime,
        }
        # Store only works for previous messages.
        if is_previous and (store_commands := commands.get("store")):
            # This command saves raw user input (i.e. user's reply message) to the given variable.
            #
            # Procedure: #store
            # Arguments:
            #    * var_name
            #    * Optional[type]  -- You can enforce certain input type here.
            for args in store_commands:
                value: Union[str, int, float, list[str]]
                _type = args.get("type", "string")

                # Get a validator for the chosen type. Skipping if there is no validator..
                if _type and (validator := validators.get(_type)):
                    try:
                        # ..and type check it with async or sync routine.
                        is_coro = inspect.iscoroutinefunction(validator)
                        # Note(andrew): Validators are expected to raise an error if the input is invalid.
                        if is_coro:  value = await validator(raw_answer)  # type: ignore [misc]
                        else:        value = validator(raw_answer)

                        # Note(andrew): Additionaly, make sure our validators didn't return 'None'. Don't
                        #     forget that this is not the only place that throws value error, even though
                        #     it looks a bit weird. Any validation func call is going to throw ValueError
                        #     for bad input value, so *don't* just wrap this 'if' statement with try-catch.
                        if value is None:  raise ValueError

                    except ValueError:
                        # TODO(andrew): Keep it in here for a while, just in case. Can be changed to 'debug' later.
                        logging.warning("Expected store input type '%s', but received raw input '%s' ...", _type, raw_answer)

                        # This default message will contain explanation of what input type was expected from the user.
                        # TODO(andrew): Maybe we should also try providing some examples of the expected input (?)
                        context["message"]["text"] = self.strings["badStoreTypeError"].format(_type)
                        context["buttons"] = [{"text": self.strings["back"]}, {"text": self.strings["stop"]}]  # @DefaultButtons
                        # self.send(context)
                        self.complex_send(context)
                        # Early exit here, after filling the error.
                        return OK

                    # Additional configurations, that we might apply here.
                    if _type == "time":
                        formats = stored_vars.get("formats", {})
                        # If the dictionary is empty, we are most likely created new one, so need to store it.
                        if not formats:  stored_vars["formats"] = formats
                        # Adding format of the datetime (or setting default -- '02:25 May 12 2018').
                        formats[args["var_name"]] = {"type": "time", "formatstr": args.get("format", "%H:%M %B %d, %Y")}

                else:
                    value = raw_answer

                # Finally, store the processed input to the variable name.
                stored_vars[args["var_name"]] = value

                # @Incomplete: Maybe we should disable saving response to the history then?
                # save_answer = False (this is outside current scope of the function, in the main scope)

        # This command should run only for the "next" message.
        if not is_previous and (eval_commands := commands.get("eval")):
            # This command evaluates expression that can contain mathematical expressions (like '==', '>' etc) and some
            # python builtins (like 'bool()', 'len()' etc). The result of the evaluation is saved to the given variable.
            #
            # Procedure: #eval
            # Arguments:
            #    * save_to
            #    * expr
            for args in eval_commands:
                stored_vars[args["save_to"]] = self.eval_expr(args["expr"])

    async def run_context_commands(self, question: dict, raw_answer: str, context: Context, button: Button, is_previous: bool):
        # Run procedures for the question itself.
        if ret_code := await self._run_context_commands(question, raw_answer, context, is_previous):
            return ret_code

        if btn_obj := self.get_button_with_key(question, button.key):
            # Check for the arguments in the buttons, so they can be executed as well.
            if ret_code := await self._run_context_commands(btn_obj, raw_answer, context, is_previous):
                return ret_code

    async def run_aftermath_commands(self, question: dict, raw_answer: str, context: Context):
        # Procedure #pop removes the variable, but only after the message have already been sent.
        for args in question["commands"].get("pop", tuple()):
            del self.user["answers"]["qa"]["stored_vars"][args["name"]]

        # Procedure #flow allows to set/change flow that is being used in the botsociety.
        for args in question["commands"].get("flow", tuple()):
            if args["name"] in ("default", "main"):  flow_name = MAIN_BOTSOCIETY_FLOW
            else:                                    flow_name = args["name"]
            # Setting new flow for the next message.
            self.user["context"]["current_flow_name"] = flow_name

    # @Speed: This will be slow, maybe we need to parse command (and everything related to botsociety) into
    #         some construct, so then we can map variables directly from known names-nameplaces etc.
    def eval_expr(self, expr: str):
        # Fill the expression with variables:
        for arg_match in self.EMBEDDED_ARGUMENTS.finditer(expr):
            var_expr = arg_match.group(0)
            var_name = arg_match.group(1)

            value = self.user["answers"]["qa"]["stored_vars"].get(var_name, "")

            # Rewriting 'expr' here, which updates it in-place, so we can iteratively insert all values.
            expr = expr.replace(var_expr, str(value))
        return encode(self.interpreter.eval(expr))

    def complex_send(self, context: Context):
        config = self.user["answers"]["qa"]["stored_vars"]["flow_config"]

        if not (delay := config.get("delay")):
            self.send(context)
            return

        self.set_reading_speed(config.get("delay_chars_per_second", 40))  # Backup default (for live-updated flows)
        self.send(context, delay=delay)


class LazyHistoryGetter:
    def __init__(self, user):
        self.user = user

    def __str__(self):
        return "\n".join(f"{key}: {value}" for key, value in self.user["answers"]["qa"]["qa_results"].items())
