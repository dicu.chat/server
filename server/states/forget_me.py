#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from . import BaseState, Context, OK, BACK
import asyncio


# @Important: This state deletes all user data we have about any specific user.
class ForgetMeState(BaseState):

    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["forgetMeConfirmationQ"]
        context["buttons"] = [{"text": button} for button in self.get_buttons()]
        self.send(context)
        return OK

    async def process(self, context: Context):
        prompt = context["message"]["text"]
        button = self.parse_button(prompt, verify=self.get_buttons())

        if button == "confirm":
            asyncio.create_task(self.db.delete_user(self.user["identity"]))

            context["message"]["text"] = self.strings["forgetMeEnd"]
            context["buttons"] = []
            self.send(context)
            return OK(commit=False)

        elif button == "back":
            return BACK

        await self.handle_invalid_answer(
            prompt, context, self.strings["invalidAnswer"],
            history=["The user made a mistake and entered wrong input. The user needs to confirm "
                     "or deny suggestion from the bot, instead they entered following text:"]
        )
        self.send(context)

        context["message"]["text"] = self.strings["forgetMeConfirmationQ"]
        context["buttons"] = [{"text": button} for button in self.get_buttons()]
        self.send(context)
        return OK

    def get_buttons(self):
        return [self.strings["confirm"], self.strings["back"]]
