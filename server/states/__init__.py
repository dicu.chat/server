#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from ..definitions import OK, GO_TO_STATE, END, BACK, NOOP, Context
from ..db import PermissionLevel
from ..base import BaseState
import importlib
import inspect
import os

__all__ = (
    "collect",
    "BaseState",
    "OK",
    "GO_TO_STATE",
    "END",
    "BACK",
    "NOOP",
    "Context",
    "PermissionLevel",
)


def collect():
    collection = list()
    # For all files in current folder
    for file in os.listdir(os.path.dirname(__file__)):
        # If file starts from letter and ends with .py
        if file[0].isalpha() and file.endswith(".py"):
            # Name of the file with prefix "states." but without ".py"
            name_as_module = f"{__name__}.{file[:-3]}"
            # Import this file
            as_module = importlib.import_module(".", name_as_module)
            # For each object definition that is a class
            for name, obj in inspect.getmembers(as_module, inspect.isclass):
                if name.endswith("State"):
                    collection.append(obj)
    return collection
