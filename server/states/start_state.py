#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from . import BaseState, Context, GO_TO_STATE, OK, PermissionLevel
from time import time


class StartState(BaseState):
    has_entry = False
    # Allow anyone to access this.
    permission_required = PermissionLevel.ANON

    async def process(self, context: Context):
        # Note(andrew): This logic handles unauthorized users gracefully by showing them
        #     a "access denied" message (for a friendly, less confusing UX, compared to
        #     straight ignoring them). But to avoid DoS possiblities, we show the message
        #     only once per minute.
        if self.permission_required > self.user["permissions"]:
            last_time = self.user["context"].get("bad_permissions_warned", 10)
            curr_time = int(time())
            if curr_time - last_time > 60:
                self.user["context"]["bad_permissions_warned"] = curr_time
                context["message"]["text"] = self.strings["badPermissionsWarning"]
                context["buttons"] = []
                self.send(context)
                return OK

        # Note(andrew): We also want to deal with a special case here: our main flow state
        #     is botsociety state, where the main user-facing interface is presented. And
        #     we always want to eventually reach that state, and *right now* we forward all
        #     users directly from this start state to QAState, where possible. This is why
        #     the code below needs to be here.
        #
        #     The actual issue we are addressing here is when user presses /start, we want
        #     to remove any states from history, so '.entry()' for any following state is
        #     being called properly for each /start, even when user repeats the command few
        #     times in a row. There is probably a better way to do this (and maybe we want
        #     to apply this for other /commands too).  @Robustness
        if self.user["states"]:  self.user["states"] = []

        return GO_TO_STATE("LanguageDetectionState")
