#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from . import BaseState, Context, OK, BACK, NOOP, GO_TO_STATE
from datetime import datetime, timedelta
from ..external import AtlassianAPI
from ..utils import iter_sliced
from typing import Optional
from copy import deepcopy
import math


class ReminderState(BaseState):
    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["reminderInit"]
        context["buttons"] = [{"text": promise} for promise in self.get_keys()]
        self.send(context)

        self.user["context"]["reminder"] = {
            "type": None,
            "jira": {
                "statuses": [],  # type: ignore [dict-item]
                "projects": [],  # type: ignore [dict-item]
            },
            "custom": {
                "title": None,
                "body":  None,
            },
            "period": None,
            "time":   None,
        }
        return OK

    async def process(self, context: Context):
        raw_prompt = context["message"]["text"]
        button = self.parse_button(raw_prompt, verify=self.get_keys())

        if   button == "back":  return BACK
        elif button == "new":   return GO_TO_STATE("NewReminderState")
        elif button == "list":  return GO_TO_STATE("ListReminderState")

        await self.handle_invalid_answer(
            raw_prompt, context, self.strings["invalidAnswer"],
            history=["You are currently in the reminder state. It helps you creating new reminders or list existing ones."]
        )
        context["buttons"] = [{"text": promise} for promise in self.get_keys()]
        self.send(context)
        return OK

    def get_keys(self):
        return (self.strings["new"], self.strings["list"], self.strings["back"])


class NewReminderState(BaseState):
    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["reminderType"]
        context["buttons"] = [{"text": promise} for promise in self.get_keys()]
        self.send(context)
        return OK

    async def process(self, context: Context):
        raw_prompt = context["message"]["text"]
        button = self.parse_button(raw_prompt, verify=self.get_keys())

        if button == "back":
            return BACK

        elif button == "typeJira":
            if not await self.db.get_external_service(self.user["identity"], "jira"):
                context["message"]["text"] = self.strings["reminderErrJiraUnauth"]
                context["buttons"] = [{"text": promise} for promise in self.get_keys()]
                self.send(context)
                return OK

            self.user["context"]["reminder"]["type"] = "jira"
            return GO_TO_STATE("TitleReminderState")

        elif button == "typeGoogleCalendar":
            if not await self.db.get_external_service(self.user["identity"], "google calendar"):
                context["message"]["text"] = self.strings["reminderErrGoogleCalendarUnauth"]
                context["buttons"] = [{"text": promise} for promise in self.get_keys()]
                self.send(context)
                return OK

            self.user["context"]["reminder"]["type"] = "google calendar"
            return GO_TO_STATE("TitleReminderState")

        elif button == "typeCustom":
            self.user["context"]["reminder"]["type"] = "custom"
            return GO_TO_STATE("TitleReminderState")

        await self.handle_invalid_answer(
            raw_prompt, context, self.strings["invalidAnswer"],
            history=["You are currently choosing different types of reminder, like 'jira' and 'custom'."]
        )
        context["buttons"] = [{"text": promise} for promise in self.get_keys()]
        self.send(context)
        return OK

    def get_keys(self):
        return (
            self.strings["typeJira"], self.strings["typeCustom"],
            self.strings["typeGoogleCalendar"], self.strings["back"],
        )


class TitleReminderState(BaseState):
    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["reminderTitle"]
        context["buttons"] = [{"text": self.strings["back"]}]
        self.send(context)
        return OK

    async def process(self, context: Context):
        raw_prompt = context["message"]["text"]
        button = self.parse_button(raw_prompt, verify=(self.strings["back"],))

        if button == "back":  return BACK

        # TODO: add character limit (something like 256)
        self.user["context"]["reminder"]["custom"]["title"] = raw_prompt

        # Handle special type of the reminder "JIRA" - no body.
        if self.user["context"]["reminder"]["type"] == "jira":             return GO_TO_STATE("JiraReminderState")
        # Handle special type of the reminder "Google Calendar" - no body.
        if self.user["context"]["reminder"]["type"] == "google calendar":  return GO_TO_STATE("TypeReminderState")

        return GO_TO_STATE("BodyReminderState")


class BodyReminderState(BaseState):
    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["reminderBody"]
        context["buttons"] = [{"text": self.strings["back"]}]
        self.send(context)
        return OK

    async def process(self, context: Context):
        raw_prompt = context["message"]["text"]
        button = self.parse_button(raw_prompt, verify=(self.strings["back"],))

        if button == "back":  return BACK

        # TODO: add character limit (something like 2048)
        self.user["context"]["reminder"]["custom"]["body"] = raw_prompt
        return GO_TO_STATE("TypeReminderState")


class JiraReminderState(BaseState):
    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["reminderStatus"]
        context["buttons"] = [{"text": promise} for promise in self.get_keys()]
        self.send(context)

        self.user["context"]["jira_multichoice"] = []
        return OK

    async def process(self, context: Context):
        raw_prompt = context["message"]["text"]
        button = self.parse_button(raw_prompt, verify=self.get_keys())

        if   button == "back":  return BACK
        elif button == "next":  return GO_TO_STATE("JiraProjectReminderState")
        elif button:
            # Access original english text value of the button and add it ot the list of available statuses.
            self.user["context"]["reminder"]["jira"]["statuses"].append(str(self.en_strings[button.key]))
            self.user["context"]["jira_multichoice"].append(button.key)

            parsed_results = "\n".join(f"- {self.strings[key]}" for key in self.user["context"]["jira_multichoice"])
            context["message"]["text"] = self.strings["qaMulti"].format(
                parsed_results or self.strings["qaNoChoice"], self.strings["next"],
            )
            context["buttons"] = [
                {"text": p} for p in self.get_keys() if p.key not in self.user["context"]["jira_multichoice"]
            ]
            self.send(context)
            return OK

        await self.handle_invalid_answer(
            raw_prompt, context, self.strings["invalidAnswer"],
            history=["You are currently choosing different jira issue statuses, like 'Open', 'In Progress', 'To Do' etc."]
        )
        context["buttons"] = [{"text": promise} for promise in self.get_keys()]
        self.send(context)
        return OK

    def get_keys(self):
        return (
            self.strings["statusBlocked"], self.strings["statusOpen"],
            self.strings["statusToDo"], self.strings["statusAdHoc"],
            self.strings["statusInProgress"], self.strings["statusResolved"],
            self.strings["statusToReview"],
            self.strings["next"], self.strings["back"],
        )


class JiraProjectReminderState(BaseState):
    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["reminderProject"]
        # Here we know use is already authorized:
        creds = await self.db.get_external_service(self.user["identity"], "jira")
        api = AtlassianAPI(creds["url"], creds["username"], creds["token"])
        # Query projects:
        self.user["context"]["jira_projects"] = {proj["name"]: proj["key"] for proj in await api.get_projects()}
        # Project names as buttons:
        context["buttons"] = [
            *({"text": text} for text in self.user["context"]["jira_projects"]),
            {"text": self.strings["next"]}, {"text": self.strings["back"]},
        ]
        self.send(context)
        # Multichoice cache:
        self.user["context"]["jira_multichoice"] = []
        return OK

    async def process(self, context: Context):
        raw_prompt = context["message"]["text"]
        button = self.parse_button(raw_prompt, verify=(self.strings["next"], self.strings["back"]))

        if   button == "back":  return BACK
        elif button == "next":  return GO_TO_STATE("TypeReminderState")
        elif code := self.user["context"]["jira_projects"].get(raw_prompt):
            self.user["context"]["reminder"]["jira"]["projects"].append(code)
            self.user["context"]["jira_multichoice"].append(raw_prompt)

            parsed_results = "\n".join(f"- {v}" for v in self.user["context"]["jira_multichoice"])
            context["message"]["text"] = self.strings["qaMulti"].format(
                parsed_results or self.strings["qaNoChoice"], self.strings["next"],
            )
            context["buttons"] = [
                *({"text": v} for v in self.user["context"]["jira_projects"]
                    if v not in self.user["context"]["jira_multichoice"]),
                {"text": self.strings["next"]}, {"text": self.strings["back"]},
            ]
            self.send(context)
            return OK

        await self.handle_invalid_answer(
            raw_prompt, context, self.strings["invalidAnswer"],
            history=[
                "You are currently choosing one or few of your jira projects for the reminder. "
                f"Available names are: {', '.join(self.user['context']['jira_projects'])}."
            ]
        )
        context["buttons"] = [
            *({"text": text} for text in self.user["context"]["jira_projects"]),
            {"text": self.strings["next"]}, {"text": self.strings["back"]},
        ]
        self.send(context)
        return OK


class TypeReminderState(BaseState):
    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["reminderRepeat"]
        context["buttons"] = [{"text": p} for p in self.get_keys()]
        self.send(context)
        return OK

    async def process(self, context: Context):
        raw_prompt = context["message"]["text"]
        button = self.parse_button(raw_prompt, verify=self.get_keys())

        if   button == "back":  return BACK
        elif button.key in ["oneTime", "daily", "weekly"]:
            self.user["context"]["reminder"]["period"] = button.key
            return GO_TO_STATE("TimeReminderState")

        await self.handle_invalid_answer(
            raw_prompt, context, self.strings["invalidAnswer"],
            history=["You are currently choosing reminder type, either a one-time, daily or weekly reminder."]
        )
        context["buttons"] = [{"text": p} for p in self.get_keys()]
        self.send(context)
        return OK

    def get_keys(self):
        return (
            self.strings["oneTime"], self.strings["daily"],
            self.strings["weekly"], self.strings["back"],
        )


class TimeReminderState(BaseState):
    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["reminderTime"].format(datetime.now().strftime("%H:%M %d/%m/%Y"))
        context["buttons"] = [{"text": self.strings["back"]}]
        self.send(context)
        return OK

    async def process(self, context: Context):
        raw_prompt = context["message"]["text"]
        button = self.parse_button(raw_prompt, verify=(self.strings["back"],))

        if button == "back":  return BACK

        # Parsing time.
        try:   dt_obj: Optional[datetime] = datetime.strptime(raw_prompt, "%H:%M %d/%m/%Y")
        except ValueError:
            # Here we try another format (short format for the year).
            try:   dt_obj = datetime.strptime(raw_prompt, "%H:%M %d/%m/%y")
            except ValueError:
                dt_obj = None

        # Finally we process date object if it's parsed correctly.
        if dt_obj is None:
            await self.handle_invalid_answer(
                raw_prompt, context, self.strings["reminderErrDateParsing"],
                history=["You entered a date in the invalid format!"],
            )
            context["buttons"] = [{"text": self.strings["back"]}]
            self.send(context)
            return OK

        # Arbitrary data value validation:
        # 1. Must be in the future, so bigger than current datetime.
        elif dt_obj < datetime.now():
            await self.handle_invalid_answer(
                raw_prompt, context, self.strings["reminderErrRequireFuture"],
                history=["You entered a date in the past, you must enter some time in the future!"],
            )
            context["buttons"] = [{"text": self.strings["back"]}]
            self.send(context)
            return OK

        # 2. Minimum time until reminder is 2 minutes.
        elif dt_obj - datetime.now() < timedelta(minutes=2):
            await self.handle_invalid_answer(
                raw_prompt, context, self.strings["reminderErrMinTime"].format("2 minutes"),
                history=["You entered a time that is too soon. Minimum time is 2 minutes!"],
            )
            context["buttons"] = [{"text": self.strings["back"]}]
            self.send(context)
            return OK

        self.user["context"]["reminder"]["time"] = dt_obj.isoformat()
        return GO_TO_STATE("ConfirmationReminderState")


class ConfirmationReminderState(BaseState):
    async def entry(self, context: Context):
        time_obj = datetime.fromisoformat(self.user["context"]["reminder"]["time"])
        now_obj = datetime.now()

        is_type = lambda _type: self.user["context"]["reminder"]["type"] == _type
        if   is_type("jira"):             body = self.strings["reminderJiraBody"]
        elif is_type("google calendar"):  body = self.strings["reminderGoogleCalendarBody"]
        # This is a default ('custom') path:
        else:                             body = self.user["context"]["reminder"]["custom"]["body"]

        context["message"]["text"] = self.strings["reminderCheck"].format(
            self.user["context"]["reminder"]["custom"]["title"],
            body,
            self.user["context"]["reminder"]["type"],
            self.user["context"]["reminder"]["period"],
            time_obj.strftime("%H:%M %d/%m/%y"),
            now_obj.strftime("%H:%M %d/%m/%y"),
        )
        context["buttons"] = [{"text": p} for p in self.get_keys()]
        self.send(context)
        return OK

    async def process(self, context: Context):
        raw_prompt = context["message"]["text"]
        button = self.parse_button(raw_prompt, verify=self.get_keys())

        if   button == "back":     return BACK
        elif button == "confirm":
            # Prepare message.
            if self.user["context"]["reminder"]["type"] in ("jira", "google calendar"):
                context["message"]["text"] = self.strings["reminderBaseJira"].format(
                    self.user["context"]["reminder"]["custom"]["title"]
                )
            else:
                context["message"]["text"] = self.strings["reminderBaseCustom"].format(
                    self.user["context"]["reminder"]["custom"]["title"],
                    self.user["context"]["reminder"]["custom"]["body"],
                )

            context["buttons"] = []
            self.create_task(
                self.db.create_checkback, self.user, deepcopy(context.to_dict()),
                datetime.fromisoformat(self.user["context"]["reminder"]["time"]) - datetime.now(),
                kwargs = dict(
                    title = self.user["context"]["reminder"]["custom"]["title"],
                    period = self.user["context"]["reminder"]["period"],
                    type = self.user["context"]["reminder"]["type"],
                    **self.user["context"]["reminder"]["jira"],
                ),
            )
            # Remove data after processing state.
            # del self.user["context"]["reminder"]

            context["message"]["text"] = self.strings["reminderDone"]
            context["buttons"] = []
            self.send(context)
            # Processing finished, retreat from the state.
            return NOOP

        elif button == "cancel":
            # Remove data after processing state.
            # del self.user["context"]["reminder"]

            context["message"]["text"] = self.strings["reminderCancel"]
            context["buttons"] = []
            self.send(context)
            # Processing finished, retreat from the state.
            return NOOP

        await self.handle_invalid_answer(
            raw_prompt, context, self.strings["invalidAnswer"],
            history=["You need to either confirm or cancel the reminder, please press an according button."]
        )
        context["buttons"] = [{"text": p} for p in self.get_keys()]
        self.send(context)
        return OK

    def get_keys(self):
        return (self.strings["confirm"], self.strings["cancel"], self.strings["back"])


# TODO: Everything below is basically the same pagination / menu code as in 'authorization_state' - should be generalized.
class ListReminderState(BaseState):
    async def entry(self, context: Context):
        self.user["context"]["reminder_menu"] = {
            "page": 0,
            "page_size": 9,
            "total_approx": None,
            "current_page": {},
        }
        await self.send_page(context)
        return OK

    async def process(self, context: Context):
        raw_prompt = context["message"]["text"]
        button = self.parse_button(raw_prompt, verify=self.get_keys())

        if   button == "back":  return BACK
        elif button == "next":
            # Turning serialized "Decimal" back into "int". Remember: "Decimal(-1) % 3" evaluates to "-1"..
            page      = int(self.user["context"]["reminder_menu"]["page"])
            page_size = int(self.user["context"]["reminder_menu"]["page_size"])
            total     = int(self.user["context"]["reminder_menu"]["total_approx"])

            self.user["context"]["reminder_menu"]["page"] = (page + 1) % math.ceil((total or 1) / page_size)
            await self.send_page(context)
            return OK

        elif button == "previous":
            # Turning serialized "Decimal" back into "int". Remember: "Decimal(-1) % 3" evaluates to "-1"..
            page      = int(self.user["context"]["reminder_menu"]["page"])
            page_size = int(self.user["context"]["reminder_menu"]["page_size"])
            total     = int(self.user["context"]["reminder_menu"]["total_approx"])

            self.user["context"]["reminder_menu"]["page"] = (page - 1) % math.ceil((total or 1) / page_size)
            await self.send_page(context)
            return OK

        elif raw_prompt and self.user["context"]["reminder_menu"]["current_page"].get(raw_prompt, False):
            await self.db.delete_checkback(self.user["context"]["reminder_menu"]["current_page"][raw_prompt])  # Passing here stored value as checkback_id (uuid4 value)
            await self.send_page(context)
            return OK

        await self.handle_invalid_answer(
            raw_prompt, context, self.strings["invalidAnswer"],
            history=["Please press one of the buttons!"]
        )
        self.send(context)
        return OK

    async def send_page(self, context: Context):
        # Turning serialized "Decimal" back into "int". Remember: "Decimal(-1) % 3" evaluates to "-1"..
        page      = int(self.user["context"]["reminder_menu"]["page"])
        page_size = int(self.user["context"]["reminder_menu"]["page_size"])

        text, total, buttons = await self.get_page(context, page, page_size)
        # Special case for when all reminders on this page were deleted, but in general, there are more reminders left to display.
        if not text and total:
            page -= 1
            text, total, buttons = await self.get_page(context, page, page_size)

        context["message"]["text"] = self.strings["reminderList"].format(
            text or "",  # Display empty string if text is None (no tokens in db).
            page + 1,  # Display page, range starts from 1.
            math.ceil((total or 1) / page_size),
        )
        context["buttons"] = buttons
        # Store for reuse:
        self.user["context"]["reminder_menu"]["total_approx"] = total
        self.send(context)

    async def get_page(self, context: Context, page: int, page_size: int):
        # Clear space for reminders in the context.
        self.user["context"]["reminder_menu"]["current_page"].clear()

        # Query reminders from db
        reminders, total = await self.db.query_checkbacks_by_chat(context["chat"]["chatId"], page, page_size)
        text, buttons = None, []  # type: ignore [var-annotated]
        row_size = 3
        for row_n, row in enumerate(iter_sliced(reminders, row_size)):
            tmp = []
            for i, item in enumerate(row):
                line = self.strings["reminderListItem"].format(
                    page * page_size + row_n * row_size + i + 1, item["kwargs"]["title"][:24],  # Will this be sufficient?
                )
                tmp.append({"text": item["kwargs"]["title"][:24]})
                text = line if text is None else text + line

                # Store reminder title in the context.
                self.user["context"]["reminder_menu"]["current_page"][item["kwargs"]["title"]] = item["id"]

            buttons.append(tmp)

        buttons.append([{"text": p} for p in self.get_keys()])
        return text, total, buttons

    def get_keys(self):
        return (self.strings["previous"], self.strings["back"], self.strings["next"])
