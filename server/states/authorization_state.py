#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from . import BaseState, Context, OK, GO_TO_STATE, BACK, NOOP, PermissionLevel
from aiohttp import ContentTypeError, InvalidURL, ClientError
from ..external import AtlassianAPI, GoogleCalendarAPI
from ..db import ExternalService
from ..utils import iter_sliced
import secrets
import math


class AuthorizationState(BaseState):
    # Allow anyone to access this.
    permission_required = PermissionLevel.ANON

    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["authorizedMenu"]
        context["buttons"] = [{"text": promise} for promise in self.get_keys()]
        self.send(context)
        return OK

    async def process(self, context: Context):
        button = self.parse_button(context["message"]["text"], verify=self.get_keys())
        # Handle buttons:
        if   button == "back":             return BACK
        elif button == "authCreateToken":  return GO_TO_STATE("AuthorizationCreateTokenState")
        elif button == "authListTokens":   return GO_TO_STATE("AuthorizationListTokensState")
        elif button == "authUseToken":     return GO_TO_STATE("AuthorizationUseTokenState")
        elif button == "authServices":     return GO_TO_STATE("AuthorizationServicesState")
        return OK

    def get_keys(self):
        promises = []
        # These buttons only make sense if bot is not publicly open.
        if PermissionLevel.ANON < BaseState.permission_required:
            # Admin can create tokens:
            if self.user["permissions"] >= PermissionLevel.ADMIN:         promises.extend([self.strings["authCreateToken"], self.strings["authListTokens"]])
            # User needs authorization:
            if self.user["permissions"] < BaseState.permission_required:  promises.append(self.strings["authUseToken"])

        # User is authorized and can access the bot:
        if self.user["permissions"] >= BaseState.permission_required:     promises.append(self.strings["authServices"])

        # Default buttons:
        promises.append(self.strings["back"])
        return promises


class AuthorizationCreateTokenState(BaseState):
    # Only respond to an admin.
    permission_required = PermissionLevel.ADMIN

    async def entry(self, context: Context):
        await self.send_token_msg(context)
        return OK

    async def process(self, context: Context):
        button = self.parse_button(context["message"]["text"], verify=self.get_keys())

        # Handle buttons:
        if button == "back":
            return BACK

        elif button == "authNewToken":
            await self.send_token_msg(context)
            return OK

        # Fallback:
        return OK

    async def send_token_msg(self, context: Context):
        # Generate token:
        token = secrets.token_hex(32)
        # Storing token to db.
        await self.db.create_access_token({"token": token, "identity": "✅"})  # ✅ Stands for "empty"
        # Reply with the token.
        context["message"]["text"] = self.strings["authorizedNewToken"].format(token)
        context["buttons"] = [{"text": p} for p in self.get_keys()]
        self.send(context)

    def get_keys(self):
        return (self.strings["authNewToken"], self.strings["back"])


class AuthorizationListTokensState(BaseState):
    # Only respond to an admin.
    permission_required = PermissionLevel.ADMIN

    async def entry(self, context: Context):
        self.user["context"]["tokens_menu"] = {
            "page": 0,
            "page_size": 9,
            "total_approx": None,
            "current_page": {},
        }
        await self.send_page(context)
        return OK

    async def process(self, context: Context):
        text   = context["message"]["text"]
        button = self.parse_button(text, verify=self.get_keys())

        # Turning serialized "Decimal" back into "int". Remember: "Decimal(-1) % 3" evaluates to "-1"..
        page      = int(self.user["context"]["tokens_menu"]["page"])
        page_size = int(self.user["context"]["tokens_menu"]["page_size"])
        total     = int(self.user["context"]["tokens_menu"]["total_approx"])

        if button == "next":
            self.user["context"]["tokens_menu"]["page"] = (page + 1) % math.ceil((total or 1) / page_size)
            await self.send_page(context)
            return OK

        elif button == "previous":
            self.user["context"]["tokens_menu"]["page"] = (page - 1) % math.ceil((total or 1) / page_size)
            await self.send_page(context)
            return OK

        elif button == "back":
            return BACK

        token = self.user["context"]["tokens_menu"]["current_page"].get(text, False)
        if text and len(text) == 10 and token:
            await self.db.delete_access_token(token)
            await self.send_page(context)
            return OK

        return NOOP

    async def send_page(self, context: Context):
        identity  = self.user["identity"]
        # Turning serialized "Decimal" back into "int". Remember: "Decimal(-1) % 3" evaluates to "-1"..
        page      = int(self.user["context"]["tokens_menu"]["page"])
        page_size = int(self.user["context"]["tokens_menu"]["page_size"])

        text, total, buttons = await self.get_page(identity, page, page_size)
        # Special case for when all tokens on this page were deleted, but in general, there are more tokens left to display.
        if not text and total:
            page -= 1
            text, total, buttons = await self.get_page(identity, page, page_size)

        context["message"]["text"] = self.strings["authorizedListTokens"].format(
            text or "",  # Display empty string instead of the None.
            page + 1,  # Display page, range starts from 1.
            math.ceil((total or 1) / page_size),
        )
        context["buttons"] = buttons
        # Store for reuse:
        self.user["context"]["tokens_menu"]["total_approx"] = total
        self.send(context)

    async def get_page(self, identity: str, page: int, page_size: int):
        # Clear space for tokens in the context.
        self.user["context"]["tokens_menu"]["current_page"].clear()

        # Query tokens from db
        tokens, total = await self.db.query_access_tokens(identity, page, page_size)
        text, buttons = None, []  # type: ignore [var-annotated]
        row_size = 3
        for row_n, row in enumerate(iter_sliced(tokens, row_size)):
            tmp = []
            for i, item in enumerate(row):
                line = self.strings["authorizedListToken"].format(
                    page * page_size + row_n * row_size + i + 1,
                    item["token"][:10], item["identity"],
                )
                text = line if text is None else text + line
                tmp.append({"text": item["token"][:10]})

                # Store tokens in the context.
                self.user["context"]["tokens_menu"]["current_page"][item["token"][:10]] = item["token"]

            buttons.append(tmp)

        buttons.append([{"text": p} for p in self.get_keys()])
        return text, total, buttons

    def get_keys(self):
        return (self.strings["previous"], self.strings["back"], self.strings["next"])


class AuthorizationUseTokenState(BaseState):
    permission_required = PermissionLevel.ANON

    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["authorizedUseToken"]
        context["buttons"] = [{"text": self.strings["back"]}]
        self.send(context)
        return OK

    async def process(self, context: Context):
        raw_answer = context["message"]["text"]
        button = self.parse_button(raw_answer, verify=(self.strings["back"],))

        if button == "back":
            return BACK

        # Default authorization for the bot.
        elif await self.db.check_access_token(raw_answer):
            self.user["permissions"] = PermissionLevel.DEFAULT
            await self.db.update_token_owner(self.user["identity"], raw_answer)

            context["message"]["text"] = self.strings["authorizedDone"].format(raw_answer[:10])
            context["buttons"] = []
            self.send(context)
            return NOOP

        else:
            context["message"]["text"] = self.strings["authorizedUseBadToken"]
            context["buttons"] = [{"text": self.strings["back"]}]
            self.send(context)
            return OK


class AuthorizationServicesState(BaseState):
    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["authorizedServices"]
        context["buttons"] = [{"text": p} for p in self.get_keys()]
        self.send(context)
        return OK

    async def process(self, context: Context):
        raw_answer = context["message"]["text"]
        button = self.parse_button(raw_answer, verify=self.get_keys())

        if button == "back":                  return BACK
        elif button == "authJira":            return GO_TO_STATE("AuthorizationJiraState")
        elif button == "authGoogleCalendar":  return GO_TO_STATE("AuthorizationGoogleCalendarState")

        return OK

    def get_keys(self):
        return (self.strings["authJira"], self.strings["authGoogleCalendar"], self.strings["back"])


class AuthorizationJiraState(BaseState):
    async def entry(self, context: Context):
        context["message"]["text"] = self.strings["authorizedJiraInit"]
        context["buttons"] = [{"text": self.strings["back"]}]
        self.send(context)
        return OK

    async def process(self, context: Context):
        raw_answer = context["message"]["text"]
        button = self.parse_button(raw_answer, verify=(self.strings["back"],))

        if button == "back":
            return BACK

        error = ""
        if len(data := raw_answer.split()) == 3:
            url, email, token = data
            api = AtlassianAPI(url, email, token)
            try:
                profile = await api.get_profile()
                error = profile.get("errorMessage", "")
            except InvalidURL:        error = "Bad url"
            except ContentTypeError:  error = "Bad url or invalid credentials"
            except ClientError:       error = "Bad credentials"  # default 'error' value

            if not error:
                new_service: ExternalService = {
                    "identity": self.user["identity"],
                    "service": "jira",
                    "url": url,
                    "username": email,
                    "token": token,
                    "credentials": {},
                }
                await self.db.store_external_service(new_service)
                # logging.info("auth obj: %s", await self.db.get_external_service(self.user["identity"], "jira"))

                context["message"]["text"] = self.strings["authorizedJiraSuccess"].format(profile["displayName"])
                context["buttons"] = []
                self.send(context)
                return NOOP
        else:
            error = "Bad input"

        context["message"]["text"] = self.strings["authorizedJiraError"].format(error)
        context["buttons"] = [{"text": self.strings["back"]}]
        self.send(context)
        return OK


class AuthorizationGoogleCalendarState(BaseState):
    async def entry(self, context: Context):
        api = GoogleCalendarAPI()
        url, state = await api.init_auth()
        await self.db.add_partial_login(state, {
            "identity": self.user["identity"],
            "service": "google calendar",
            "url": "",
            "username": "",
            "token": "",
            "credentials": {},
        })

        context["message"]["text"] = self.strings["authorizedGoogleCalendarInit"].format(url)
        context["buttons"] = [{"text": self.strings["back"]}]
        self.send(context)
        return OK

    async def process(self, context: Context):
        raw_answer = context["message"]["text"]
        button = self.parse_button(raw_answer, verify=(self.strings["back"],))

        if button == "back":  return BACK
        return NOOP
