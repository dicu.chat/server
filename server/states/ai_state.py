#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from ..settings import (
    AI_ENABLE_PROMPT_TRANSLATION, AI_ENABLE_RESPONSE_TRANSLATION, AI_SUPPORTED_LANGUAGES, AI_PRIMARY_LANGUAGE, AI_DEFAULT_MODEL,
    PROM_AI_STATE_RESPONSE_TIME, PROM_AI_STATE_USER_MESSAGE_LENGTH, PROM_AI_STATE_AI_MESSAGE_LENGTH, PROM_AI_STATE_ERRORS,
    AI_MAX_INPUT_SIZE,
)
from . import BaseState, Context, OK, END, BACK, GO_TO_STATE
from ..external import MODELS
import logging
import random


class AIState(BaseState):
    # Store conversation up to 50 message total (both user and ai).
    MAX_HISTORY_SIZE = 50

    async def entry(self, context: Context):
        if self.user["context"].pop("enable_ai_message", True):
            context["message"]["text"] = self.strings["aiNotice"]
            context["buttons"] = [{"text": self.strings["back"]}, {"text": self.strings["mainMenu"]}]
            self.send(context)

        # Check for the randomization.
        if self.user["context"].pop("random_ai_model", False):
            # Hack: remove gpt3, but let's keep the list from original hashmap of all models.
            models = list(MODELS)
            models.remove("gpt3")
            model_name = random.choice(models)
            logging.info("Choosen random model due to context preference. Model: '%s'", model_name)
            self.user["context"]["current_ai_model"] = model_name

        else:
            model_name = self.user["context"].get("preferred_ai_model", AI_DEFAULT_MODEL)
            if MODELS.get(model_name):
                self.user["context"]["current_ai_model"] = model_name
                logging.info("Using selected model from the context. Model: '%s'", model_name)
            else:
                logging.warning("Couldn't get ai model '%s' from MODELS (using the default '%s') ...", model_name, AI_DEFAULT_MODEL)
                self.user["context"]["current_ai_model"] = AI_DEFAULT_MODEL

        # Track user history with AI:
        self.user["context"]["aistate"] = {"history": list()}

        # Tracking ai usage metrics.
        self.user["context"]["aistate_talked"] = self.user["context"].get("aistate_talked", False)
        return OK

    async def process(self, context: Context):
        # Alias for text answer
        prompt = str(context["message"]["text"])
        # Parse button to have easy access to intent
        button = self.parse_button(prompt, verify=(self.strings["back"], self.strings["stop"], self.strings["mainMenu"]))

        if button == "back":
            context["message"]["text"] = ""  # why..?
            return BACK

        elif button == "stop":
            return END

        elif button == "mainMenu":
            context["message"]["text"] = "/start"  # Resetting botsociety flow.
            return GO_TO_STATE("QAState")

        # Limit input tokens (chars) size.
        if len(prompt) > AI_MAX_INPUT_SIZE:
            # Sending default error message as response.
            context["message"]["text"] = self.strings["errorAiInputMaxSize"].format(len(prompt), AI_MAX_INPUT_SIZE)
            context["buttons"] = [{"text": self.strings["back"]}, {"text": self.strings["mainMenu"]}]
            self.send(context)
            return OK

        # Dispatching a typing event for the expected message:
        await self.send_event("recording_voice" if context["context"].get("expectingVoice") else "typing", context, 1)

        # Note(andrew): This is just a statistics thing that helps us with keeping track of how much users talked to AI.
        self.user["context"]["aistate_talked"] = True

        # Tracking user prompt size:
        PROM_AI_STATE_USER_MESSAGE_LENGTH.observe(len(prompt))

        # Translate user messages to <AI_PRIMARY_LANGUAGE>, if <AI_ENABLE_RESPONSE_TRANSLATION> is true and user doesn't already speak any of <AI_SUPPORTED_LANGUAGES>.
        if AI_ENABLE_PROMPT_TRANSLATION and self.user["language"] not in AI_SUPPORTED_LANGUAGES:
            prompt = await self.translate(prompt, AI_PRIMARY_LANGUAGE, from_lang = self.user["language"])

        # Here we are using a dictionary containing functions to call into different models by name.
        model_call = MODELS[self.user["context"]["current_ai_model"]]

        # Tracking response time of the model.
        with PROM_AI_STATE_RESPONSE_TIME.time():
            # Query response from the chosen AI model:
            try:  answer = await model_call(self.user["identity"], prompt, self.user["context"]["aistate"]["history"])
            # Note(andrew): There are several potential issues that might occur when calling models from here,
            #     including: url might not be provided in the config, or the url is no longer is working now;
            #     unexpected failure on the side of the model-server, which usually results in 500 http error,
            #     and we might be having issues trying to read response data; it also might be just a planned
            #     restart/upgrade of one of the model images. This is probably not the best way to handle some
            #     of those issues, and we might need to handle some of them without throwing an exception for
            #     a better, more robust functionality. But we can't predict all the issues that might occur, and
            #     this is relatively safe way to still have some response to the user (still responding with a
            #     sensible message despite the issues), and letting them know that something indeed went wrong,
            #     but at the same time showing that other parts of the system is still functioning correctly.  @Robustness
            #                                                                              - andrew, October 12, 2021
            except Exception as e:
                PROM_AI_STATE_ERRORS.inc()
                # Properly logging the error.
                logging.exception(e)

                # Sending default error message as response.
                context["message"]["text"] = self.strings["genericAiFailResponse"]
                context["buttons"] = [{"text": self.strings["back"]}, {"text": self.strings["mainMenu"]}]
                self.send(context)
                return OK  # @UXDecision: Should this be 'OK', 'END', 'BACK', or 'NOOP'?

        # Tracking user prompt size:
        PROM_AI_STATE_AI_MESSAGE_LENGTH.observe(len(answer))

        # Note(andrew): Translate AI messages to user's language, if <AI_ENABLE_RESPONSE_TRANSLATION> is
        #     true and user doesn't already speak any of <AI_SUPPORTED_LANGUAGES>. We are doing this outside
        #     of the error handling, because we return inside the 'except' block, which means we never will
        #     get here unless code went through the 'green' path. If you are going to change anything inside
        #     that block, make sure to still return, because otherwise, when we get here, 'answer' will cause
        #     an error, because it won't be declared/initialized anywhere.
        #                                                                               - andrew, October 12, 2021
        if AI_ENABLE_RESPONSE_TRANSLATION and self.user["language"] not in AI_SUPPORTED_LANGUAGES:
            answer = await self.translate(answer, self.user["language"], from_lang = AI_PRIMARY_LANGUAGE)

        context["message"]["text"] = answer
        context["buttons"] = [{"text": self.strings["back"]}, {"text": self.strings["mainMenu"]}]
        self.send(context)

        # Limit the amount of turnes we allow to talk to the AI. By default limited by our history size.
        if limit := self.user["context"].get("message_ai_limit", self.MAX_HISTORY_SIZE):
            if len(self.user["context"]["aistate"]["history"]) >= limit:
                context["message"]["text"] = self.strings["byeDisengagementAi"]
                # This doesn't make sense here: context["buttons"] = [{"text": self.strings["back"]}, {"text": self.strings["stop"]}]
                self.send(context)
                return END

        return OK
