#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from . import BaseState, Context, NOOP, PermissionLevel


class GetIdState(BaseState):
    has_entry = False
    # Allow anyone to access this.
    permission_required = PermissionLevel.ANON

    async def process(self, context: Context):
        context["message"]["text"] = self.strings["yourIdentity"].format(self.user["identity"])
        context["buttons"] = []
        self.send(context)
        return NOOP
