#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from . import BaseState, Context, OK, END, GO_TO_STATE
import iso639


class LanguageDetectionState(BaseState):
    async def entry(self, context: Context):
        # Do not repeat if context already contains language.
        if not str(context["message"]["text"]).startswith("/language") and self.user["context"].get("language", False):
            # Don't store this state, it spams state history with states that are skipped.
            if self.user["states"]:  self.user["states"].pop()
            return GO_TO_STATE("QAState")

        self.user["context"]["bq_state"] = 1
        # Special case for telegram-like client side language code entity
        if self.user["context"].get("language_state") is None and context["user"]["langCode"]:
            lang = iso639.find(context["user"]["langCode"])
            if lang and lang["name"] != "Undetermined":
                # Update current context
                self.user["language"] = lang["iso639_1"]
                self.user["context"]["language"] = lang["iso639_1"]
                self.set_language(lang["iso639_1"])

                # Ask if user wants to continue with the language
                context["message"]["text"] = self.strings["appConfirmLanguage"].format(lang["native"])
                context["buttons"] = [
                    {"text": self.strings["yes"]},
                    {"text": self.strings["no"]},
                    {"text": self.strings["stop"]},
                ]

                self.user["context"]["language_state"] = 4
                self.send(context)
                return OK

        # Send language message
        context["message"]["text"] = self.strings["chooseLang"]
        # Add confirmation button to skip
        if context["user"]["langCode"]:
            self.user["context"]["lang_code"] = context["user"]["langCode"]
            context["buttons"] = [{"text": self.strings["skipLang"].format(context["user"]["langCode"])}]
        # Set user context as 'Was Asked Language Question'
        self.user["context"]["language_state"] = 1
        # Don't forget to add task
        self.send(context)
        return OK

    async def process(self, context: Context):
        raw_answer = context["message"]["text"]
        failed = True

        # Never translate "try_again", because user might not understand new language.
        try_again_en = self.strings["tryAgain"]
        try_again_en.set_language("en")
        # Try filling it from cache.. We need this method to be called manually,
        # because we need to fill english version of the string from cache even
        # when user's language is set to something else but english.
        self.strings.fill_from_cache(try_again_en)

        button = self.parse_button(raw_answer, try_first=(try_again_en,))

        # Jump from current state to final `end` state
        if button == "stop":  return END

        if self.user["context"].get("language_state") == 1:
            # Autodetection button
            if self.parse_fstring(raw_answer, self.strings["skipLang"]):
                # Use current lang, if none -> use from cache
                #    (May make sense if some front end sends lang data only once on first request)
                if context["user"]["langCode"]:  lang = context["user"]["langCode"]
                else:                            lang = self.user["context"]["lang_code"]

                self.user["language"] = lang
                self.user["context"]["language"] = lang
                self.set_language(lang)
                # Send language info message.
                await self.send_language_choice(context)
                return GO_TO_STATE("QAState")

            # Run language detection
            language_obj = await self.nlu.detect_language(raw_answer)
            # Couldn't detect any language
            if language_obj is None:
                # Ask if user wants to continue with the language
                context["message"]["text"] = self.strings["failedLanguage"].format(raw_answer)
                context["buttons"] = [{"text": self.strings["stop"]}]
                self.send(context)
                return OK

            elif isinstance(language_obj, list) and len(language_obj) != 1:
                self.user["context"]["language_state"] = 3
                self.user["context"]["language_obj"] = {lang["native"]: lang["iso639_1"] for lang in language_obj}
                # Ask what language user wants
                context["message"]["text"] = self.strings["chooseLangCountryBased"]
                # Never translate "try again", because user might not understand new language
                context["buttons"] = [
                    *({"text": lang["native"]} for lang in language_obj),
                    {"text": self.strings["stop"]}, {"text": try_again_en},
                ]
                self.send(context)
                return OK

            elif isinstance(language_obj, list):
                language_obj = language_obj[0]

            # Update current context
            self.user["language"] = language_obj["iso639_1"]
            self.user["context"]["language"] = language_obj["iso639_1"]
            self.set_language(language_obj["iso639_1"])

            # Ask if user wants to continue with the language
            context["message"]["text"] = self.strings["confirmLanguage"].format(language_obj["native"])
            context["buttons"] = [
                {"text": self.strings["continue"]},
                {"text": try_again_en},
                {"text": self.strings["stop"]},
            ]
            self.user["context"]["language_state"] = 2
            self.send(context)
            return OK

        elif self.user["context"].get("language_state") == 2:
            if button == "continue":
                # Send language info message.
                await self.send_language_choice(context)
                return GO_TO_STATE("QAState")

            elif button == "tryAgain":
                failed = False
                self.user["language"] = "en"
                self.user["context"]["language"] = "en"
                self.set_language("en")

        elif self.user["context"].get("language_state") == 3:
            if lang := self.user["context"]["language_obj"].get(raw_answer):
                self.user["language"] = lang
                self.user["context"]["language"] = lang
                # Send language info message.
                await self.send_language_choice(context)
                return GO_TO_STATE("QAState")

            elif button == "tryAgain":
                failed = False

        elif self.user["context"].get("language_state") == 4:
            if button == "yes":
                # Send language info message.
                await self.send_language_choice(context)
                return GO_TO_STATE("QAState")

            elif button == "no":
                failed = False

        if failed:
            # Failed to recognise the answer -> fail message path.
            await self.handle_invalid_answer(raw_answer, context, self.strings["invalidAnswer"], history=[])
            self.send(context)

        # Send language message, but now with "Stop" button.
        context["message"]["text"] = self.strings["chooseLang"]
        context["buttons"] = [{"text": self.strings["stop"]}]
        # Set user context as 'Was Asked Language Question'
        self.user["context"]["language_state"] = 1
        # Don't forget to add task
        self.send(context)
        return OK

    async def send_language_choice(self, context: Context):
        context["message"]["text"] = self.strings["languageInfo"]
        context["buttons"] = []
        self.send(context)
