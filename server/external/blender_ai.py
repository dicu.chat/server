#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from ..settings import BLENDER_AI_URL as BASE_URL
import aiohttp
import logging

AI_URL = f"{BASE_URL}/api/get_response"


async def _get_response(entity: str, prompt: str, history: list) -> str:
    async with aiohttp.ClientSession() as session:
        async with session.post(AI_URL, json={"user_id": entity, "prompt": prompt, "history": history}) as r:
            res = await r.json()

            # TODO: explain.
            try:   return res["text"]
            except KeyError as e:
                logging.info("Erroneus response: %s ...", res)
                raise e


async def get_response_blender(entity: str, prompt: str, history: list) -> str:
    answer = await _get_response(entity, prompt, history)
    history.append(prompt)
    history.append(answer)
    return answer
