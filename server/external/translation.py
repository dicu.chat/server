#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from ..settings import CLOUD_TRANSLATION_API_KEY
from aiohttp import ClientSession
import argparse
import logging
import asyncio
import ujson
import re


class TranslationAPI:
    BASE_URL = "https://translation.googleapis.com/language/translate/v2"
    TRANSLATION_URL = f"{BASE_URL}?key={CLOUD_TRANSLATION_API_KEY}"
    DETECTION_URL = f"{BASE_URL}/detect?key={CLOUD_TRANSLATION_API_KEY}"
    HEADERS = {
        "Content-Type": "application/json; charset=utf-8",
    }
    INSIDE_SPAN = re.compile(r"<span.*?>([\w\W]*?)<\/span>")

    def __init__(self):
        self.key = CLOUD_TRANSLATION_API_KEY

    async def __get_json(self, url: str, data: dict, headers: dict):
        async with ClientSession() as session:
            async with session.post(url, json=data, headers=headers) as response:
                return await response.json()

    async def detect_language(self, text: str):
        # Failsafe handling of the case when the key is not provided:
        if len(self.key) != 39:
            return "en"

        data = {"q": text}
        result = await self.__get_json(self.DETECTION_URL, data, self.HEADERS)
        # Schema: [[{LANG}], ...], where LANG: {"language": "en"}
        # If not 'data' (we failed - default to english).
        if "data" not in result:
            logging.warning("Google Translation API 'detect_language' failed: %s", result)
            return "en"

        return result["data"]["detections"][0][0]["language"]

    async def translate_text(self, text: str, target: str, from_lang: str = "en") -> str:
        # Failsafe handling of the case when the key is not provided:
        if len(self.key) != 39:
            return await self.shim_translate_text(text, target, from_lang)

        # Here we need to ensure that target and source are different, otherwise google will return an error.
        assert target != from_lang

        data = {
            "source": from_lang,
            "target": target,
            "format": "text",
            "q": text,
        }
        # logging.info("Google translate request: %s", data)
        result = await self.__get_json(self.TRANSLATION_URL, data, self.HEADERS)
        # logging.info("Google translate result: %s", result)
        if "data" not in result:
            logging.warning("Google Translation API 'detect_language' failed: %s", result)
            return text

        return result["data"]["translations"][0]["translatedText"]

    async def shim_translate_text(self, text: str, target: str, from_lang: str = "en") -> str:
        """
        Return a shim translation, for debugging and testing.
        It is easily identifiable without needing cloud translations
        """
        return f"{text} [{from_lang}->{target}]"

    async def translate_dict(self, target: str, texts: dict, from_lang: str = "en"):
        tasks = await asyncio.gather(*(self.translate_text(text, target) for text in texts.values()))

        return {key: tasks[i] for i, key in enumerate(texts)}

    async def translate_promises(self, promises, source):
        tasks = await asyncio.gather(*(self.translate_text(source[p.source_language][p.key]["text"], p.lang, p.source_language) for p in promises))

        for i, p in enumerate(promises):
            # Storing variable for in-place update.
            new_text = tasks[i]

            original_text = source[p.source_language][p.key]["text"]
            # I love you, Google employee (https://issuetracker.google.com/issues/119256504).
            for m_old, m_new in zip(self.INSIDE_SPAN.finditer(original_text), self.INSIDE_SPAN.finditer(new_text)):
                new_text = new_text.replace(m_new.group(0), m_old.group(1))

            p.fill(new_text)

    async def translate_to_multiple_languages_dict(self, languages: list, source_dict: dict, from_lang: str = "en"):
        tasks_group = asyncio.gather(
            *[self.translate_dict(lang, source_dict, from_lang) for lang in languages]
        )
        res = await tasks_group
        return {lang: res[i] for i, lang in enumerate(languages)}

    async def translation(self, source_file: str, from_lang: str, languages: list, output_file: str):
        with open(source_file) as f:
            source = ujson.load(f)

        results = await self.translate_to_multiple_languages_dict(languages, source, from_lang)
        result_json = {from_lang: source, **results}

        with open(output_file, "w") as output:
            ujson.dump(result_json, output, indent=4)

        logging.info("Translation done!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="translation",
        description="Translate file into multiple languages.",
    )
    parser.add_argument(
        "file_input", type=str, help="input file to translate from"
    )
    parser.add_argument(
        "language_input", type=str, help="language to translate from"
    )
    parser.add_argument(
        "languages_output", type=str, nargs="+", help="languages to translate input file to",
    )
    parser.add_argument(
        "output_file", type=str, help="output file for the translations"
    )

    tr = TranslationAPI()

    args = parser.parse_args()
    asyncio.run(tr.translation(
        args.file_input,
        args.language_input,
        args.languages_output,
        args.output_file,
    ))
    # [EXAMPLE]:
    # This example translates file 'strings.json', which contains serialized dict in english, into file with many languages.
    #    $ python translation.py ../strings/json/strings.json en de ru es fr uk ../strings/json/all_strings.json
