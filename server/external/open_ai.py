#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
import asyncio
import openai

from ..settings import OPENAI_MODEL, OPENAI_MAX_TOKENS


def _get_filter_label(response):
    # Code provided from: https://beta.openai.com/docs/engines/content-filter
    output_label = response["choices"][0]["text"]

    # This is the probability at which we evaluate that a "2" is likely real
    # vs. should be discarded as a false positive
    toxic_threshold = -0.355

    if output_label == "2":
        # If the model returns "2", return its confidence in 2 or other output-labels
        logprobs = response["choices"][0]["logprobs"]["top_logprobs"][0]

        # If the model is not sufficiently confident in "2",
        # choose the most probable of "0" or "1"
        # Guaranteed to have a confidence for 2 since this was the selected token.
        if logprobs["2"] < toxic_threshold:
            logprob_0 = logprobs.get("0", None)
            logprob_1 = logprobs.get("1", None)

            # If both "0" and "1" have probabilities, set the output label
            # to whichever is most probable
            if logprob_0 is not None and logprob_1 is not None:
                if logprob_0 >= logprob_1:
                    output_label = "0"
                else:
                    output_label = "1"
            # If only one of them is found, set output label to that one
            elif logprob_0 is not None:
                output_label = "0"
            elif logprob_1 is not None:
                output_label = "1"

            # If neither "0" or "1" are available, stick with "2"
            # by leaving output_label unchanged.

    # if the most probable token is none of "0", "1", or "2"
    # this should be set as unsafe
    if output_label not in ["0", "1", "2"]:
        output_label = "2"

    return output_label


async def _get_response(entity: str, query: str) -> str:
    def _get(query: str):
        resp = openai.Completion.create(
            prompt = query,
            engine = OPENAI_MODEL,
            temperature = 0.9,
            max_tokens = OPENAI_MAX_TOKENS,
            top_p = 1,
            frequency_penalty = 0.0,
            presence_penalty = 0.6,
            stop = ("\n", " Human:", " AI:"),
            user = entity,
        )
        resp_text = resp.choices[0].text.strip()

        # Implement OpenAI Content Filter to exclude all 'unsafe' (CF=2) output
        filter_resp = openai.Completion.create(
            engine = "content-filter-alpha",
            prompt = "<|endoftext|>" + resp_text + "\n--\nLabel:",
            temperature = 0,
            max_tokens = 1,
            top_p = 0,
            logprobs = 10,
            user = entity,
        )

        if _get_filter_label(filter_resp) == "2":
            return "Response did not pass content filter, please try again."
        else:
            return resp_text

    return await asyncio.get_event_loop().run_in_executor(None, _get, query)


async def get_response_gpt3(entity: str, prompt: str, history: list) -> str:
    # Use preambule to the text so model can better understand what to do:
    preambule = "The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.\n\n"
    # Prepare history:
    history.append("Human: ")
    # Add prefix for the AI to see and finish:
    history.append("AI:")

    # OpenAI only lets us send 1000 characters as a query, so we need to figure out how of the prompt we can send
    unprepared_query = preambule + "\n".join(history)
    remaining_chars = 1000 - len(unprepared_query)
    # Add on as much as we can.
    # Should we warn the user here? - Hasan
    history[0] += prompt.strip(" \n\r\t")[:remaining_chars]

    # Make an api call to the openai.
    answer = await _get_response(entity, preambule + "\n".join(history))
    # Add latest answer to history:
    history.append(f"{history.pop()} {answer}")
    return answer
