#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from typing import Optional, Union, List, TypedDict
from ..settings import RASA_URL, ROOT_PATH
from .translation import TranslationAPI
from aiohttp import ClientSession
import logging
import iso639
import json


class Language(TypedDict):
    iso639_2_b: str
    iso639_2_t: str
    iso639_1: str
    name: str
    native: str


RASA_PATH = ROOT_PATH / "rasa" / "languages" / "csv" / "raw_data" / "country_languages.json"
with open(RASA_PATH) as f:
    available_langs = json.load(f)


class RasaAPI:
    GET_ENTITIES_URL = f"{RASA_URL}/model/parse"

    def __init__(self, translation_api: TranslationAPI):
        self.tr = translation_api

    # Note: Have to add return type check ignore, because gvanrossum bad, don't understand how python works.
    def find(self, value: str) -> Optional[Language]:  # type: ignore [return]
        if (found := iso639.find(value)) and found["name"] != "Undetermined":
            return found

    async def _detect_entities(self, text: str):
        async with ClientSession() as session:
            async with session.post(self.GET_ENTITIES_URL, json={"text": text}) as resp:
                data = await resp.json()

        for each_entity in data["entities"]:
            yield each_entity["value"], each_entity["entity"]

    # Note: Have to add return type check ignore, because gvanrossum bad, don't understand how python works.
    async def detect_language(self, text: str) -> Optional[Union[Language, List[Language]]]:  # type: ignore [return]
        # 1) Try to detect entity using rasa nlu
        async for value, entity in self._detect_entities(text):
            # There might be a false positive from rasa (maybe add some strings comparison later.
            if (entity in ("language", "country", "country_flag")) and (found := self.find(value)):
                logging.info("NLU model detected language: (%s)[%s]", text, found["name"])
                return found

            # Returned country name, so we are taking a mapping file from rasa and using it directly.
            if (entity in ("country", "country_flag")) and (langs := available_langs.get(value)):
                logging.info("NLU model detected country: (%s)[%s]", text, entity)
                # Using filter and generator expression to create a list of a non-None items in one iter.
                return list(filter(None, (self.find(lang) for lang in langs)))

        else:
            # 2) As a fallback, detect what is the language of the speaker.
            language = await self.tr.detect_language(text)
            logging.info("Translator detected language: (%s)[%s]", text, language)

            if found := self.find(language):
                return found

    # This function returns time (isoformat) extracted from text.
    async def detect_datetime(self, text: str) -> Optional[str]:
        async for value, entity in self._detect_entities(text):
            if entity == "time":
                # There is a default case, where date is a string:
                if isinstance(value, str):
                    logging.info("NLU model detected date for 'time' entity (displayed as isoformat): %s ...", value)
                    return value

                # In this case, user provided a range, so dict contains 'from' and 'to'. Let's pick 'from' for now..
                elif isinstance(value, dict) and "from" in value:
                    logging.info("NLU model detected dictionary for 'time' entity, picking 'from' as a default: %s ...", value)
                    return value["from"]

                else:
                    logging.error("Unexpected result from NLU model for 'time' entity: %s (type %s)", value, type(value))
        # If nothing found, we return None.
        return None
