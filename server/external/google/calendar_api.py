#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from datetime import datetime, timedelta
from .base_api import BaseGoogleAPI
import asyncio


class GoogleCalendarAPI(BaseGoogleAPI):
    SCOPES = ["https://www.googleapis.com/auth/calendar.readonly"]

    async def get_events(self, period: timedelta = None):
        def _get_events(period: timedelta = None):
            time_now = datetime.utcnow().isoformat() + "Z"  # 'Z' indicates UTC time
            if period is not None:
                period = (datetime.utcnow() + period).isoformat() + "Z"  # type: ignore [assignment]

            return self.service.events().list(
                calendarId = "primary", orderBy = "startTime",
                timeMin = time_now, timeMax = period,
                maxResults = 15, singleEvents = True,
            ).execute().get("items", [])

        return await asyncio.get_event_loop().run_in_executor(
            None, _get_events, period,
        )

    async def iter_events(self, period: timedelta = None):
        for event in await self.get_events(period):
            yield event
