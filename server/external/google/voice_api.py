#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from google.cloud import texttospeech
from google.cloud import speech
from pydub import AudioSegment
from bs4 import BeautifulSoup
from io import BytesIO
import asyncio

from .base_api import BaseGoogleAPI


class GoogleTextAndSpeechAPI(BaseGoogleAPI):
    def __init__(self, *args, **kwargs):
        # Here we call outer init first, because we need to set credentials path in the environment.
        super().__init__(*args, **kwargs)

        # Instantiates a text-to-speech client.
        self.tts_client = texttospeech.TextToSpeechClient()
        # Instantiates a speech-to-text client.
        self.stt_client = speech.SpeechClient()
        # Select the type of audio file you want returned (returning popular OGG_OPUS mode, used by telegram, or MP3).
        self.audio_config = texttospeech.AudioConfig(
            audio_encoding=texttospeech.AudioEncoding.MP3
        )

    async def text_to_speech(self, text: str, extension: str, codec: str, lang: str = "en") -> BytesIO:
        def _text_to_speech() -> BytesIO:
            """ Synthesizes speech from the input string of text or ssml.
            Note: ssml must be well-formed according to:
                https://www.w3.org/TR/speech-synthesis/
            """

            # Build the voice request, select the language code ("en-US").
            voice_args = texttospeech.VoiceSelectionParams(
                language_code=lang, ssml_gender=texttospeech.SsmlVoiceGender.FEMALE,
            )

            # Set the text input to be synthesized
            synthesis_input = texttospeech.SynthesisInput(text=BeautifulSoup(text, "lxml").text)

            # Perform the text-to-speech request on the text input with the selected voice parameters and audio file type.
            response = self.tts_client.synthesize_speech(
                input=synthesis_input, voice=voice_args, audio_config=self.audio_config
            )

            # The response's audio_content is binary.
            voice = BytesIO()
            voice.write(response.audio_content)
            voice.seek(0)

            # Special case:
            if extension == "mp3":
                return voice

            new_voice = BytesIO()
            AudioSegment.from_file(voice, format="mp3").export(new_voice, format=extension, codec=codec)
            new_voice.seek(0)
            return new_voice

        return await asyncio.get_event_loop().run_in_executor(None, _text_to_speech)

    async def speech_to_text(self, voice: BytesIO, url: str, extension: str, codec: str, lang: str = "en") -> str:
        if not voice and not url:
            raise ValueError("You must provide EITHER 'voice' file bytes OR 'url' to the file.")

        if voice and url:
            raise ValueError("You must provide EITHER 'voice' file bytes OR 'url' to the file. NOT BOTH!")

        if url:
            voice = await self.download_to_memory(url)
            voice.seek(0)

        def _speech_to_text() -> str:
            """Transcribe the given audio file asynchronously."""

            """
            Note that transcription is limited to a 60 seconds audio file.
            Use a GCS file for audio longer than 1 minute.
            """

            # Special case:
            if extension == "mp3":
                audio = speech.RecognitionAudio(content=voice.getvalue())
            else:
                # Converting to WAV:
                mp3 = BytesIO()
                AudioSegment.from_file(voice, format=extension, codec=codec).export(mp3, format="mp3")
                mp3.seek(0)
                audio = speech.RecognitionAudio(content=mp3.getvalue())

            config = speech.RecognitionConfig(
                encoding=speech.RecognitionConfig.AudioEncoding.MP3,  # .OGG_OPUS,
                sample_rate_hertz=16000,
                language_code=lang,
            )

            operation = self.stt_client.long_running_recognize(config=config, audio=audio)
            response = operation.result(timeout=90)

            # Each result is for a consecutive portion of the audio. Iterate through
            # them to get the transcripts for the entire audio file.
            return "\n".join(result.alternatives[0].transcript for result in response.results)

        return await asyncio.get_event_loop().run_in_executor(None, _speech_to_text)


if __name__ == "__main__":
    # Note: to run this file directly, you will need to remove '.' in '.base_api' import.
    async def main():
        api = GoogleTextAndSpeechAPI(service_credentials_path="service_credentials.json")
        # Text-to-speech
        voice = await api.text_to_speech("My name is Andrew Kotikoff. I'm glad to meet you!")
        with open("result.ogg", "wb") as f:
            f.write(voice.getvalue())

        # Speech-to-text  --  Note: This will work terribly, you need to put your own file here.
        # voice = BytesIO()
        # with open("result.ogg", "rb") as f:
        #     voice.write(f.read())
        # text = await api.speech_to_text(voice)

    asyncio.run(main())
