#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from .free_voice_api import FreeGoogleTextAndSpeechAPI
from .voice_api import GoogleTextAndSpeechAPI
from .calendar_api import GoogleCalendarAPI
from typing import Union, Type
import ast
import os

DefaultGoogleTextAndSpeechAPI: Union[Type[FreeGoogleTextAndSpeechAPI], Type[GoogleTextAndSpeechAPI]]

if ast.literal_eval(os.getenv("DEFAULT_FREE_TEXT_AND_SPEECH_API", "True")):
    DefaultGoogleTextAndSpeechAPI = FreeGoogleTextAndSpeechAPI
else:
    DefaultGoogleTextAndSpeechAPI = GoogleTextAndSpeechAPI

__all__ = (
    "GoogleCalendarAPI",
    "FreeGoogleTextAndSpeechAPI",
    "GoogleTextAndSpeechAPI",
    "DefaultGoogleTextAndSpeechAPI",
)
