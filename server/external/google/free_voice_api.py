#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from pydub.exceptions import CouldntDecodeError
import speech_recognition as sr
from pydub import AudioSegment
from bs4 import BeautifulSoup
from io import BytesIO
from gtts import gTTS
import asyncio
import logging

from .base_api import BaseGoogleAPI


class FreeGoogleTextAndSpeechAPI(BaseGoogleAPI):
    def __init__(self, *args, **kwargs):
        self.r = sr.Recognizer()
        super().__init__(*args, **kwargs)

    async def text_to_speech(self, text: str, extension: str, codec: str, lang: str = "en"):
        def _text_to_speech():
            # Text to speech:
            mp3 = BytesIO()
            gTTS(BeautifulSoup(text, "lxml").text, tld="com", lang=lang).write_to_fp(mp3)
            mp3.seek(0)

            # Special case, we don't need to change anything.
            if extension == "mp3":
                return mp3

            # Convert to proper ogg (telegram will recognize the file better):
            voice = BytesIO()
            AudioSegment.from_file(mp3, format="mp3").export(voice, format=extension, codec=codec)
            voice.seek(0)
            return voice

        return await asyncio.get_event_loop().run_in_executor(None, _text_to_speech)

    async def speech_to_text(self, voice: BytesIO, url: str, extension: str, codec: str, lang: str = "en") -> str:
        if not voice and not url:
            raise ValueError("You must provide EITHER 'voice' file bytes OR 'url' to the file.")

        if voice and url:
            raise ValueError("You must provide EITHER 'voice' file bytes OR 'url' to the file. NOT BOTH!")

        if url:
            voice = await self.download_to_memory(url)

        def _speech_to_text() -> str:
            # Special case, we don't need to change anything.
            voice.seek(0)
            if extension != "wav":
                # Converting to WAV:
                wav = BytesIO()
                logging.info("Extension: %s. Codec: %s.", extension, codec)
                try:
                    AudioSegment.from_file(voice, format=extension, codec=codec).export(wav, format="wav")
                except CouldntDecodeError:
                    voice.seek(0)
                    try:
                        AudioSegment.from_file_using_temporary_files(voice, format=extension, codec=codec).export(wav, format="wav")
                    except CouldntDecodeError:
                        logging.error("Failed to parse file!")
                        # Maybe this is bad, but on the other hand we report an error..
                        return "[failed to parse]"

                wav.seek(0)
            else:
                wav = voice

            # Loading file into library format:
            with sr.AudioFile(wav) as source:
                audio_data = self.r.record(source)

            try:
                # Recognition part:
                return self.r.recognize_google(audio_data, language=lang)
            except sr.UnknownValueError as e:
                logging.warning("Speech-to-text engine didn't return any value (error: %s).. replacing text with \"[inaudible]\"", str(e))
                return "[inaudible]"

        return await asyncio.get_event_loop().run_in_executor(None, _speech_to_text)


if __name__ == "__main__":
    # Note: to run this file directly, you will need to remove '.' in '.base_api' import.
    async def main():
        api = FreeGoogleTextAndSpeechAPI()
        # Text-to-speech
        voice = await api.text_to_speech("My name is Andrew Kotikoff. I'm glad to meet you!")
        with open("result.ogg", "wb") as f:
            f.write(voice.getvalue())

        # Speech-to-text  --  Note: This will work terribly, you need to put your own file here.
        # voice = BytesIO()
        # with open("result.ogg", "rb") as f:
        #     voice.write(f.read())
        # text = await api.speech_to_text(voice)

    asyncio.run(main())
