#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import Flow
from io import BytesIO
import aiohttp
import asyncio
import os


class BaseGoogleAPI:
    SCOPES: list[str] = []
    SERVER_HOST: str = os.environ["SERVER_HOST"]
    CALLBACK_URL = f"https://{SERVER_HOST}/api/webhooks/google/auth"

    def __init__(self, credentials: Credentials = None, service = None, service_credentials_path: str = os.environ["CREDENTIALS"]):
        self.credentials = credentials
        self.service = service
        self.service_credentials_path = service_credentials_path

        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = self.service_credentials_path

    def set_credentials(self, credentials: Credentials):
        self.credentials = credentials

    def init_service(self, service: str, version: str):
        self.service = build(service, version, credentials=self.credentials)

    async def init_auth(self, email: str = None, callback_url: str = None) -> tuple[str, str]:
        def _init_flow(email: str, scopes: list[str], credentials_path: str, callback_url: str) -> tuple[str, str]:
            flow = Flow.from_client_secrets_file(
                credentials_path, scopes = scopes, redirect_uri = callback_url,
            )
            # Generate URL for request to Google's OAuth 2.0 server.
            # Use kwargs to set optional request parameters. Returns a tuple of url and state.
            return flow.authorization_url(
                # Enable offline access so that you can refresh an access token without
                # re-prompting the user for permission. Recommended for web server apps.
                access_type="offline",
                # Enable incremental authorization. Recommended as a best practice.
                include_granted_scopes="true",
                # Adding log in hint, if present.
                login_hint = email,
                # Always ask, no caching.
                prompt="consent",
            )

        return await asyncio.get_event_loop().run_in_executor(
            None, _init_flow,
            email, self.SCOPES, self.service_credentials_path, callback_url or self.CALLBACK_URL,
        )

    async def complete_auth(self, code: str, state: str, callback_url: str = None) -> Credentials:
        def _complete_flow(code: str, state: str, scopes: list[str], credentials_path: str, callback_url: str) -> Credentials:
            flow = Flow.from_client_secrets_file(
                credentials_path, scopes = scopes, state = state, redirect_uri = callback_url,
            )
            flow.fetch_token(code = code)
            return flow.credentials

        return await asyncio.get_event_loop().run_in_executor(
            None, _complete_flow,
            code, state, self.SCOPES, self.service_credentials_path, callback_url or self.CALLBACK_URL,
        )

    @staticmethod
    def creds_to_dict(credentials: Credentials) -> dict:
        return {
            "token": credentials.token,
            "refresh_token": credentials.refresh_token,
            "token_uri": credentials.token_uri,
            "client_id": credentials.client_id,
            "client_secret": credentials.client_secret,
            "scopes": credentials.scopes
        }

    @staticmethod
    def dict_to_creds(credentials: dict) -> Credentials:
        return Credentials(**credentials)

    @staticmethod
    async def download_to_memory(url: str) -> BytesIO:
        inmemory_file = BytesIO()
        # Download file to bytes.
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                async for chunk in response.content.iter_any():
                    inmemory_file.write(chunk)

        # Move back to the beginning of the file.
        inmemory_file.seek(0)
        return inmemory_file
