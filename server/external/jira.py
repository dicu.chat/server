#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
import aiohttp
import logging
import base64


class AtlassianAPI:
    def __init__(self, url: str, username: str, token: str):
        self.url = url
        self.username = username
        self.token = token

        self.set_headers()

    def set_headers(self):
        auth = base64.b64encode(f"{self.username}:{self.token}".encode())
        self.headers = {
            "Accept": "application/json",
            "Authorization": f"Basic {auth.decode()}",
        }

    async def get(self, rel_url: str) -> dict:
        async with aiohttp.ClientSession(headers=self.headers) as session:
            async with session.get(f"{self.url}/rest/{rel_url}") as r:
                if r.status != 200:
                    logging.debug("JIRA GET '%s' request error: %s", rel_url, r)
                return await r.json()

    async def get_profile(self) -> dict:
        return await self.get("api/2/myself")

    """
    async def query_issue(self, issue: str) -> dict:
        return await self.get(f"api/2/issue/{issue}")

    async def query_dashboards(self) -> dict:
        return await self.get("api/2/dashboard")
    """

    async def get_projects(self) -> dict:
        return await self.get("api/2/project")

    async def get_issues_with_status(self, user: str, statuses: list[str], projects: list[str], max_results: int = -1) -> list[dict]:
        if statuses:
            status = ",".join(f"\"{value}\"" for value in statuses)
            status = f"+AND+status+IN+({status})"
        else:
            status = ""

        if projects:
            project = ",".join(f"\"{value}\"" for value in projects)
            project = f"+AND+project+IN+({project})"
        else:
            project = ""

        additional = "+ORDER+BY+created+DESC"

        maxResults = "" if max_results == -1 else f"maxResults={max_results}"

        r = await self.get(f"api/2/search?jql=assignee={user}{status}{project}{additional}&{maxResults}")
        return r["issues"]

    async def get_issues_for_user(self, statuses: list[str] = None, projects: list[str] = None, max_results: int = -1):
        user = await self.get_profile()
        issues = await self.get_issues_with_status(user["accountId"], statuses or [], projects or [], max_results)
        return issues


if __name__ == "__main__":
    from pprint import pprint
    import asyncio

    async def main():
        api = AtlassianAPI(input("Url: "), input("User: "), input("Token: "))
        issues = await api.get_issues_for_user(["In Progress", "To Do"])
        for issue in issues:
            pprint(issue["fields"]["status"]["name"])
            exit(1)

    asyncio.run(main())
