#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
import openai

from ..settings import OPENAI_API_KEY

from .google import GoogleCalendarAPI, DefaultGoogleTextAndSpeechAPI, FreeGoogleTextAndSpeechAPI, GoogleTextAndSpeechAPI
from .translation import TranslationAPI
from .rasa import RasaAPI, Language
from .jira import AtlassianAPI

from .dialogpt_ai import get_response_dialogpt
from .blender_ai import get_response_blender
from .custom_ai import get_response_gpt2
from .open_ai import get_response_gpt3

openai.api_key = OPENAI_API_KEY

MODELS = {
    "dialogpt": get_response_dialogpt,
    "blender": get_response_blender,
    "gpt2": get_response_gpt2,
    "gpt3": get_response_gpt3,
}

__all__ = (
    "AtlassianAPI",
    "TranslationAPI",
    "GoogleCalendarAPI",
    "GoogleCalendarAPI",
    "DefaultGoogleTextAndSpeechAPI",
    "FreeGoogleTextAndSpeechAPI",
    "GoogleTextAndSpeechAPI",
    "RasaAPI",
    "Language",
    "get_response",
    "get_response_dialogpt",
    "get_response_blender",
    "get_response_gpt2",
    "get_response_gpt3",
)
