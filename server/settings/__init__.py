#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from .settings import *  # noqa: F403, F401
from .settings import SERVER_SECURITY_TOKEN, DEBUG
# from jaeger_client import Config as JaegerConfig
from prometheus_client import Histogram, Counter
from collections import namedtuple
from typing import Dict
import logging


Config = namedtuple("Config", ["token", "url"])


tokens: Dict[str, Config] = {
    "server": Config(SERVER_SECURITY_TOKEN, ""),
}

# __all__ = settings.__all__ + ("Config", "tokens")


# Logging
formatter = "%(asctime)s - %(filename)s:%(lineno)s - %(levelname)s - %(message)s"
date_format = "%d-%b-%y %H:%M:%S"

logging.basicConfig(
    format=formatter, datefmt=date_format,
    level=logging.DEBUG if DEBUG else logging.INFO
)

# Prometheus metrics stuff, initialize here for easy import into other files.
PROM_PROCESS_TIME = Histogram("servercore_proccessing_time", "Timings of internal processing of the event")
PROM_PROCESS_ERRORS = Counter("servercore_errors_process", "Amount of errors during processing")

_ai_b1 = (1, 2, 4, 8, 16, 32, 64, 128)
PROM_AI_STATE_CONVERSATION_LENGTH = Histogram("servercore_ai_state_conversation_length", "Conversation length between user and ai in the AIState", buckets=_ai_b1)
_ai_b2 = (1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024)
PROM_AI_STATE_USER_MESSAGE_LENGTH = Histogram("servercore_ai_state_user_message_length", "Message length (sent by user) in the AIState", buckets=_ai_b2)
PROM_AI_STATE_AI_MESSAGE_LENGTH   = Histogram("servercore_ai_state_ai_message_length",   "Message length (sent by AI) in the AIState",   buckets=_ai_b2)
PROM_AI_STATE_RESPONSE_TIME       = Histogram("servercore_ai_state_ai_response_time",    "Timings of how long AI response takes")

PROM_AI_STATE_ERRORS = Counter("servercore_errors_ai_state", "Amount of errors in AIState")

PROM_SENDER_ERRORS = Counter("servercore_errors_sender", "Amount of errors during events sending")
