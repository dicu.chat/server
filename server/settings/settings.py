#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
import pathlib
import dotenv
import ast
import os

dotenv.load_dotenv(".env")
# Three steps back from current file
ROOT_PATH = pathlib.Path(os.path.abspath(__file__)).parent.parent.parent

SERVER_SECURITY_TOKEN = os.environ["SERVER_SECURITY_TOKEN"]

CLOUD_TRANSLATION_API_KEY = os.environ["CLOUD_TRANSLATION_API_KEY"]

AI_URL = os.environ["AI_URL"]
BLENDER_AI_URL = os.environ.get("BLENDER_URL", "")
DIALOGPT_AI_URL = os.environ.get("DIALOGPT_URL", "")

DATABASE_URL = os.environ["DATABASE_URL"]

AWS_ACCESS_KEY_ID = os.environ["AWS_ACCESS_KEY_ID"]
AWS_SECRET_ACCESS_KEY = os.environ["AWS_SECRET_ACCESS_KEY"]

RASA_URL = os.environ["RASA_URL"]
BOTSOCIETY_API_KEY = os.environ["BOTSOCIETY_API_KEY"]

SERVER_HOST = os.environ.get("SERVER_HOST", "0.0.0.0")
SERVER_PORT = int(os.environ.get("SERVER_PORT") or 8080)

_server_url = os.environ.get("SERVER_URL")
if _server_url is None:
    if SERVER_PORT:
        SERVER_URL = f"http://{SERVER_HOST}:{SERVER_PORT}"
    else:
        SERVER_URL = f"https://{SERVER_HOST}"
else:
    SERVER_URL = _server_url

DEBUG = bool(ast.literal_eval(os.environ["DEBUG"]))
N_CORES = int(os.environ["N_CORES"])

BOTSOCIETY_LANGUAGE = os.getenv("BOTSOCIETY_LANGUAGE", "en")
DEFAULT_LANGUAGE = os.getenv("DEFAULT_LANGUAGE")

CREDENTIALS = os.environ["CREDENTIALS"]
SERVICE_CREDENTIALS = os.environ["SERVICE_CREDENTIALS"]

MAIN_BOTSOCIETY_FLOW: str = os.getenv("MAIN_BOTSOCIETY_FLOW", "main")

TEST_ALLOW_NETWORK_LOAD = bool(ast.literal_eval(os.environ["TEST_ALLOW_NETWORK_LOAD"]))
TEST_ALLOW_CPU_LOAD = bool(ast.literal_eval(os.environ["TEST_ALLOW_CPU_LOAD"]))

DISABLE_COMMANDS: set[str] = set(os.environ.get("DISABLE_COMMANDS", "").split(","))
DISABLE_ACCESS_TOKENS = bool(ast.literal_eval(os.environ["DISABLE_ACCESS_TOKENS"]))

AI_DEFAULT_MODEL: str = os.environ["AI_DEFAULT_MODEL"]
AI_INVALID_ANSWER_HANDLING: bool = bool(ast.literal_eval(os.environ["AI_INVALID_ANSWER_HANDLING"]))
AI_MAX_INPUT_SIZE: int = int(os.environ["AI_MAX_INPUT_SIZE"])

OPENAI_API_KEY: str = os.environ["OPENAI_API_KEY"]
OPENAI_MODEL: str = os.environ["OPENAI_MODEL"]
OPENAI_MAX_TOKENS = int(os.environ["OPENAI_MAX_TOKENS"])

AI_ENABLE_PROMPT_TRANSLATION = bool(ast.literal_eval(os.environ["AI_ENABLE_PROMPT_TRANSLATION"]))
AI_ENABLE_RESPONSE_TRANSLATION = bool(ast.literal_eval(os.environ["AI_ENABLE_RESPONSE_TRANSLATION"]))
AI_PRIMARY_LANGUAGE = os.environ.get("AI_PRIMARY_LANGUAGE", "en")
AI_SUPPORTED_LANGUAGES: set[str] = set(os.environ.get("AI_SUPPORTED_LANGUAGES", "").split(","))

OWNER_HASH = os.environ.get("OWNER_HASH", None)

__all__ = (
    "ROOT_PATH",
    "SERVER_SECURITY_TOKEN",
    "CLOUD_TRANSLATION_API_KEY",
    "AI_URL",
    "BLENDER_AI_URL",
    "DIALOGPT_AI_URL",
    "DATABASE_URL",
    "AWS_ACCESS_KEY_ID",
    "AWS_SECRET_ACCESS_KEY",
    "RASA_URL",
    "BOTSOCIETY_API_KEY",
    "SERVER_HOST",
    "SERVER_PORT",
    "SERVER_URL",
    "DEBUG",
    "N_CORES",
    "BOTSOCIETY_LANGUAGE",
    "DEFAULT_LANGUAGE",
    "CREDENTIALS",
    "SERVICE_CREDENTIALS",
    "OWNER_HASH",
    "MAIN_BOTSOCIETY_FLOW",
    "TEST_ALLOW_NETWORK_LOAD",
    "TEST_ALLOW_CPU_LOAD",
    "DISABLE_COMMANDS",
    "DISABLE_ACCESS_TOKENS",
    "AI_DEFAULT_MODEL",
    "AI_INVALID_ANSWER_HANDLING",
    "AI_MAX_INPUT_SIZE",
    "OPENAI_API_KEY",
    "OPENAI_MODEL",
    "OPENAI_MAX_TOKENS",
    "AI_ENABLE_PROMPT_TRANSLATION",
    "AI_ENABLE_RESPONSE_TRANSLATION",
    "AI_PRIMARY_LANGUAGE",
    "AI_SUPPORTED_LANGUAGES",
)
