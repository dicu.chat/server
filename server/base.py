#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from .settings import (
    tokens, ROOT_PATH, DISABLE_ACCESS_TOKENS, AI_INVALID_ANSWER_HANDLING, AI_ENABLE_RESPONSE_TRANSLATION,
    AI_SUPPORTED_LANGUAGES, AI_PRIMARY_LANGUAGE, AI_ENABLE_PROMPT_TRANSLATION, SERVER_URL,
    PROM_PROCESS_ERRORS, PROM_SENDER_ERRORS,
)
from .definitions import Context, SenderTask, ExecutionTask, OK, NOOP
from .utils import CustomEncoder, async_lru_cache, iter_sliced
from .strings import StringAccessor, TextPromise, Button
from typing import Union, Iterable, Optional
from .external import get_response_gpt2
from .db import User, PermissionLevel
from collections import defaultdict
from aiohttp import ClientSession
from secrets import token_hex
from itertools import chain
import aiofiles
import asyncio
import logging
import base64
import copy
import json
import os
import io


class BaseState(object):
    """
    This class is a parent-class for all state handlers, it provides:
        - interface to Google Translations API
        - interface to our own phrases/responses
        - automatically translates our strings to the language of the user
        - automatically adds pictures to the chosen intents/conversation steps/questions
        - interface to the database
        - interface to the NLU (Natural Language Understanding unit - https://gitlab.com/dicu.chat/rasa)
        - automatic requests queueing
        - automatic database updates for the `User` object

    Server will pick up files and extract state classes from them, you don't need
    to worry about "registering state" - there is no hardcoded list.
    The important detail is that you **must** put your state file
    to the $(PROJECT_ROOT)/server/states folder.

    You *must* name each class `$(FEATURE_NAME)State`
    You *should* put each class in it's own file.
    You *should* name each file using lower_case $(FEATURE_NAME). Optionally add `_state` in the end.
    """

    HEADERS = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {tokens['server'].token}",
    }
    # This variable allows to ignore `entry()` when needed
    has_entry = True
    # This variable defines which users (with which permissions) can access the state.
    permission_required = PermissionLevel.ANON if DISABLE_ACCESS_TOKENS else PermissionLevel.DEFAULT

    # Prepare state
    def __init__(self, db, tr, nlu, strings, bots_data, user, handler, free_tas, tas, gtasks):
        self.db = db
        self.tr = tr
        self.nlu = nlu
        self.STRINGS = strings

        self.free_tas = free_tas
        self.tas = tas

        self.user = user

        self._handler = handler

        # Data buffer that is assigned to when the class is initialized,
        # stores reference to the relevant Botsociety Data
        self.bots_data = bots_data
        # Keeps list of tasks
        self.tasks = list()
        self.random_tasks = list()
        self.delayed_tasks = list()
        self.global_tasks = gtasks

        self.tasks_present = False
        self.delay_cursor = 0
        self.delay_chars_per_second = None

        # Keeps execution queue
        self.execution_queue = list()
        # Create language variable
        self.__language = self.user["language"]
        self.strings = StringAccessor(self.__language, self.STRINGS)
        # Sometimes we need access to original strings directly.
        self.en_strings = StringAccessor("en", self.STRINGS)

        # Media path and folder
        self.media_folder = "media"
        self.media_path = os.path.join(ROOT_PATH, self.media_folder)

        if not os.path.exists(self.media_path):  os.mkdir(self.media_path)

    def set_language(self, value: str):
        """
        This method sets language to a current state
        If language is None - base language version is english

        Args:
            value (str): language code of the user's country
        """
        if value:
            self.__language = value
            self.strings = StringAccessor(self.__language, self.STRINGS)

    def set_reading_speed(self, chars_per_second: int):
        self.delay_chars_per_second = chars_per_second

    async def wrapped_entry(self, context: Context):
        """
        This method is executed when user enters State for the first time, if `has_entry` variable is set to True.
        It is a wrapper for state-author-controlled `entry` method.

        Args:
            context (Context): holds parsed and verified request, with auto-filled default values
            user (User): user object that is stored directly in the database
        """
        # Forbid access if permissions are insufficient.
        if self.permission_required > self.user["permissions"]:  return NOOP

        # @Note(andrew): Update last active field in the user, which we can use for usage
        #      statistics later ("active users last 1/7/30 days"). We do not add any calls
        #      into the database here to update the field, because in most of the cases we
        #      'commit' user with all updated fields after processing, unless there was an
        #      error. So, instead of doing any direct calls (even if they will be created
        #      as task in the background), we just rely that in most of the "green paths"
        #      cases user will be commited ('commit' has to run most of the time, because
        #      we modify user state a lot and much of the logic depends on it).  @Robustness
        self.user["last_active"] = self.db.now().isoformat()

        if context["message"]["voice"]:  await self.speech_to_text(context)
        # This raw input is important since we are going to use it after processing the
        # request, in case of failure. There might be a situation where text was modified
        # but something else failed after that, so we want to be sure that we are feeding
        # gpt2 a real user input.
        #
        # Note, we do this weird 'str(' stuff due to the fact that we might've entered this
        # state not from a user, but from the previous state, or something like that (In
        # that case, by the way, we are still not using a true user input. This might be
        # resolved in the future if we decide to store a copy of initial user input in
        # something like ['message']['raw_text'] or ['raw_input']).  @Robustness
        raw_user_input = str(context["message"]["text"])

        # Wrap base method to avoid breaking server
        try:  status = await self.entry(context)
        except Exception as e:
            PROM_PROCESS_ERRORS.inc()
            # Log exception
            logging.exception(e)
            # Do not commit to database if something went wrong
            status = OK(commit=False)

            # Note(andrew): Since we know there was a fatal error during processing, we can try sending
            #     an error message indicating that server encountered problems. This might be weird if
            #     we are sending multiple messages so we might want to differentiate those cases later.  @Robustness
            #
            #     For now lets just cancel whatever partial response we did.
            self.tasks.clear()
            self.random_tasks.clear()
            self.delayed_tasks.clear()

            await self.handle_invalid_answer(
                raw_user_input, context, self.strings["fatalStateProcessingError"],
                ["There is some internal error and user is confused. You need to explain them that some error happened."]
            )
            self.send(context)

        # Add an assert about status code, since this will fail as a bad error later, but here we have an opportunity to explain that.  @Robustness
        assert status is not None, f"Status code value returned from the '{self.__class__.__name__}.entry()' method is None!" \
            " This probably happened because you had some code path in the state's logic missing 'return <status_code>'."

        # Commit changes to database
        if status.commit:          asyncio.create_task(self.db.commit_user(user=self.user))
        # @Important: Fulfill text promises
        if self.strings.promises:  await self.strings.fill_promises()  # @Speed: Can we do this asynchronously?
        # @Important: Since we call this always, check if the call is actually needed
        if self.tasks_present:     asyncio.create_task(self._collect())
        # @Important: Execute all queued jobs
        if self.execution_queue:   asyncio.create_task(self._execute_tasks())

        return status

    async def wrapped_process(self, context: Context):
        """
        This method is executed when user enters State for second or any consequent time,
        or for the first time if `has_entry` variable is set to False.
        It is a wrapper for state-author-modified `process` method.

        Args:
            context (Context): holds parsed and verified request, with auto-filled default values
            user (User): user object that is stored directly in the database
        """
        # Forbid access if permissions are insufficient.
        if self.permission_required > self.user["permissions"]:  return NOOP

        self.user["last_active"] = self.db.now().isoformat()

        if context["message"]["voice"]:  await self.speech_to_text(context)
        raw_user_input = str(context["message"]["text"])

        # Wrap base method to avoid breaking server
        try:  status = await self.process(context)
        except Exception as e:
            PROM_PROCESS_ERRORS.inc()
            logging.exception(e)
            status = OK(commit=False)

            # Error handling.
            self.tasks.clear()
            self.random_tasks.clear()
            self.delayed_tasks.clear()

            await self.handle_invalid_answer(
                raw_user_input, context, self.strings["fatalStateProcessingError"],
                ["There is some internal error and user is confused. You need to explain them that some error happened."]
            )
            self.send(context)

        # Add an assert about status code, since this will fail as a bad error later, but here we have an opportunity to explain that.  @Robustness
        assert status is not None, f"Status code value returned from the '{self.__class__.__name__}.process()' method is None!" \
            " This probably happened because you had some code path in the state's logic missing 'return <status_code>'."

        # Commit changes to database
        if status.commit:          asyncio.create_task(self.db.commit_user(user=self.user))
        # @Important: Fulfill text promises
        if self.strings.promises:  await self.strings.fill_promises()  # @Speed: Can we do this asynchronously?
        # @Important: Since we call this always, check if the call is actually needed
        if self.tasks_present:     asyncio.create_task(self._collect())
        # @Important: Execute all queued jobs
        if self.execution_queue:   asyncio.create_task(self._execute_tasks())

        return status

    # Actual state method to be written for the state
    async def entry(self, context: Context):
        """
        The method handles each interaction when user enters your state

        Args:
        context (Context): context object of the request
        user (User): user object from database corresponding to the user who sent message
        db (Database): database wrapper object
        """
        return OK

    # Actual state method to be written for the state
    async def process(self, context: Context):
        """
        The method handles each interaction with user (except first interaction)

        Args:
        context (Context): context object of the request
        user (User): user object from database corresponding to the user who sent message
        db (Database): database wrapper object
        """
        return OK

    def parse_button(
        self, raw_text: str, truncated: bool = False, truncation_size: int = 20,
        verify: Iterable[TextPromise] = None, try_first: Iterable[TextPromise] = None,
        match_case_insensitive: bool = True,
    ) -> Button:
        """
        Function compares input text to all available strings (of user's language) and if
        finds matching - returns Button object, which has text and key attributes, where
        text is raw_text and key is a key of matched string from strings.json

        Args:
            raw_text (str): just user's message
            truncated (bool): option to look for not full matches (only first `n` characters). Defaults to False.
            truncation_size (int): number of sequential characters to match. Defaults to 20.
            verify (tuple[TextPromise]): a custom object that is used instead of global language object
                                (e.g. you want a match from the list of specific buttons)
        """
        # By default it's useful to match all text lowercase for buttons (intent parsing). It improves user
        # experience and allows for easier application of automated translations / transcriptions of the text.
        if match_case_insensitive:  raw_text = raw_text.lower()

        if try_first is None:  try_first = tuple()

        if verify is None:     verify = tuple()

        special_iter = chain(
            ((p.lang, p.key) for p in try_first), ((p.lang, p.key) for p in verify),
            # Here we additionaly chain all the strings, BUT ONLY WHEN <verify> is not provided.
            tuple() if verify else ((self.__language, key) for key in self.STRINGS[self.__language]),
        )

        lang_iter = ((k, self.STRINGS[lang][k]["text"]) for lang, k in special_iter if k in self.STRINGS[lang])

        btn = Button(raw_text)
        # @Speed: look up tables are always better, but since amount of buttons is at most in range of 10,
        #         there is probably no point. Should try though.
        #                                                                               - andrew, sometime 2020
        #
        #         On the other hand, since all of the stuff above was rewritten since then, and aims to
        #         make this a generator, we don't really waste much space and/or speed.
        #                                                                               - andrew, July 12 2021
        for key, value in lang_iter:
            value = TextPromise.remove_translation_span(value)
            # Matching in a case insensitive way for 'value' as well.
            if match_case_insensitive:  value = value.lower()

            # Look for exact matches in cached buttons:
            if value == raw_text:
                btn.set_key(key)
                break

            # Look for truncated (partial) matches in cached buttons:
            if truncated and len(value) > truncation_size and value[:truncation_size] == raw_text[:truncation_size]:
                btn.set_key(key)
                break

        return btn

    # TODO: This is very broken, need to find a better way to match strings that were altered..
    #                                                                         - andrew, July 12 2021
    # Parse intent of single-formatted string, comparing everything but inserted
    #     Returns True           ->  intent matched
    #     Returns None or False  ->  intent didn't match
    def parse_fstring(self, raw_text: str, promise: TextPromise, item1: str = "{", item2: str = "}"):
        """
        Method lets you compare "filled" f-string with "un-filled" one to identify intent, which is not possible
        with simple `==` comparison, because the f-string is *actually* "filled".

        Compares sub-strings to the "{" char and after the "}" char, exclusively.

        Args:
            raw_text (str): raw input, which is "filled" string
            promise (TextPromise): cached input, "un-filled" string
            item1 (str): object to compare from start to position of it in the strings *exclusively*
            item2 (str): object to compare from its position to the end of the strings *exclusively*
        """
        if promise.value and isinstance(promise.value, str):
            # Find where "{" or "}" should've been, then use it to go one char left or right, accordingly
            if (i1 := promise.value.find(item1)) != -1 and (i2 := promise.value.find(item2)) != -1:
                # Find from the end, so can use negative index
                #    Can't just measure from the start, because there will be inserted text of random length
                i2 = len(promise.value) - i2
                return raw_text[:i1] == promise.value[:i1] and raw_text[-i2 + 1:] == promise.value[-i2 + 1:]
        else:
            logging.error("Failed to parse fstring! Raw Input: '%s'. From: '%s'", raw_text, promise)

    # easy method to prepare context
    def set_data(
        self, context: Context, question: dict, avoid_buttons: list[str] = None,
        back_button: bool = True, stop_button: bool = True, main_button: bool = True,
    ):
        # change value from None to empty list for the "in" operator
        avoid_buttons = avoid_buttons or []

        # set according text
        context["message"]["text"] = self.strings[question["text_key"]]

        # Will be an empty list if question is a 'free answer' type.
        context["buttons"] = [
            {"text": self.strings[button["text_key"]]}
            for button in question["buttons"]
            if button["text_key"] not in avoid_buttons
        ]

        # Add special buttons if necessarily
        if back_button:  context["buttons"] += [{"text": self.strings["back"]}]
        if main_button:  context["buttons"] += [{"text": self.strings["mainMenu"]}]
        if stop_button:  context["buttons"] += [{"text": self.strings["stop"]}]

        # Add files if needed.
        if   media := question.get("image"):  context["files"] = [{"mediaType": "image", "contentType": "url", "content": media}]
        elif media := question.get("video"):  context["files"] = [{"mediaType": "video", "contentType": "url", "content": media}]
        elif media := question.get("gif"):    context["files"] = [{"mediaType": "gif",   "contentType": "url", "content": media}]
        else:                                 context["files"] = []

    # TODO: Find better way with database
    # @Note: What if we do it in non blocking asyncio.create_task (?)
    # @Note: But on the other hand, we can't relay on the file status
    # @Note: for example if next call needs to upload it somewhere
    # @Note: If you deal with reliability and consistency - great optimisation
    async def download_by_url(self, url: str, *file_path):
        """
        Downloads any file to the given directory with given filename
        from the url, in asynchronous way (not-blocking-ish).
        """
        # Make sure file exists or create folder on the path.
        if not self.exists(*file_path[:-1]):  os.mkdir(os.path.join(self.media_path, *file_path[:-1]))

        # Full file path with filename
        filepath = os.path.join(self.media_path, *file_path)
        # Initiate aiohttp sessions, get file
        async with ClientSession() as session:
            async with session.get(url) as response:
                # Open file with aiofiles and start steaming bytes, write to the file
                logging.debug("Downloading file: %s to %s", url, filepath)
                async with aiofiles.open(filepath, "wb") as f:
                    async for chunk in response.content.iter_any():
                        await f.write(chunk)
                logging.debug("Finished download [%s]", filepath)
        return filepath

    # This method saves file for temporary access (10 minutes).
    async def serve_temp_file(self, folder: str, extension: str, bytesio: io.BytesIO) -> str:
        path = os.path.join("/tmp", "app_tempfiles", folder)
        if not os.path.exists(path):  os.makedirs(path, exist_ok=True)

        filepath = os.path.join(path, f"{token_hex(32)}.{extension}")
        async with aiofiles.open(filepath, "wb") as f:
            await f.write(bytesio.getvalue())

        return os.path.join(SERVER_URL, filepath.removeprefix("/"))

    def exists(self, *args):
        """
        Checks for the file in the passed directory/filepath, shortcut for the
        os `exists` and `join` methods. Uses media_path as root path
        """
        return os.path.exists(os.path.join(self.media_path, *args))

    # @Note: we shouldn't abuse translation api because:
    # @Note:     * it's not good enough
    # @Note:     * it takes time to make a call to the cloud
    @async_lru_cache(maxsize=1024)
    async def translate(self, text: str, target: str, from_lang: str = "en") -> str:
        """
        Method is wrapper for translation_text from translation module.
        Simply returns translated text for the target language.
        Good usage example if translating text between users.

        Args:
        text (str): message to translate
        target (str): target language (ISO 639-1 code)
        """
        return await self.tr.translate_text(text, target, from_lang=from_lang)

    # @Important: command to actually send all collected requests from `process` or `entry`
    async def _collect(self):
        with PROM_SENDER_ERRORS.count_exceptions():
            results = list()

            async with ClientSession(json_serialize=lambda o: json.dumps(o, cls=CustomEncoder)) as session:
                # @Important: Since asyncio.gather order is not preserved, we don't want to run them concurrently
                # @Important: so, gather tasks that were tagged with "allow_gather".

                # @Important: Group tasks by the value of "30" for the sake of not hitting front end too hard.
                if self.random_tasks:
                    for group in iter_sliced(self.random_tasks, 30):
                        results.extend(await asyncio.gather(
                            *(self._send(r_task, session) for r_task in group)
                        ))

                tasks_count = defaultdict(lambda: {"count": 0, "context": None})

                for d_task in self.delayed_tasks:
                    identity = d_task.request["user"]["identity"]
                    # @Note(andrew): We also need exception counter internally in the '._delayed_send()', since we dispatch it here.
                    t = asyncio.create_task(self._delayed_send(d_task))

                    self.global_tasks[identity].append(t)
                    tasks_count[identity]["count"] += 1
                    tasks_count[identity]["context"] = d_task.request

                    results.append(None)

                # Executing a 'typing' event for the delayed messages.
                for item in tasks_count.values():
                    await self.send_event("recording_voice" if item["context"]["context"].get("expectingVoice") else "typing", item["context"], item["count"])

                # Send ordinary tasks
                for each_task in self.tasks:
                    res = await self._send(each_task, session)
                    results.append(res)

            return results

    async def _delayed_send(self, task: SenderTask):
        with PROM_SENDER_ERRORS.count_exceptions():
            assert task.delay

            curr_delay = self.delay_cursor

            if isinstance(task.delay, int) or isinstance(task.delay, float):
                self.delay_cursor += task.delay

            elif isinstance(task.delay, str) and task.delay == "auto":
                if text := task.request["message"]["text"]:
                    self.delay_cursor += len(str(text)) / self.delay_chars_per_second

                # What will happen if it's voice? What should happen?  @Robustness
                else:
                    self.delay_cursor += 3.0

            else:
                raise ValueError(f"Unexpected value of task.delay: '{task.delay}' of type '{type(task.delay)}'!")

            await asyncio.sleep(curr_delay)

            async with ClientSession(json_serialize=lambda o: json.dumps(o, cls=CustomEncoder)) as session:
                await self._send(task, session)

    # @Important: Real send method, takes SenderTask as argument
    async def _send(self, task: SenderTask, session: ClientSession):
        # Takes instance data holder object with the name from the tokens storage, extracts url
        url = tokens[task.service].url

        # Doing text-to-speech stuff here:
        expecting_voice = task.request["context"].get("expectingVoice")
        if expecting_voice:  await self.text_to_speech(task.request)

        # Removing extra info that we don't want to expose.
        try:
            del task.request["user"]["identity"]
            del task.request["serviceIn"]
            del task.request["serviceOut"]
            del task.request["viaService"]
        except KeyError:
            pass

        # Unpack context, set headers (content-type: json etc).
        async with session.post(url, json=task.request, headers=self.HEADERS) as resp:
            # If reached server - log response.
            if resp.status == 200:
                logging.debug("Task (service=%s) - sent successfully..", task.service)
                return
            
            # For the sake of not creating issues with logging, lets monkeypatch outputed value of the voice file.
            if task.request["message"]["voice"] and task.request["message"]["voice"]["contentType"] == "bytes":
                task.request["message"]["voice"]["content"] = task.request["message"]["voice"]["content"][:25]

            logging.error("[ERROR]: Sending task (service=%s, context=%s) status %s", task.service, task.request, resp.status)

    # @Important: `send` method that allows sending payload to the user.
    def send(self, context: Context, to_entity: Union[User, str] = None, allow_gather: bool = False, delay: Optional[Union[str, float, int]] = None):
        """
        Method creates task that sends context['request'] to the
        to_user User after executing your code inside state.

        Args:
          to_entity:    user object to send message to, or just service name
          context:      request context that is sent to the user
          allow_gather: allow random order (better for parallel requests, i.e. many users)
          delay:        number of seconds to wait before sending
        """
        # @Important: [Explanation to the code below]:
        # @Important: maybe add some queue of coroutines and dispatch them all when handler return status (?)
        # @Important: or just dispatch them via asyncio.create_task so it will be more efficient (?)
        # @Important: reasoning:
        # @Important:   simple way:   server -> request1 -> status1 -> request2 -> status2 -> request3 -> status3
        # @Important:     this way:   server -> gather(request1, request2, request3) -> log(status1, status2, status3)
        if to_entity is None:  to_entity = self.user

        if isinstance(to_entity, str):  service = to_entity
        else:                           service = to_entity["via_instance"]

        task = SenderTask(service, copy.deepcopy(context.to_dict()), delay)
        if allow_gather:  self.random_tasks.append(task)
        elif task.delay:  self.delayed_tasks.append(task)
        else:             self.tasks.append(task)

        # Trigger execution of all tasks.
        self.tasks_present = True
        # Remove data from the object to behave as expected when sending message after sending files.  @Robustness
        context["files"] = []

    async def send_event(self, event: str, context: Context, message_count: int = 0):
        if event == "typing" or event == "recording_voice":
            assert message_count > 0

            service = self.user["via_instance"]
            url = tokens[service].url

            data = {"name": event, "data": {"chatId": context["chat"]["chatId"], "messageCount": message_count}}

            async with ClientSession(json_serialize=lambda o: json.dumps(o, cls=CustomEncoder)) as session:
                async with session.post(f"{url}/api/events/actions", json=data, headers=self.HEADERS) as resp:
                    if resp.status == 200:
                        res = await resp.json()
                        logging.debug("Event of '%s' action was sent successfully (service='%s'). Result: %s", event, service, res)
                        return

                    logging.error("Failed to send '%s' action to the '%s'. Probably it doesn't have '%s' events implemented..", event, service, event)
        else:
            raise NotImplementedError(f"Event {event} is not implemented!")

    async def _execute_tasks(self):
        results = await asyncio.gather(
            *[
                exec_task.func(*exec_task.args, **exec_task.kwargs)
                for exec_task in self.execution_queue
            ]
        )
        return results

    def create_task(self, func, *args, **kwargs):
        """
        Method executes async function (with given args and kwargs) immediately after processing state.

        Args:
        func (Async Func): function to be executed
        args (Any): args to be passed into the func
        kwargs (Any): kwargs to be passed into the func
        """
        self.execution_queue.append(ExecutionTask(func, args, kwargs))

    async def handle_invalid_answer(self, prompt: str, context: Context, default: TextPromise, history: list[str]):
        # Note(andrew): Here we want to use gpt2 for intelligent error message (or context-based fun response,
        #     depends how you look at it), so first check if this is enabled at all. Additionally, we want to
        #     check whether user prompt is non-empty, because non-empty prompt is required for the gpt2 model
        #     to produce any meaningful result.
        if AI_INVALID_ANSWER_HANDLING and prompt:
            # Translate user messages to <AI_PRIMARY_LANGUAGE>, if <AI_ENABLE_RESPONSE_TRANSLATION> is true and user doesn't already speak any of <AI_SUPPORTED_LANGUAGES>.
            if AI_ENABLE_PROMPT_TRANSLATION and self.user["language"] not in AI_SUPPORTED_LANGUAGES:
                prompt = await self.translate(prompt, AI_PRIMARY_LANGUAGE, from_lang = self.user["language"])

            # Use GPT2 here always.
            answer = await get_response_gpt2(self.user["identity"], prompt, history)

            # Here we do an additional check, because often AI is not capable of producting anything.
            if not answer:
                # Default invalid answer message.
                context["message"]["text"] = default
                logging.warning("For some reason AI wasn't able to produce any meaningful answer. Using default message fallback ...")
                return

            # Translate AI messages to user's language, if <AI_ENABLE_RESPONSE_TRANSLATION> is true and user doesn't already speak any of <AI_SUPPORTED_LANGUAGES>.
            if AI_ENABLE_RESPONSE_TRANSLATION and self.user["language"] not in AI_SUPPORTED_LANGUAGES:
                answer = await self.translate(answer, self.user["language"], from_lang = AI_PRIMARY_LANGUAGE)

            # Put AI answer as a response:
            context["message"]["text"] = answer
        else:
            # Default invalid answer message, without any processing.
            context["message"]["text"] = default

    async def speech_to_text(self, context):
        # Warn for weird combination of having voice AND text in the message.
        if context["message"]["text"]:
            logging.warning("For some reason message contains voice ('%s') and text ('%s') ...", context["message"]["voice"], context["message"]["text"])

        # Get voice note from the request, if necessary.
        url, _file = None, None
        voice = context["message"]["voice"]
        if voice["contentType"] == "url":
            url = voice["content"]
        else:
            _file = io.BytesIO()
            _file.write(base64.b64decode(voice["content"]))

        context["message"]["text"] = await self.free_tas.speech_to_text(
            _file, url, voice["extension"], voice["codec"], self.user["language"],
        )

    async def text_to_speech(self, context):
        text = context["message"]["text"]
        extension = context["context"].get("wantedExtension", "mp3")
        codec = context["context"].get("wantedCodec")

        voice = await self.tas.text_to_speech(text, extension, codec, self.user["language"])
        context["message"]["voice"] = {
            "mediaType": "voice",
            "contentType": "url",
            "content": await self.serve_temp_file("voice", extension, voice),
            "extension": extension,
            "codec": codec,
        }
