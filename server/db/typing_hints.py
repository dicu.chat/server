#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from typing import TypedDict, Dict, Union, List, Optional, Any


class User(TypedDict):
    user_id:      str
    service:      str
    identity:     str
    via_instance: str
    first_name:   Optional[str]
    last_name:    Optional[str]
    username:     str
    language:     str
    created_at:   str
    location:     Optional[str]
    last_active:  str
    conversation: Optional[str]
    answers:      Dict[str, Any]
    files:        Dict[str, Union[str, list]]
    states:       List[str]
    permissions:  int  # PermissionLevel
    context:      Dict[str, Any]


class CheckBack(TypedDict):
    id: str
    server_mac: str
    identity: str
    chat_id:  int
    request:  str
    send_at:  str
    kwargs:   dict
    state:    str


class AccessToken(TypedDict):
    token:    str
    identity: str


# TODO: Re-work concept of rooms to be more abstract and automatic, allow for creation of "room" with certain permission level (e.g. "TechnicalSupport" or "Doctor").
class Session(TypedDict):
    name:  str
    token: str
    url:   str


# class TranslationItem(TypedDict):
#    text: str
#    language: str
#    result_text: str
#    result_language: str


class StringItem(TypedDict):
    language: str
    string_key: str
    content_hash: str
    text: str
    manual: bool


class ExternalService(TypedDict):
    identity: str
    service: str
    url: str
    username: str
    token: str
    credentials: dict
