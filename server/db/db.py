#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from .typing_hints import Session, StringItem, AccessToken, CheckBack, ExternalService
from ..settings import DATABASE_URL  # , AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY
from typing import List, Iterable, AsyncGenerator, Tuple, Optional, Any
from ..utils import custom_default
from .enums import PermissionLevel
from .create_db import create_db
from .typing_hints import User
import datetime
import logging
import pytz
import uuid
import json

from aiodynamo.models import BatchGetRequest, BatchWriteRequest
from aiodynamo.credentials import Credentials
from aiodynamo.http.aiohttp import AIOHTTP
from aiodynamo.errors import ItemNotFound
from aiodynamo.client import Client, URL
from aiodynamo.expressions import F
from aiohttp import ClientSession


def singleton(cls, *args, **kw):
    instances = {}

    def _singleton():
        if cls not in instances:
            instances[cls] = cls(*args, **kw)
        return instances[cls]

    return _singleton


@singleton
class Database:
    LIMIT_CONCURRENT_CHATS = 100
    TZ = pytz.utc

    def __init__(self, database_url=None, region_name="eu-center-1"):
        self.db_url = database_url or DATABASE_URL
        self.region = region_name

        self.aioclient: ClientSession = None
        self.aiodynamo: Client = None

        # Cache
        self.active_conversations = 0
        self.mac = str(uuid.getnode())

        self.ongoing_logins = dict()

    async def init(self, *args, **kwargs):
        logging.info("Starting up the database client ...")

        self.aioclient = ClientSession()
        self.aiodynamo = Client(AIOHTTP(self.aioclient), Credentials.auto(), self.region, endpoint=URL(self.db_url))

        # Create db tables
        await create_db(self.aiodynamo)

        self.users             = self.aiodynamo.table("Users")
        self.check_backs       = self.aiodynamo.table("CheckBacks")
        self.access_tokens     = self.aiodynamo.table("AccessTokens")
        self.sessions          = self.aiodynamo.table("Sessions")
        self.string_items      = self.aiodynamo.table("StringItems")
        self.external_services = self.aiodynamo.table("ExternalServices")

    # High level methods

    # User methods

    async def create_user(self, user: User):
        await self.users.put_item(item=user)

    async def delete_user(self, identity: str):
        await self.users.delete_item(key={"identity": identity})

    async def get_user(self, identity: str) -> Optional[User]:
        try:
            return await self.users.get_item(key={"identity": identity})
        except ItemNotFound:
            return None

    async def update_user_attribute(self, identity: str, attr_name: str, attr_value: Any):
        await self.users.update_item(
            key={"identity": identity}, update_expression=F(attr_name).set(attr_value)
        )

    async def update_user_append_state(self, identity: str, state: str):
        await self.users.update_item(
            key={"identity": identity}, update_expression=F("states").append([state])
        )

    async def commit_user(self, user: User):
        await self.users.put_item(item=user)

    async def iter_users(self):
        async for user in self.users.scan():
            yield user

    # Checkbacks

    async def create_checkback(self, user: User, request: dict, send_in: datetime.timedelta, kwargs: dict, state: str = "empty"):  # DynamoDB doesn't accept empty strings..
        await self.check_backs.put_item(item={
            "id": str(uuid.uuid4()),
            "server_mac": str(uuid.getnode()),
            "identity": request["user"]["identity"],
            "chat_id": request["chat"]["chatId"],
            "request": json.dumps(request, default=custom_default),
            "send_at": (self.now() + send_in).isoformat(),
            "kwargs": kwargs,
            "state": state,
        })

    async def update_checkback_attribute(self, checkback_id: str, attr_name: str, attr_value: str):
        await self.check_backs.update_item(
            key={"id": checkback_id}, update_expression=F(attr_name).set(attr_value)
        )

    async def iter_checkbacks(self, now: datetime.datetime, until: datetime.datetime) -> AsyncGenerator[CheckBack, None]:
        expr = F("server_mac").equals(self.mac) & F("send_at").between(now.isoformat(), until.isoformat())

        # async for checkback in self.check_backs.query(key_condition=expr):  # TODO
        async for checkback in self.check_backs.scan(filter_expression=expr):
            yield checkback

    async def query_checkbacks_by_chat(self, chat_id: str, page: int, page_size: int) -> Tuple[List[CheckBack], int]:
        checkbacks = []
        async for checkback in self.check_backs.scan(filter_expression=F("chat_id").equals(chat_id)):
            checkbacks.append(checkback)
        return checkbacks[page*page_size : page*page_size + page_size], len(checkbacks)  # noqa: E203, E226

    async def delete_checkback(self, checkback_id: str):
        await self.check_backs.delete_item(key={"id": checkback_id})  # TODO Error handling

    # Bot Access Tokens

    async def create_access_token(self, item: AccessToken):
        await self.access_tokens.put_item(item=item)

    async def delete_access_token(self, token: str):
        # Unauthorize the user.
        try:
            item = await self.access_tokens.get_item(key={"token": token})
            await self.update_user_attribute(item["identity"], "permissions", PermissionLevel.ANON)
        except ItemNotFound:
            pass

        await self.access_tokens.delete_item(key={"token": token})

    async def check_access_token(self, token: str) -> bool:
        try:
            item = await self.access_tokens.get_item(key={"token": token})
        except ItemNotFound:
            return False
        return item.get("identity", "") == "✅"

    async def update_token_owner(self, identity: str, token: str):
        await self.access_tokens.update_item(
            key={"token": token}, update_expression=F("identity").set(identity)
        )

    async def query_access_tokens(self, identity: str, page: int, page_size: int) -> Tuple[List[AccessToken], int]:
        tokens = []
        async for token in self.access_tokens.scan(filter_expression=F("identity").not_equals(identity)):
            tokens.append(token)
        return tokens[page*page_size : page*page_size + page_size], len(tokens)  # noqa: E203, E226

    # Sessions

    async def create_session(self, session: Session):
        await self.sessions.put_item(item=session)

    async def get_session(self, instance_name: str) -> Optional[Session]:
        try:
            return await self.sessions.get_item(key={"name": instance_name})
        except ItemNotFound:
            return None

    async def iter_frontend_sessions(self) -> AsyncGenerator[Session, None]:
        # Just passing through the async iterator from aiodynamo.
        async for session in self.sessions.scan():
            yield session

    # Translation

    async def create_translation(self, item: StringItem):
        await self.string_items.put_item(item=item)

    async def bulk_save_translations(self, items: List[StringItem]):
        await self.aiodynamo.batch_write({
            "StringItems": BatchWriteRequest(items_to_put=items)  # type: ignore [arg-type]
        })

    async def get_translation(self, lang: str, key: str) -> Optional[StringItem]:
        try:
            return await self.string_items.get_item(key={
                "language": lang, "string_key": key
            })
        except ItemNotFound:
            return None

    async def query_translations(self, items: Iterable[Tuple[str, str]]) -> List[StringItem]:
        result = await self.aiodynamo.batch_get({
            "StringItems": BatchGetRequest(keys=[
                {"language": item[0], "string_key": item[1]} for item in items
            ])
        })
        return result.items["StringItems"]  # type: ignore [return-value]

    async def iter_translations(self) -> AsyncGenerator[StringItem, None]:
        # Just passing through the async iterator from aiodynamo.
        async for item in self.string_items.scan():
            yield item

    # External Services

    async def get_external_service(self, identity: str, service: str) -> Optional[ExternalService]:
        try:
            return await self.external_services.get_item(key={
                "identity": identity, "services": service
            })
        except ItemNotFound:
            return None

    async def store_external_service(self, service: ExternalService):
        await self.external_services.put_item(item=service)

    async def add_partial_login(self, state: str, partial_creds: str):
        self.ongoing_logins[state] = partial_creds

    async def get_partial_login(self, state: str):
        return self.ongoing_logins.pop(state, None)

    # Generic time getter for database methods.
    def now(self) -> datetime.datetime:
        return datetime.datetime.now(self.TZ)
