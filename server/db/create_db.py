#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from aiodynamo.models import GlobalSecondaryIndex, KeySchema, KeySpec, KeyType, Throughput, Projection, ProjectionType
import logging


class TableStatus:
    def __init__(self, name: str, status: str = None):
        self.name = name
        self.status = status

    def __repr__(self):
        return f"{self.name}: {self.status}"


async def create_db(client):
    statuses = list()

    async def create_table(*args, **kwargs):
        status = TableStatus(kwargs["name"])
        statuses.append(status)

        if await client.table_exists(kwargs["name"]):
            status.status = "ALREADY EXISTS"
            return

        await client.create_table(*args, **kwargs)
        status.status = "Created Successfully"

    await create_table(
        name="Users",
        throughput=Throughput(5000, 5000),
        keys=KeySchema(KeySpec("identity", KeyType.string)),
    )

    await create_table(
        name="CheckBacks",
        throughput=Throughput(5000, 5000),
        keys=KeySchema(KeySpec("id", KeyType.string)),
        gsis=[
            GlobalSecondaryIndex(
                name="time",
                schema=KeySchema(KeySpec("server_mac", KeyType.string), KeySpec("send_at", KeyType.string)),
                projection=Projection(ProjectionType.all),
                throughput=Throughput(5000, 5000),
            ),
            GlobalSecondaryIndex(
                name="chat",
                schema=KeySchema(KeySpec("chat_id", KeyType.string)),
                projection=Projection(ProjectionType.all),
                throughput=Throughput(5000, 5000),
            )
        ]
    )

    await create_table(
        name="AccessTokens",
        throughput=Throughput(5000, 5000),
        keys=KeySchema(KeySpec("token", KeyType.string)),
        gsis=[GlobalSecondaryIndex(
            name="user_search",
            schema=KeySchema(KeySpec("identity", KeyType.string)),
            projection=Projection(ProjectionType.all),
            throughput=Throughput(5000, 5000),
        )]
    )

    await create_table(
        name="Sessions",
        throughput=Throughput(5000, 5000),
        keys=KeySchema(KeySpec("name", KeyType.string)),
    )

    await create_table(
        name="StringItems",
        throughput=Throughput(5000, 5000),
        keys=KeySchema(KeySpec("language", KeyType.string), KeySpec("string_key", KeyType.string))
    )

    await create_table(
        name="ExternalServices",
        throughput=Throughput(5000, 5000),
        keys=KeySchema(KeySpec("identity", KeyType.string), KeySpec("service", KeyType.string))
    )

    logging.info("Table statuses:\n    %s", "\n    ".join(str(x) for x in statuses))
