#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from sanic.response import json
from sanic import Blueprint
import asyncio

from ..settings import tokens, Config
from ..definitions import Context


def init(handler, db):
    bp = Blueprint("Authorized API Endpoints")

    @bp.middleware("request")
    async def request_validation(request):
        # Using standart authentication method provided by the sanic framework. Token here is a value
        # extracted from the 'Authorization: Token <value>' or 'Authorization: Bearer <value>' headers.
        if not (token := request.token):
            return json({
                "status": 403,
                "message": "Token is missing! You must provide 'Authorization: Token <token>' or 'Authorization: Bearer <token>' header."
            }, status=403)

        if not (service_name := request.headers.get("instance-name")):
            return json({
                "status": 403,
                "message": "Service name is missing! You must provide 'Instance-Name' header, which contains your generated application name."
            }, status=403)

        # First, check for the session data in the in-memory data..
        if not (service := tokens.get(service_name)):
            # ..otherwise, the session might be saved in the database..
            if potential_session := await db.get_session(service_name):
                # ..if it is, we save it to the in-memory storage and set the token to the retrieved values.
                assert potential_session["name"] == service_name

                tokens[potential_session["name"]] = Config(potential_session["token"], potential_session["url"])
                service = tokens.get(service_name)

        if not service or token != service.token:
            return json({"status": 403, "message": "token unauthorized (bad token)"}, status=403)

        # Additional validation for the POST body data.
        if request.method == "POST":
            data = request.json
            if not data or not isinstance(data, dict):
                return json({"status": 403, "message": "expected json"}, status=403)

    @bp.route("/api/process_message", methods=["POST"])
    async def data_handler(request):
        headers = {}
        headers["serviceIn"] = request.headers.get("service-in")
        headers["viaInstance"] = request.headers.get("instance-name")
        headers["serviceOut"] = request.headers.get("service-out")

        if not headers["serviceIn"]:
            return json({
                "status": 403,
                "message": "You must add 'Service-In' header, in which you must "
                           "specify your specific service name. Like 'telegram' or 'web-widget'."
            }, status=403)

        # Build json data into the context object.
        result = Context.from_json(request.json, headers)
        # Verify context `required` attributes.
        if not result.validated:
            return json({"status": 403, "message": result.error}, status=403)

        # Validated object.
        ctx = result.object
        # Create processing task for the event.
        asyncio.create_task(handler.process(ctx))
        # Return context.
        return json({"status": 200}, status=200)

    @bp.route("/api/export/commands", methods=["GET"])
    async def commands_export_handler(request):
        return json({command: value["description"] for command, value in handler.commands.items() if not value["owner_only"]})

    return bp
