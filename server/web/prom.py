#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
# from ..settings import PROM_AI_STATE_CONVERSATION_LENGTH
from prometheus_client import Gauge, generate_latest
from datetime import datetime, timedelta
from sanic.response import text
from sanic import Blueprint


def init(handler, db):
    bp = Blueprint("Prometheus /metrics collection endpoint")

    # Note(andrew): Making sure this is created only once on the start-up.
    total_users  = Gauge("servercore_users", "Total users in the database of the server core")
    total_active = Gauge("servercore_users_last_month", "Users active in the last month")

    total_ai = Gauge("servercore_users_talked_to_ai", "Total users that had conversation with AI")

    # _ai = (1, 2, 4, 8, 16, 32, 64, 128)
    # PROM_AI_STATE_CONVERSATION_LENGTH = Histogram("servercore_ai_state_conversation_length", "Conversation length between user and ai in the AIState", buckets=_ai)

    @bp.route("/metrics")
    async def prometheus_metrics_export(request):
        total_users.set(0)
        total_active.set(0)
        total_ai.set(0)
        # PROM_AI_STATE_CONVERSATION_LENGTH.set()

        # Collecting all users from the database to count.
        async for user in db.iter_users():
            total_users.inc()

            if (db.now() - datetime.fromisoformat(user["last_active"])) < timedelta(days=30):
                total_active.inc()

            if user["context"].get("aistate_talked"):
                total_ai.inc()

            # if aistate := user["context"].get("aistate", {}):
            #     conv_length = len(aistate.get("history", []))
            #     PROM_AI_STATE_CONVERSATION_LENGTH.observe(conv_length)

        data = generate_latest().decode()
        return text(data)

    return bp
