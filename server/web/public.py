#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from sanic.response import json, html, empty
from sanic import Blueprint
import aiohttp
import logging
import secrets

from ..settings import tokens, Config, BOTSOCIETY_API_KEY, ROOT_PATH
from ..external import GoogleCalendarAPI
from ..utils import parse_api, save_file


def init(handler, db):
    bp = Blueprint("Publicly Available Unauthorized API Endpoints")

    @bp.route("/api/webhooks/botsociety")
    async def botsociety_webhook(request):
        # Data needed to use API
        args = request.args
        logging.info("Received a request on botsociety webhook with arguments: %s", args)

        # Get user id and conv id for get request to the Botsociety API.
        headers = {"Content-Type": "application/json", "api_key_public": BOTSOCIETY_API_KEY, "user_id": args.get("user_id", "")}
        conv_id = args.get("conversation_id", "")
        # Get data from api.
        async with aiohttp.ClientSession(headers=headers) as session:
            async with session.get(f"https://app.botsociety.io/apisociety/2.0/npm/conversations/{conv_id}") as resp:
                data = await resp.json()

        # Parse/Save data
        try:   save_file(parse_api(data["messages"]), path=ROOT_PATH / "server" / "archive")
        except Exception as e:
            logging.exception("Error during parsing of the botsociety data: %s", e)
            return json({"status": 500, "message": str(e)}, status=500)

        # Make sure to reload in-memory data / cache
        handler.load_bots_file()
        return empty()

    @bp.route("/api/webhooks/google/auth")
    async def google_authentication_webhook(request):
        # scopes = request.args.get("scope", "").split()
        state = request.args.get("state")
        code = request.args.get("code")

        if not (partial_creds := await db.get_partial_login(state)):
            return json({"status": 400, "message": "invalid google authorization attempt"}, status=400)

        api = GoogleCalendarAPI()
        credentials = await api.complete_auth(code, state)
        api.set_credentials(credentials)
        partial_creds["credentials"].update(api.creds_to_dict(credentials))
        await db.store_external_service(partial_creds)

        return html(
            "<!DOCTYPE html><html lang='en'><meta charset='UTF-8'><body>"
            "<center><big><big><big><big><big><big>"
            "Authorization was successful, now you can return to your chat!"
            "</big></big></big></big></big></big></center>"
            "<script>setTimeout(window.close, 3000);</script>"
            "</body>"
        )

    @bp.route("/api/setup", methods=["POST"])
    async def bridge_setup(request):
        # Verify security token (of the server).
        if (token := request.token) != tokens["server"].token:  return json({"status": 403, "message": "token unauthorized", "token": token}, status=403)

        # Get data from request.
        data = request.json
        # Quit if no data or data is of a bad type.
        if not data or not isinstance(data, dict):  return json({"status": 403, "message": "expected json"})
        # Pull url from request.
        if not (url := data.get("url", "")):        return json({"status": 403, "message": "url invalid", "url": url}, status=403)

        # If its in the cache we take it from there.
        for sname in tokens:
            # For reasons we save the server token in this cache as well so we have to ignore it in here.
            if sname == "server":         continue

            if tokens[sname].url == url:  return json({"status": 200, "name": sname, "token": tokens[sname].token})

        # It was not in the cache, maybe its in the database.
        async for session in db.iter_frontend_sessions():
            # Url is the unique identifier here.
            if session["url"] == url:
                tokens[session["name"]] = Config(session["token"], session["url"])
                return json({"status": 200, "name": session["name"], "token": session["token"]})

        # @Important: Token hex returns n * 2 amount of symbols.
        # Generate new token and name for the instance.
        new_token = secrets.token_hex(20)
        new_name  = secrets.token_hex(10)

        # Create new config object and store it to the cache.
        tokens[new_name] = Config(new_token, url)
        # Store new config object to the database.
        await db.create_session({"name": new_name, "token": new_token, "url": url})
        # Return useful data back to the caller.
        return json({"status": 200, "name": new_name, "token": new_token})

    return bp
