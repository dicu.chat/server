#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from aiohttp import ClientSession, client_exceptions
from sanic import Blueprint
import datetime
import asyncio
import logging
import os

from ..settings import tokens, Config


def init(handler, db):
    bp = Blueprint("Server Routines (Background Tasks)")

    @bp.listener("before_server_start")
    async def load_frontend_sessions_routine(_app, _loop):
        async for session in db.iter_frontend_sessions():
            tokens[session["name"]] = Config(session["token"], session["url"])

    @bp.listener("after_server_start")
    async def propagate_relevant_commands(_app, _loop):
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {tokens['server'].token}",
        }
        data = {name: obj["description"] for name, obj in handler.commands.items() if not obj["owner_only"]}
        async with ClientSession(headers=headers) as session:
            for name, config in tokens.items():
                if name == "server":  continue

                try:
                    async with session.post(f"{config.url}/api/export/commands", json=data) as r:
                        if r.status == 200:
                            logging.debug("Successfully propagated commands to front end: %s", name)
                            continue

                        resp = await r.json()
                        if resp.get("status", -1) == 200:
                            logging.debug("Successfully propagated commands to front end: %s", name)
                            continue

                        logging.warning("Failed to propagate commands to front end: %s. Response status: http %s, body %s", name, r.status, resp.get("status"))
                except client_exceptions.ClientError as e:
                    logging.warning("Failed to propagate commands to front end: %s. Error: %s", name, str(e))

    # Repeatedly clean temp files folder every minute.
    @bp.listener("after_server_start")
    async def clean_app_tempfiles_routine(_app, _loop):
        async def _clean_app_tempfiles_routine():
            tmp_folder = "/tmp/app_tempfiles"
            while True:
                try:
                    await asyncio.sleep(60)

                    for root, _, files in os.walk(tmp_folder):
                        for name in files:
                            fp = os.path.join(root, name)
                            if not os.path.isfile(fp):  continue

                            dt = datetime.datetime.fromtimestamp(os.path.getmtime(fp))
                            if datetime.datetime.now() - dt > datetime.timedelta(minutes=10):
                                logging.info("Detected expired file: %s, removing ...", fp)
                                os.remove(fp)
                except Exception as e:
                    logging.exception(e)

        asyncio.create_task(_clean_app_tempfiles_routine())

    return bp
