#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from typing import TypedDict, Any, Literal, Optional


class User(TypedDict):
    userId: str
    firstName: Optional[str]
    lastName: Optional[str]
    username: Optional[str]
    langCode: Optional[str]


class Chat(TypedDict):
    chatId: str
    name: Optional[str]
    username: Optional[str]


class File(TypedDict):
    # Generic type of the file for frontends to handle better.
    mediaType: Literal["voice", "image", "gif", "video", "document", "location"]
    # Depending on 'content_type' different actions will be required to use operate the file.
    contentType: Literal["bytes", "url"]
    # Content means different things depending on the 'type':
    #   - 'bytes': base64-encoded bytes contents of the voice file
    #   - 'url': 'http://' or 'https://' type url to the file
    content: str
    # Additional attributes for files.
    extension: Optional[str]
    codec: Optional[str]


class Message(TypedDict):
    text: Optional[str]
    # Note: voice is a file with 'mediaType' set to 'voice'.
    voice: Optional[File]


class Button(TypedDict):
    text: str
    # This property aims to notify frontend that the button should be treated as a link, and
    # the content of the link is in the 'value' property.
    buttonType: Optional[Literal["text", "url"]]
    # Contains additional value for the button (e.g. link, in case of 'url' type).
    value: Optional[str]


class RequestBody(TypedDict):
    """This is the main typing class that describes API schema in human-readable python notation."""
    user: User
    chat: Chat
    message: Message
    # Items below are optional, but they represent, at least, an empty object (dict or list).
    files: list[File]
    buttons: list[Button]
    # You can write any additional information to this context when sending a request, so then server response will contain it.
    # Note: Context size is limited to 4KB, if it's more than that, the request will be rejected (TBD).
    context: dict[Any, Any]

    # The stuff below is not a part of a request body, but is provided in headers.
    serviceIn: str  # @NoAPI: Headers
    serviceOut: str  # @NoAPI: Headers
    viaInstance: str  # @NoAPI: Headers


class Headers(TypedDict):
    serviceIn: str
    serviceOut: str
    viaInstance: str
