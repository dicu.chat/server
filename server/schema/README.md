## Schema
This directory contains context schema to refer to while developing or extending frontend and backend APIs.  

### Authentication Headers
  
##### Request Headers
**TODO: Add description**  
  
Token: `Authorization: Token <value>` OR `Authorization: Bearer <value>`  
Service: `Service: <value>`  
Service-In: `Service-In: <value>`  
  
##### Response Headers
**TODO: Add description**  
  
Token: `Authorization: Token <value>` OR `Authorization: Bearer <value>`  
Service: `Service: <value>`  
Service-In: `Service-In: <value>`  
Service-Out: `Service-Out: <value>`  
  
### Request Body (Human readable format)
[**Click to open the file**](./__init__.py)

# [⚠️ Deprecated ⚠️] Context schema
Context schema to refer to while developing or extending front end and back end APIs  
If **any** of the required arguments is not specified, server will return **403**.  

Surely, might be changed at any point during development stage, so before writing responses look here to see relevant info.

[**schema file**](./schema.json)

