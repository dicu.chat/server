#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from ..strings import TextPromise
from decimal import Decimal
import json


class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, TextPromise):
            return str(o)
        elif isinstance(o, Decimal):
            return float(o)
        return json.JSONEncoder.default(self, o)


def custom_default(obj):
    if isinstance(obj, Decimal):
        return float(obj)
    elif isinstance(obj, TextPromise):
        return str(obj)
    raise TypeError


def encode(obj):
    if isinstance(obj, float):
        # Ehhh, we have to use this custom precision here, because otherwise 'Decimal' makes value scientific notation..
        return Decimal(f"{obj:.2f}")
    return obj
