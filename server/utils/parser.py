#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from typing import Tuple, Dict, Optional, List, Union
from ..settings import MAIN_BOTSOCIETY_FLOW
from collections import defaultdict
from .json_utils import encode
import datetime
import logging
import emoji
import ujson
import uuid
import ast
import re
import os


# TODO: reduce the amount of loops in all of this..

# Function is used in the parser, but also is very useful for debugging
# wraps data into better-readable indented json-string
def w(data):
    return ujson.dumps(data, indent=4, ensure_ascii=False)


# Find message by id
# TODO: use dict
def get_msg(data, msg_id):
    for message in data:
        if message["id"] == msg_id:
            return message


# Command pattern (Examples: "#ai", "#end", "#goto(place=here, in=100)")
COMMAND_EXPRESSION = re.compile(r"(?:^|[^\\])#([^\s\(]+)(\([^\)]+\))?")
FORMATTING = re.compile(r"\{.*?\}")


# Parse text for special `#commands`
def get_text(text: str, enforce_partial: bool = False) -> Tuple[str, bool, Dict[str, List[Dict[str, Optional[str]]]]]:
    cmds = COMMAND_EXPRESSION.finditer(text)
    result: Dict[str, List[Dict[str, Optional[str]]]] = defaultdict(lambda: list())

    if enforce_partial:
        result["partial"] = list()

    # After doing search, fix all actual hashtags:
    #     change all '\#' to '#'
    # TODO: the logic about escaping is broken. We should escape using ##
    # and we should remove before deescape
    text = text.replace(r"\#", "#")
    for cmd in cmds:
        # Remove the #command after finding and strip any amount of trailing "\n" or " "
        text = text.replace(cmd.group(0), "").strip("\n ")
        # Some edge case
        if not cmd.group(1):
            continue

        cmd_name = str(cmd.group(1)).lower()
        if cmd_name not in result:
            result[cmd_name] = list()

        _cmd_data = dict()
        args = filter(lambda s: bool(s), (cmd.group(2) or "").strip("()").split(","))
        for arg in args:
            if "=" in arg:
                arg_v = arg.strip()
                index = arg_v.find("=")

                key, value = arg_v[:index].strip(), arg_v[index + 1:].strip()
                try:
                    _literal = ast.literal_eval(value)
                    _cmd_data[key] = encode(_literal)
                except (ValueError, SyntaxError):
                    _cmd_data[key] = value
            else:
                _cmd_data[arg] = None

        if _cmd_data:
            result[cmd_name].append(_cmd_data)

    # Convert to normal dictionary..
    result = dict(result)

    # Automatically add all 'no-translation' tags to "{context_arg}"s.
    for _var in FORMATTING.finditer(text):
        var = _var.group(0)
        text = text.replace(var, f"<span class=\"kw\" translate=\"no\">{var}</span>")

    # Additional parsing.. Convert emojies into their proper rendered form, e.g. ':heart:' into ❤️ .
    text = emoji.emojize(text, variant="emoji_type", use_aliases=True)
    return text, bool(result), result


# Parse Botsociety api data
def parse_api(raw_data):
    # Result list
    result = list()
    # Merging all actions in one loop
    for msg in raw_data:
        # Add our own values
        msg["free_answer"] = False
        # Allow to use both normal buttons and quickreplies
        msg["buttons"] = msg.get("buttons", [])
        msg["multichoice"] = False
        # Generate text key for the translation
        msg["text_key"] = str(uuid.uuid4())[:8]
        msg["commands"] = {}
        msg["expected_type"] = "text"
        msg["text"] = msg.get("text", "")

        enforce_partial = bool(msg.get("video"))

        # Enforce overwrite mimetype to a sane one.
        if (image := msg.get("image")) and image.endswith(".gif"):
            msg["gif"] = image
            del msg["image"]

        # Message that is not quickreply
        if msg["type"] != "quickreplies":
            # Parse text
            text, _, cmds = get_text(msg["text"], enforce_partial=enforce_partial)
            msg["text"] = text
            msg["commands"] = cmds

            next_msg = get_msg(raw_data, msg["next_message"])

            if next_msg:
                # Handle quickreplies as buttons
                if next_msg["type"] == "quickreplies":
                    msg["next_message"] = None
                    msg["buttons"] = next_msg["quickreplies"]
                # Handle user message as free answer
                elif next_msg["is_left_side"] is False:
                    msg["next_message"] = next_msg["next_message"]
                    msg["free_answer"] = True

                    if next_msg["type"] in ("image", "location"):
                        msg["expected_type"] = next_msg["type"]

        # Make sure to keep these statements separate
        if msg["type"] == "quickreplies":
            # TODO: This needs to be handled properly at some point (maybe?).
            continue
        elif not msg["is_left_side"]:
            # Skip
            continue

        # Make sure to add keys for all buttons and do the parsing
        for index, btn in enumerate(msg["buttons"]):
            btn["text_key"] = f"{msg['text_key']}-btn{index}"
            text, _, cmds = get_text(btn["text"])
            btn["text"] = text
            btn["commands"] = cmds

        # Add messages if didn't skip
        result.append(msg)
    logging.info("Parsed new flow from Botsociety.")
    return result


# Default path for parsed file.
DEFAULT_ARCHIVE = os.path.abspath(os.path.join("server", "archive"))


# Save data to the file / reduce history.
def save_file(data, flow_name: str = MAIN_BOTSOCIETY_FLOW, path: Union[str, os.PathLike[str]] = DEFAULT_ARCHIVE, rewrite: bool = True):
    logging.info("Saving to path: '%s' ...", path)

    # Prepare path directory if needed
    if not os.path.exists(path):
        os.mkdir(path)

    flow_data = {}

    # Rename old "latest", if necessary.
    latest_fp = os.path.join(path, "latest.json")
    if os.path.exists(latest_fp):
        if rewrite:
            # Read creation time from file system in Unix timestamp format and isoformat it
            ct = datetime.datetime.fromtimestamp(os.path.getmtime(latest_fp)).isoformat()
            # Move the actual file by renaming
            os.rename(latest_fp, os.path.join(path, f"{ct}.json"))
        else:
            with open(latest_fp, "r") as f:
                flow_data = ujson.load(f)

    flow_data.update({flow_name: data})
    # Write new data "latest".
    with open(latest_fp, "w") as file_latest:
        file_latest.write(w(flow_data))

    # If more than 5 files -> remove oldest
    history = os.listdir(path)
    # Sort by newest first
    history.sort(reverse=True)
    # Make sure not to remove latest file
    history.remove("latest.json")
    # Remove older files, when there are more than 5 total.
    for fp in history[4:]:
        fp = os.path.join(path, fp)
        os.remove(fp)

    logging.info("Updated latest file / archived files.")


# Get first message; next message; next message by the answer
def get_next(items, curr_id=None, answer=None):
    if curr_id is None:
        for msg in items:
            if msg["is_first_message"]:
                return msg
        else:
            raise ValueError("Somehow we dont have first message?")
    else:
        curr = get_msg(items, curr_id)

    if not curr:
        return None

    if curr["buttons"]:
        # find button
        for btn in curr["buttons"]:
            if answer == btn["text_key"]:
                next_id = btn.get("next_message")
                if next_id is not None:
                    return get_msg(items, next_id)
    else:
        return get_msg(items, curr["next_message"])
