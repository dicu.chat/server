#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from .settings import tokens, ROOT_PATH, OWNER_HASH, DISABLE_COMMANDS, MAIN_BOTSOCIETY_FLOW, DEFAULT_LANGUAGE, SERVICE_CREDENTIALS, PROM_PROCESS_TIME, PROM_PROCESS_ERRORS
from .external import AtlassianAPI, TranslationAPI, RasaAPI, GoogleCalendarAPI, DefaultGoogleTextAndSpeechAPI, FreeGoogleTextAndSpeechAPI
from .db import Database, User, PermissionLevel, CheckBack
from .definitions import OK, BACK, GO_TO_STATE, END, NOOP
from datetime import timedelta, datetime
from collections import defaultdict
from .strings import Strings
from .states import collect
import inspect
import asyncio
import aiohttp
import logging
import json
import os


class Handler(object):
    STATES_HISTORY_LENGTH = 10
    LATEST_DATA_PATH = os.path.join(ROOT_PATH, "server", "archive", "latest.json")
    COMMANDS = {
        "/start":     {"owner_only": False, "state": "StartState",             "description": "Starts conversation with the bot"},
        "/language":  {"owner_only": False, "state": "LanguageDetectionState", "description": "Change language for the bot"},
        "/reminder":  {"owner_only": False, "state": "ReminderState",          "description": "Create or manage reminders"},
        "/id":        {"owner_only": False, "state": "GetIdState",             "description": "Display your unique hash string"},
        "/login":     {"owner_only": False, "state": "AuthorizationState",     "description": "Credentials management"},
        "/ai":        {"owner_only": False, "state": "AIState",                "description": "Talk to AI chatbot model"},
        "/stop":      {"owner_only": False, "state": "ENDState",               "description": "End conversation with the bot"},
        "/loadflow":  {"owner_only": True,  "state": "LoadBotSocietyState",    "description": "Use this command to load botsociety by the flow id"},
        "/forget_me": {"owner_only": False, "state": "ForgetMeState",          "description": "Data deletion menu"},
        # "/tokens":    {"state": "AccessTokensMenuState", "description": "Manage existing access tokens"},
        # "/new_access_token": {"state": "NewAccessTokenState", "description": "Create new access token"},
    }
    HEADERS = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {tokens['server'].token}",
    }

    def __init__(self):
        self.__states = {state_class.__name__: state_class for state_class in collect()}
        self.db = Database()
        self.translator = TranslationAPI()
        self.nlu = RasaAPI(self.translator)

        self.free_tas = FreeGoogleTextAndSpeechAPI()
        self.tas = None
        if os.path.exists(SERVICE_CREDENTIALS):  self.tas = DefaultGoogleTextAndSpeechAPI(service_credentials_path=SERVICE_CREDENTIALS)

        self.strings = Strings(self.translator, self.db)
        self.bots_data = None

        self.commands = {k: v for k, v in self.COMMANDS.items() if k not in DISABLE_COMMANDS}

        if os.path.exists(self.LATEST_DATA_PATH):
            self.load_bots_file()
            logging.info("Succesfuly loaded Core Botsociety file.")
        else:
            logging.warning("No Core Botsociety file was found!")

        # TODO: explain
        self.delayed_tasks = defaultdict(list)

    def __get_state(self, name, user: User):
        state = self.__states.get(name)
        if state is None:
            # If non-existing state - send user to the start state
            # @Important: Don't forget to initialize the state
            return (
                False,
                self.__states[self.commands["/start"]["state"]](
                    db        = self.db,
                    tr        = self.translator,
                    nlu       = self.nlu,
                    strings   = self.strings,
                    bots_data = self.bots_data,
                    user      = user,
                    handler   = self,
                    free_tas  = self.free_tas,
                    tas       = self.tas,
                    gtasks    = self.delayed_tasks,
                ),
                self.commands["/start"]["state"]
            )
        # @Important: Don't forget to initialize the state
        return (
            True,
            state(
                db        = self.db,
                tr        = self.translator,
                nlu       = self.nlu,
                strings   = self.strings,
                bots_data = self.bots_data,
                user      = user,
                handler   = self,
                free_tas  = self.free_tas,
                tas       = self.tas,
                gtasks    = self.delayed_tasks,
            ),
            name
        )

    async def __get_or_register_user(self, context) -> User:
        # Getting user from database
        user = await self.db.get_user(context["user"]["identity"])
        if user is None:
            # Special case for the owner to log in, requires an owner identity, and gives maximum permissions..
            is_owner = OWNER_HASH == context["user"]["identity"]
            if is_owner:  permissions = PermissionLevel.MAX
            # ..all other users are assigned lowest permission rights.
            else:         permissions = PermissionLevel.ANON

            # Creating new user
            user = {
                "service":      context["serviceIn"],
                "via_instance": context["viaInstance"],
                "user_id":      context["user"]["userId"],
                "identity":     context["user"]["identity"],
                "first_name":   context["user"]["firstName"],
                "last_name":    context["user"]["lastName"],
                "username":     context["user"]["username"],
                "language":     "en",
                "created_at":   self.db.now().isoformat(),
                "location":     None,
                "last_active":  self.db.now().isoformat(),
                "conversation": None,
                "answers":      dict(),
                "files":        dict(),
                "states":       [self.commands["/stop"]["state"]],
                "permissions":  permissions,
                "context":      dict(),
            }

            # @Document: keep in sync with variable in language state.
            if DEFAULT_LANGUAGE:
                # @Robustness: add check here for correctness and allow different ways
                # to enter language (i.e. 'English'). To achieve this reuse module from
                # language state called 'iso639'.
                user["language"] = DEFAULT_LANGUAGE
                # This doesn't affect an actual language of the user (in what language
                # text is displayed), but tells language state that language is already
                # set.
                user["context"]["language"] = DEFAULT_LANGUAGE

            await self.db.create_user(user)

        # @Important: Dynamically update associated service instance, when it was changed
        if context["viaInstance"] != user["via_instance"]:
            # Update database
            await self.db.update_user_attribute(user["identity"], "via_instance", context["viaInstance"])

        return user

    async def process(self, context):
        # Note(andrew): Debugger stuff here (same pre-commit stuff applies), which allows us to step in into
        #     this function (at the place of '.set_trace()') in the pdb interactive shell:
        # import pdb
        # pdb.set_trace()

        # Tracking processing time with prometheus.
        with PROM_PROCESS_TIME.time(), PROM_PROCESS_ERRORS.count_exceptions():
            await self.__process(context)

    async def __process(self, context):
        # Note(andrew): We have to wrap this into error logging, because this code runs in the daemon thread,
        #     and when something breaks, there is nothing to pick up that error, so we often don't see anything.
        #     There is probably a more broad way to do this, but for now this is fine.  @Robustness
        try:
            # Cancelling older delayed tasks, since we recieved new message from the user.
            if delayed_tasks := self.delayed_tasks[context["user"]["identity"]]:
                for dtask in delayed_tasks:  dtask.cancel()
                delayed_tasks.clear()

            # Getting or registering user.
            user = await self.__get_or_register_user(context)

            # Regular handling for special states.
            if special_state := self.get_command_state(user, context):
                # Here we always want to set 'entry' bool to 'True', because some states might have an 'entry' method.
                await self.__forward_to_state(context, user, special_state, entry=True, is_command=True)
                return

            # Finding last registered state of the user
            last_state = await self.last_state(user)
            # Looking for state, creating state object
            correct_state, current_state, current_state_name = self.__get_state(last_state, user)
            if not correct_state:
                user["states"].append(current_state_name)
                # @Important: maybe we don't need to commit, since we will commit after?
                # await self.db.commit_user(user)

            # Call process method of some state
            ret_code = await current_state.wrapped_process(context)
            await self.__handle_ret_code(context, user, ret_code)
        except Exception as e:
            logging.exception(e)

    def get_command_state(self, user: User, context):
        if text := context["message"]["text"]:
            command = str(text).split()[0]  # Element 0 will always be present since even ''.split() returns [''].
            if item := self.commands.get(command):
                # Note(andrew): Some of the commands that we have are only meant to be used by super-admin (owner)
                #     with highest level of permissions. Those are commands, such as 'reloading botsociety flow by
                #     requesting a newer version' and others. This is important to check here, before we check for
                #     text matching, because this eliminates possibility for abuse.
                if item["owner_only"] and user["permissions"] != PermissionLevel.MAX:  # type: ignore [comparison-overlap]
                    return

                return item["state"]

    # get last state of the user
    async def last_state(self, user: User):
        try:   return user["states"][-1]
        except (IndexError, KeyError):  pass
        # defaults to StartState
        return self.commands["/start"]["state"]

    def isstatus(self, ret_code, status_class):
        return ret_code == status_class if inspect.isclass(ret_code) else isinstance(ret_code, status_class)

    async def __handle_ret_code(self, context, user, ret_code):
        # Handle return codes
        #    OK          -> do nothing, the state is preserved
        #    END         -> go to the ENDState
        #    BACK        -> go to the previous state
        #    GO_TO_STATE -> proceed executing wanted state
        #    NOOP        -> do nothing, states are reset
        if not ret_code.process_next or self.isstatus(ret_code, OK):
            return

        elif self.isstatus(ret_code, GO_TO_STATE):
            await self.__forward_to_state(
                context, user, ret_code.next_state, entry=ret_code.entry
            )
        elif self.isstatus(ret_code, BACK):
            user["states"].pop()
            # Clean up user text input when going back.. it really does cause issues (e.g. 'back' button may trigger infinite loop of going back).
            context["message"]["text"] = ""
            # Make sure that this state looks like a new state (using .pop), so we get into 'entry'.
            await self.__forward_to_state(
                context, user, user["states"].pop(), entry=ret_code.entry
            )
        elif self.isstatus(ret_code, END):
            await self.__forward_to_state(
                context, user, self.commands["/stop"]["state"], entry=ret_code.entry
            )
        elif self.isstatus(ret_code, NOOP):
            user["states"] = [self.commands["/start"]["state"]]
            await self.db.commit_user(user)

    async def __forward_to_state(self, context, user, next_state, entry=False, is_command=False):
        last_state = await self.last_state(user)
        correct_state, current_state, current_state_name = self.__get_state(next_state, user)

        if condition_entry := (current_state_name != last_state or is_command):
            if current_state_name != last_state:
                # Registering new last state
                user["states"].append(current_state_name)
                # @Important: maybe we don't need to commit, since we will commit after?
                # await self.db.commit_user(user)
                # Check if history is too long
                if len(user["states"]) > self.STATES_HISTORY_LENGTH:
                    # @Important: We don't commit this change to db, since we are going to commit anyways.
                    user["states"].pop(0)

        # Reminder: don't 'pop' the state before returning 'BACK', because 'last_state' comparison will break (states will be the same).
        do_entry = (current_state.has_entry and condition_entry and entry)
        if do_entry:  ret_code = await current_state.wrapped_entry(context)
        else:         ret_code = await current_state.wrapped_process(context)

        await self.__handle_ret_code(context, user, ret_code)

    # ~~~~~~~ Reminder/Background tasks ~~~~~~~

    async def reminder_loop(self) -> None:
        try:
            logging.info("Reminder loop started")
            while True:
                now = self.db.now()
                # next_circle = (now + timedelta(minutes=1)).replace(second=0, microsecond=0)
                next_circle = (now + timedelta(seconds=10)).replace(microsecond=0)
                await asyncio.sleep((next_circle - now).total_seconds())
                await self.schedule_nearby_reminders(next_circle)
        # IDK how useful is this.
        except asyncio.CancelledError:  logging.info     ("Reminder loop was cancelled")
        except Exception as e:          logging.exception("Exception in reminder loop: %s", e)

    async def schedule_nearby_reminders(self, now: datetime) -> None:
        until = now + timedelta(seconds=10)
        async with aiohttp.ClientSession() as session:
            tasks = []
            async for checkback in self.db.iter_checkbacks(now, until):
                tasks.append(self.send_reminder(0, checkback, session))  # TODO: Rework
            await asyncio.gather(*tasks)

    async def send_reminder(self, send_at: float, checkback: CheckBack, session: aiohttp.ClientSession):
        try:
            logging.debug("Sending checkback after %s seconds", send_at)
            await asyncio.sleep(send_at)
            logging.debug("Sending checkback")
            await self._send_reminder(checkback, session)
        except Exception as e:  logging.exception("Failed to send reminder: %s", e)

    async def _send_reminder(self, reminder: CheckBack, session: aiohttp.ClientSession):
        # If the reminder has state that user must be sent to, apply it here.
        if reminder["state"] != "empty":
            await self.db.update_user_append_state(reminder["identity"], reminder["state"])

        # If the reminder is 'daily' or 'weekly' we update its new time here accordinly.
        period = reminder["kwargs"]["period"]
        period_delta = timedelta(days=1 if period == "daily" else 7)
        if period in ["daily", "weekly"]:
            next_time = datetime.fromisoformat(reminder["send_at"]) + period_delta
            await self.db.update_checkback_attribute(reminder["id"], "send_at", next_time.isoformat())
        # Automatically delete one-time checkbacks that were sent.
        else:
            await self.db.delete_checkback(reminder["id"])

        context = json.loads(reminder["request"])
        url = tokens[context["viaInstance"]].url

        # Handle special reminder types:
        if reminder["kwargs"]["type"] == "jira":
            context["message"]["text"] += "\n\n"
            # Here we know user is already authorized:
            creds = await self.db.get_external_service(reminder["identity"], "jira")
            api_jira = AtlassianAPI(creds["url"], creds["username"], creds["token"])
            # Get issues from jira for signed in user.
            issues = await api_jira.get_issues_for_user(reminder["kwargs"]["statuses"], reminder["kwargs"]["projects"])
            # Messages separated by statuses.
            sorted_issues = defaultdict(list)
            for issue in issues:
                key, title = issue["key"], issue["fields"]["summary"]
                message = f"<a href=\"{creds['url']}/browse/{key}\"><b>{key}:</b> {title.strip()}</a>\n"
                # Dividing messages per status.
                sorted_issues[issue["fields"]["status"]["name"]].append(message)

            for status in sorted_issues:
                context["message"]["text"] += f"<b>{status}</b>\n"
                for message in sorted_issues[status]:  context["message"]["text"] += message
                context["message"]["text"] += "\n"

        elif reminder["kwargs"]["type"] == "google calendar":
            # Here we know user is already authorized:
            creds = await self.db.get_external_service(reminder["identity"], "google calendar")
            api_google = GoogleCalendarAPI(GoogleCalendarAPI.dict_to_creds(creds["credentials"]))
            api_google.init_service("calendar", "v3")

            curr_date = None
            has_events = False
            async for event in api_google.iter_events(period_delta):
                dt = datetime.fromisoformat(event["start"]["dateTime"])
                if not curr_date or curr_date != dt.strftime("%x"):  # type: ignore [unreachable]
                    context["message"]["text"] += dt.strftime("\n\n%A, %x:")
                    curr_date = dt.strftime("%x")

                if meet_link := event.get("hangoutLink", ""):
                    meet_link = f" <a href=\"{meet_link}\">[meeting 📲]</a>"
                context["message"]["text"] += f"\n - {dt.strftime('%H:%M')} {event['summary']}{meet_link}"

                has_events = True

            if not has_events:
                # Automatically abort sending a reminder if there are no events.
                logging.debug("No events for user %s, aborting sending google calendar reminder..", reminder["identity"])
                return

        async with session.post(url, json=context, headers=self.HEADERS) as response:
            # If reached server - log response
            if response.status == 200:
                result = await response.json()
                logging.debug("Sending checkback status: %s", result)
                return result

            logging.error("[ERROR]: Sending checkback (send_at=%s, identity=%s) Response status %s", reminder["send_at"], reminder["identity"], response.status)
            return

    def load_bots_file(self):
        latest_fp = os.path.join(ROOT_PATH, "server", "archive", "latest.json")
        with open(latest_fp) as source:
            self.bots_data = json.load(source)

        # @Deprecated: Support older format that we have.
        if not isinstance(self.bots_data, dict):
            default_name = MAIN_BOTSOCIETY_FLOW
            logging.info("Detected potentially outdated botsociety file, rewriting it in new default format, using default namespace: %s ...", default_name)
            self.bots_data = {default_name: self.bots_data}
            with open(latest_fp, "w") as f:
                json.dump(self.bots_data, f, indent=4)

        self.strings.update_strings()
