#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#


class BaseStatus:
    commit = True
    process_next = True
    entry = True

    def __init__(self, process_next=True, skip_entry=False, commit=True):
        """
        Args:
            commit (bool): commit changes to the database
        """
        self.process_next = process_next
        self.commit = commit
        self.entry = not skip_entry

    def __repr__(self):
        return f"{self.__class__.__name__}(commit={self.commit}, process_next={self.process_next}, entry={self.entry})"


class END(BaseStatus):
    """
    Status-shortcut that moves user to "final" or "exit" state.
    ENDState should implement this final state and finish converstaion.
    """


class OK(BaseStatus):
    """
    Status which stands for "Do nothing" (and usually wait for user response).
    """


class GO_TO_STATE(BaseStatus):
    """
    Status that tells the handler to switch the state.
    """
    next_state = None

    def __init__(self, next_state, skip_entry=False, commit=True):
        """
        Args:
            next_state (str): name of the new state, which should handle user
            skip_entry (bool): decide whether or not to skip `entry` handler and go straight into `process`
            commit (bool): commit changes to the database
        """
        self.next_state = next_state
        self.entry = not skip_entry
        self.commit = commit

    def __repr__(self):
        return f"{self.__class__.__name__}(commit={self.commit}, process_next={self.process_next}, entry={self.entry}, next_state={self.next_state})"


class BACK(BaseStatus):
    """
    Status that tells the handler to go back one state
    """


class NOOP(BaseStatus):
    """
    Status that doesn't do anything, but restarts the conversation flow, so next message will be handled by 'StartState'.
    """
