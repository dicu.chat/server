#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from jsonschema import Draft7Validator, validators
from ..schema import RequestBody, Headers
from ..settings import ROOT_PATH
from collections import namedtuple
from copy import copy, deepcopy
from . import UserIdentity
import json
import os


# Load schema method
def load(schema_path) -> dict:
    with open(schema_path) as schema_file:
        return json.load(schema_file)


# Method to extend validator behavior -> set defaults
def extend_with_default(validator_class):
    validate_properties = validator_class.VALIDATORS["properties"]

    def set_defaults(validator, properties, instance, main_schema):
        for name, schema in properties.items():
            # We have to use 'in', because 'None' is a valid default value.
            if "default" in schema:
                # Set default from the schema into our instance.
                instance.setdefault(name, schema["default"])

        for error in validate_properties(validator, properties, instance, main_schema):
            yield error

    return validators.extend(validator_class, {"properties": set_defaults})


# The schema from file
SCHEMA = load(os.path.join(ROOT_PATH, "server", "schema", "schema.json"))
# Validator with defaults
DefaultValidatingDraft7Validator = extend_with_default(Draft7Validator)
# Instantiate default validator
VALIDATOR = DefaultValidatingDraft7Validator(schema=SCHEMA)
# Datatype for parsing results
ValidationResult = namedtuple("ValidationResult", ["validated", "object", "error"])


class Context:
    def __init__(self, request_body: RequestBody):
        self.__dict__["request"] = request_body

    def __getitem__(self, item):
        return self.__dict__["request"][item]

    def __setitem__(self, key: str, value):
        self.__dict__["request"][key] = value

    def __repr__(self):
        return str(self.__dict__)

    @classmethod
    def from_json(cls, json_ish, headers: Headers) -> ValidationResult:
        # TODO: Disallow unfeatured properties?
        #       Or it will be too resource-consuming?
        for error in VALIDATOR.iter_errors(json_ish):
            return ValidationResult(False, None, error.args[0])

        # Set request value.
        obj = cls(json_ish)
        # Automatically generate user identity.
        obj["user"]["identity"] = UserIdentity.hash(obj["user"]["userId"], headers["serviceIn"])
        # Setting header values into the context.
        obj["viaInstance"] = headers["viaInstance"]
        obj["serviceIn"] = headers["serviceIn"]
        obj["serviceOut"] = headers["serviceOut"]
        return ValidationResult(True, obj, None)

    def to_dict(self):
        return self.__dict__["request"]

    def copy(self):
        return copy(self)

    def deepcopy(self):
        return deepcopy(self)
