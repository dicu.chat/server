#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from typing import Union, List, Dict
from bs4 import BeautifulSoup


class Button:
    def __init__(self, text: str, key: str = None):
        self.text = text
        self.key = key

    def set_key(self, key: str):
        self.key = key

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Button):  return self.key == other.key
        if isinstance(other, str):     return self.key == other
        return NotImplemented

    def __bool__(self):
        return bool(self.key)

    def __repr__(self):
        return f"Button(key={self.key})"

    def __hash__(self):
        return hash(self.key)


class TextPromise:
    def __init__(self, lang: str, key: str):
        self.lang: str = lang
        self.key:  str = key
        self.value: str = ""
        self.changed = False
        self._format_args:   List[Union[TextPromise, str]]      = []
        self._format_kwargs: Dict[str, Union[TextPromise, str]] = {}
        self._complex:       List[Union[TextPromise, str]]      = []

        self.source_language: str = "en"

    @property
    def text(self):
        return str(self)

    def fill(self, value: str):
        self.value = self.remove_translation_span(value)

    def format(self, *args, **kwargs) -> "TextPromise":
        self._format_args += args
        self._format_kwargs.update(**kwargs)
        return self

    def set_language(self, lang: str):
        self.lang = lang
        self.changed = True

    def __str__(self):
        value = self.value
        if self._format_args or self._format_kwargs:  value  = value.format(*self._format_args, **self._format_kwargs)
        if self._complex:                             value += "".join(str(item) for item in self._complex)
        return value

    def __repr__(self):
        if not self.value:        return f"EmptyTextPromise(key={self.key},lang={self.lang})"
        if len(self.value) < 12:  return f"TextPromise(key={self.key},lang={self.lang}, value={self.value})"
        return f"TextPromise(key={self.key},lang={self.lang}, value={self.value[:9]}...)"

    def __add__(self, other: Union["TextPromise", str]):
        self._complex.append(other)
        return self

    def __eq__(self, other):
        return self.key == other.key

    def __hash__(self):
        return hash(self.key)

    def __bool__(self):
        return bool(self.value)

    # Workaround to make promise to keep itself
    # TODO: remove
    def __deepcopy__(self, memdict={}):  # noqa: B006
        return self

    def __copy__(self):
        return self

    @staticmethod
    def remove_translation_span(value) -> str:
        if "<span" in value:
            # Here we remove "no translation" span from the strings (For now just remove all span tags).
            value = BeautifulSoup(value, features="html.parser")
            for span in value.find_all("span"):  span.unwrap()
            return str(value)
        return value
