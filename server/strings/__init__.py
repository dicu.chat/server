#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from ..settings import ROOT_PATH, BOTSOCIETY_LANGUAGE
from .items import TextPromise, Button
from typing import List, Set, Optional
from ..external import TranslationAPI
from collections import defaultdict
import asyncio
import hashlib
import logging
import json
import os


__all__ = ("TextPromise", "Button", "StringAccessor", "Strings")


def load_strings():
    # Note(andrew): A helper function that takes relative path, and tries to read json file
    #     from that path, after appending project's root-path to it.
    def _load(*path):
        path = os.path.join(ROOT_PATH, *path)
        with open(path) as contents:
            return json.load(contents)

    # Load our static strings file.
    strings = {"en": _load("server", "strings", "json", "strings.json")}
    statics = set(strings["en"])

    if BOTSOCIETY_LANGUAGE and BOTSOCIETY_LANGUAGE != "en":
        strings[BOTSOCIETY_LANGUAGE] = dict()

    # If present, load botsociety json file.
    if os.path.exists(os.path.join(ROOT_PATH, "server", "archive", "latest.json")):
        flows = _load("server", "archive", "latest.json")

        # Since we support multiple flows, iterate over every flow that we read from the
        # bostsociety file from disk.
        for flow in flows.values():
            # For each message in the flow we want to add to the 'strings' dictionary
            # content of the message and its corresponding key.
            for message in flow:
                strings[BOTSOCIETY_LANGUAGE][message["text_key"]] = message["text"]

                # Additionaly, for each message try to iterate over its buttons, if any
                # are present, and add contents of the button and corresponing key to
                # the 'strings' storage as well.
                for btn in message.get("buttons", tuple()):
                    strings[BOTSOCIETY_LANGUAGE][btn["text_key"]] = btn["text"]

    # Note(andrew): Now, we are going to write a new dictionary in some preferred format, using
    #     previously stored contents and keys, but also adding content hashes for keeping strings
    #     in sync. This dictionary will contain only original strings (only english in our case),
    #     and translations will be loaded later.
    original = dict()

    for lang, item in strings.items():
        original[lang] = dict()
        for key, text in item.items():
            # Generate hash, so when we pull cached translation from the database
            # we can check if it's still valid (original text didn't change since
            # we stored translation with hash last time).
            content_hash = hashlib.sha256(text.encode()).hexdigest()
            original[lang][key] = {"text": text, "hash": content_hash}

    return original, statics


class StringAccessor:
    def __init__(self, lang: str, strings: "Strings"):
        self.lang = lang
        self.promises: Set[TextPromise] = set()
        self.strings = strings

    def __getitem__(self, key: str) -> TextPromise:
        promise = TextPromise(self.lang, key)
        res = self.strings[self.lang].get(key)

        if res:  promise.fill(res["text"])

        self.promises.add(promise)
        return promise

    def fill_from_cache(self, promise: TextPromise):
        value = self.strings[promise.lang].get(promise.key)
        if value:  promise.fill(value["text"])

    # TODO(andrew): This wrapper looks bad.  @Robustness
    async def fill_promises(self):
        await self.strings.get_translations(self.promises)


class Strings:
    def __init__(self, translation: TranslationAPI, db):
        self.tr = translation
        self.db = db

        self.original, self.statics = load_strings()

        self.cache: dict[str, dict[str, dict]]  # @Incomplete
        self.cache = defaultdict(dict)
        self.cache.update(self.original)

    def __getitem__(self, key: str):
        # Note that this never fails because we are using 'defaultdict(dict)'.
        return self.cache[key]

    # TODO: explain why and when this is called.
    def update_strings(self):
        # Reload strings.
        self.original, self.statics = load_strings()
        # Make sure to update relevant parts, but DO NOT rewrite whole cache dict.
        self.cache.update(self.original)

    # Note(andrew): This method does automatic translations for any passed promises. Values for translations,
    #     are taken from 'in-memory' cache, if possible, or from the persistent storage (database), if possible.
    #     And in the worst case, we are calling 'translator' routine to do translations for us, via third party
    #     API (like Google Translation API). This method updates all the promises in-place, permutating each of
    #     them and their values, but it also returns modified list (although the list itself is not reduced or
    #     extended!).
    async def get_translations(self, promises: List[TextPromise]) -> List[TextPromise]:
        # Note(andrew): A small helper function that takes language, and a new key/text pair and adds it to the
        #     cache. Additionaly, it stores a hash of the original string, for the later validity check.
        def cache_item(source: str, lang: str, key: str, text: str):
            self.cache[lang][key] = {
                "text": text,
                "hash": self.original[source][key]["hash"],
            }

        uncached = list()
        query_cache: Optional[dict] = None

        for p in promises:
            # Note(andrew): Since google translation or similar translation APIs does not work very well, sometimes
            #     we want to write botsociety flows in the target language for specific use case, but then we get a
            #     new problem -- we do not really know which language the flow is, because we do not have that meta
            #     information from the botsociety api. So for now, the easiest solution is to have a config variable
            #     that can be set during start up (deployment), with the original language, that is expected to come
            #     from the botsociety in the flow. This is not very robust, if you decide to put together flows with
            #     different languages, but on the other hand, this use case is questionable, since you have already
            #     committed to putting effort into writing original text in that other language. In some future, we
            #     could allows configuration per-flow, or somehow bake this (and some other) metadata into the flow
            #     itself.  @Robustness
            #
            #     So, for now, we are using 'BOTSOCIETY_LANGUAGE' for this:
            if p.key not in self.statics:
                p.source_language = BOTSOCIETY_LANGUAGE

            # Try quering data from the promise from cache, using promise's language and key.
            cached_res = self.cache[p.lang].get(p.key)

            # If cached result is not present in cache at all; or if hash in this cached version of the
            # translation doesn't match hash of the original string in the loaded file.
            if not cached_res or cached_res["hash"] != self.original[p.source_language][p.key]["hash"]:
                # Note(andrew): Try checking with database, just in case we are de-synced with our persistent
                #     storage somehow. If we will do this in a sequential manner, this will be extremely slow
                #     as we will be calling into our database in a loop, which is always a bad idea. Instead,
                #     lets have additional small in-memory cache and call into database only once and only for
                #     the first time when we have to. All calls afterwards are avoided since we query info from
                #     database in a batch-read request and store results for later iterations.  @Speed
                #
                #     For the sake of simplicity and speed, lets turn 'p.lang' and 'p.key' into keys of the cache
                #     via string concat, so we can use very simple 'dict' data structure for this one-off cache.
                if query_cache is None:
                    db_results = await self.db.query_translations(((_p.lang, _p.key) for _p in promises))
                    query_cache = {f"{res['language']}{res['string_key']}": res for res in db_results}

                # Trying to find result in the populated structure.
                cached_res = query_cache.get(f"{p.lang}{p.key}")

                # Do the same check as we did for our in-memory cached result here, for database-stored result.
                if not cached_res or cached_res["content_hash"] != self.original[p.source_language][p.key]["hash"]:
                    # Note(andrew): If the check didn't pass here too, we are confident now that we don't have
                    #     the translation anywhere, neither in in-memory nor consistent cache, so we can mark
                    #     this promise for later translation.
                    uncached.append(p)
                    # Now continue. We are not filling the promise right now, because cache/hash was invalid.
                    continue

                # Here we know that cache from database is valid, but we didn't have it in the in-memory cache.
                cache_item(p.source_language, p.lang, p.key, cached_res["text"])

            # If we got here, it means either in-memory or persistent cache was valid, we can fill the promise
            # immediately with the relevant text.
            p.fill(cached_res["text"])

        # If we don't have any promises marked for translation and subsequential caching, we can exit this function
        # immediately. Additionaly we are returning list of promises that was passed here, even though that is not
        # necessary, since we were permutating each promise object.
        if not uncached:  return promises

        # If we got here, there are some new translations to make before we are done. This method of the 'translator'
        # module instance makes a bunch of parallel translation requests and permutates passed promises in-place.
        await self.tr.translate_promises(uncached, self.original)  # This step actually makes new translations.

        # Note(andrew): Now after we did all the necessary cache lookups and translations, we need to add them to local
        #     cache and to the persistent storage. This might potentially be factored out into background routine, because
        #     this loop does't help current callee in any way, and just does routine useful for the future callees. If you
        #     are doing this refactor, please read comments below (everything until the end of this routine) to figure out
        #     how to combine all background jobs in one routine, if necessary. @Speed
        to_db = []
        for p in uncached:
            logging.debug("Caching item {'%s': '%s'} for the language '%s' (original '%s') ...", p.key, str(p.value), p.lang, p.source_language)

            # Create an object that database expects as a valid translation result.
            to_db.append({
                "source_language": p.source_language,
                "language": p.lang,
                "string_key": p.key,
                "text": str(p.value),
                # Note(andrew): Here we are saving the original string's hash, not new hash of the translated string,
                #     because we will be comparing this hash of the original string that we store right now, with the
                #     hash of whatever string there is now in the strings file (or flow), that corresponds to this key.
                "content_hash": self.original[p.source_language][p.key]["hash"],
            })

            cache_item(p.source_language, p.lang, p.key, str(p.value))

        # Note(andrew): Now we are going to run a routine that saves our 'uncached' translations to the persistent
        #     storage. We don't really want to wait on it, because this increases latency of the response, and this
        #     task doesn't do any useful work for the current callee (current user), only for future 'callees'. So,
        #     instead of awaiting the routine in a blocking way, we just create a task for the asyncio loop.
        #
        #     Additionaly, not that we are not doing the optimization check here for 'if to_db:', which might seem
        #     like a good thing, but is actually useless. We know that 'to_db' is always non-empty, because 'uncached'
        #     must be non-empty to get here, and we use it to populate 'to_db'. If the logic changes at some point,
        #     this comment might become irrelevant.  @Robustness
        asyncio.ensure_future(self.db.bulk_save_translations(to_db))

        return promises  # Finally, returning updated result.

    async def load_everything(self):
        count = 0
        invalid = 0
        async for t in self.db.iter_translations():
            count += 1

            if "source_language" not in t:
                invalid += 1
                continue

            source = t["source_language"]
            lang   = t["language"]
            key    = t["string_key"]

            # Note(andrew): Since we pull *everything* from the database here, by quering all the translations, that
            #     we ever created and pushed to database, some of the translations might be invalid (i.e. someone has
            #     pushed new version of the flow, or static strings changed) and keys might not be present anymore in
            #     the 'original_strings' (that contain a mix of static and all strings from the botsociety).
            #
            #     So, this is important to deal with here, not only because it will potentially cause an error later,
            #     but also we get a clean way to remove invalid caches from database on startup, which seems to be
            #     extremely efficient (TODO).  @Robustness
            if (source not in self.original) or (key not in self.original[source]):
                invalid += 1
                continue

            self.cache[lang][key] = {
                "text": t["text"],
                "hash": t["content_hash"],
            }

        logging.info("Loaded %s translated items from database (invalidated %s of them) ...", count, invalid)
