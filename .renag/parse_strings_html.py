#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from renag import Complainer, Complaint, Severity, Span
from pyparsing import ParseResults
from html.parser import HTMLParser
from typing import List, Dict
from pathlib import Path
import json
import os
import re


STRINGS_PATH = "server/strings/json/strings.json"
TAG_PATTERN = re.compile(r"<(.*?)>")
PATH = "."
# Tags that we allow in the 'string.json' file:
ALLOWED_TAGS = {
    # Bald text
    "b": None, "strong": None,
    # Italic
    "i": None, "em": None,
    # Underline
    "u": None, "ins": None,
    # Strikethrough
    "s": None, "strike": None, "del": None,
    # Link
    "a": ("href",),
    # Pre-formatted
    "code": None, "pre": None,
    # Google translation tag
    "span": ("class", "translate"),
}


class CustomHTMLParser(HTMLParser):
    def __init__(self):
        self.text = None
        self.tags_matches = None

        self.tags_history = list()
        self.tags_stack = list()
        self.tags_count = 0
        self.tags_cache = dict()
        super().__init__()

    def set_text(self, text):
        self.text = text
        self.tags_matches = list(TAG_PATTERN.finditer(self.text))

    # Those are special kinds of tags (like <img/>), if we don't treat them here,
    # they will first go to starttag and then to endtag, which breaks our parser.
    def handle_startendtag(self, tag, attrs):
        # Find and cache relative span for latest tag:
        group = self.tags_matches[self.tags_count]

        self.tags_history.append((tag, attrs, group.span()))
        self.tags_count += 1

    def handle_starttag(self, tag, attrs):
        # Find and cache relative span for latest tag:
        group = self.tags_matches[self.tags_count]

        self.tags_stack.append((tag, group.span()))
        self.tags_history.append((tag, attrs, group.span()))

        self.tags_cache[tag] = group.span()
        self.tags_count += 1

    def handle_endtag(self, tag):
        # Find and cache relative span for latest tag:
        group = self.tags_matches[self.tags_count]

        self.tags_history.append((tag, None, group.span()))

        self.tags_cache[tag] = group.span()
        self.tags_count += 1

        try:
            v, _ = self.tags_stack.pop()
            if v != tag:
                raise ValueError(f"Expected closing tag '</{v}>', but received '</{tag}>'.")
            # Clear cache afterwards:
            self.tags_cache.pop(v)
        except IndexError:
            raise ValueError(f"Closed tag '</{tag}>' doesn't match any open tag!")
        except TypeError:
            raise ValueError(f"Unexpected closing tag '</{tag}>'!")

    def finalize(self):
        # Raise on left over stack value:
        if self.tags_stack:
            self.tags_cache = {i: span for i, (_, span) in enumerate(self.tags_stack)}
            raise ValueError("Found unmatched tag(s)!")

        # Check for inappropriate tags or tag attributes:
        if self.tags_history:
            self.tags_cache.clear()

            for i, (tag, attrs, span) in enumerate(self.tags_history):
                if tag not in ALLOWED_TAGS:
                    self.tags_cache[i] = span
                    raise ValueError(f"Found unsupported tag '{tag}'!")

                _attrs = ALLOWED_TAGS[tag]
                if attrs is None and _attrs is None:
                    continue
                elif attrs is None:
                    continue
                else:
                    if not _attrs:
                        _attrs = tuple()

                    for name, _ in attrs:
                        if name not in _attrs:
                            self.tags_cache[i] = span
                            raise ValueError(f"Tag '{tag}' contains an invalid argument {name}!")

    def clear(self):
        self.text = None
        self.tags_matches = None

        self.tags_history.clear()
        self.tags_stack.clear()
        self.tags_count = 0
        self.tags_cache.clear()


# Here we will look for bad tags from the 'strings.json'.
class ParseHTMLTagsStringsJSON(Complainer):
    capture = r""
    severity = Severity.CRITICAL
    glob = ["*.py"]

    def __init__(self):
        # Path to the source 'strings.json' file:
        self.source_path = os.path.join(PATH, STRINGS_PATH)
        # First we load and parse the default 'strings.json' file:
        with Path(self.source_path).open("r") as f:
            self.source_text = f.read()

        self.source: Dict[str, str] = json.loads(self.source_text)

    def check(self, txt: str, path: Path, capture_span: Span, capture_data: ParseResults) -> List[Complaint]:
        return []

    def finalize(self) -> List[Complaint]:
        complaints = list()
        for key, _value in self.source.items():
            # @Robustness: Can this break easily?
            if "<" in _value:
                # Prepare html parser for use:
                parser = CustomHTMLParser()

                value = json.dumps(_value)
                parser.set_text(value)

                # @Robustness: Can this break easily?
                pattern = fr"\"{key}\"(\s*?)\:(\s*?)"
                _, absolute_start = re.compile(pattern).search(self.source_text).span()  # type: ignore [union-attr]
                absolute_start += 1
                assert self.source_text[absolute_start:absolute_start + len(value)] == value

                try:
                    parser.feed(value)
                    parser.finalize()
                except ValueError as error:
                    path = Path(self.source_path).absolute()
                    spans = {path: {}}  # type: ignore [var-annotated]
                    for _, (rel_start, rel_end) in parser.tags_cache.items():
                        span = (absolute_start + rel_start, absolute_start + rel_end)
                        spans[path][span] = None

                    complaints.append(Complaint(
                        cls = type(self),
                        file_spans = spans,
                        description = str(error),
                        severity = self.severity,
                    ))

                parser.clear()
        return complaints
