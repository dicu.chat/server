#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from pyparsing import ParseResults, Literal, Word, alphas
from renag import Complainer, Complaint, Severity, Span
from pathlib import Path
from typing import List
import json
import re


STRINGS_JSON_PATH = "server/strings/json/strings.json"


class RulesStringsJSON(Complainer):
    """ Default error message to the complainer """
    capture = (Literal("self.strings[\"") + Word(alphas) + Literal("\"]")) \
            | (Literal("button == \"") + Word(alphas) + Literal("\""))  # noqa: E127
    severity = Severity.CRITICAL
    glob = ["*.py"]

    def __init__(self):
        self.strings_fp = STRINGS_JSON_PATH
        # Loading 'server/strings/json/strings.json' file from the disk and parsing it here.
        # We will do this in the __init__ statement so it only has to be read from disk once.
        # The path will be relative to the directory `renag` is run in, which must always be
        # project root.
        with Path(self.strings_fp).open("r") as f:
            # Here we read the file, since we will need the text later.
            self.text: str = f.read()

        self.all_keys = {key: {"used": False} for key in json.loads(self.text).keys()}

        # This pattern will be used later to check and enforce 'camelCase' rule for json files.
        self.camel_pattern = re.compile(r"\b([a-z0-9]+?[A-Z]?)+[a-z0-9]*?\b")
        # This pattern is for removing '-' and '_' from the string.
        self.extra_chars = re.compile(r"_|-")

    def check(self, txt: str, path: Path, capture_span: Span, capture_data: ParseResults) -> List[Complaint]:
        # Make sure the program is sane and values are what we expect.
        assert len(capture_data) == 3

        key = capture_data[1]
        # We know for sure that the key must exist (we already matched the pattern, since we are here).
        assert key

        # Check whether key exists inside the strings file, if it does - quit early.
        if key in self.all_keys:
            # Set the key to 'seen' so we can warn about unused once later.
            self.all_keys[key]["used"] = True
            return []

        # Now we need to calculate new span (inside the string, to specifically match the key).
        capture_span = (capture_span[0] + len(capture_data[0]), capture_span[1] - len(capture_data[2]))
        # We error out for non-existing keys. Note, that we are passing our new span here.
        return [
            Complaint(
                cls=type(self),
                file_spans={path: {capture_span: None}},
                description=f"The key '{key}' does not exist in '{self.strings_fp}'!",
                severity=Severity.CRITICAL,
                help="This is likely caused by typo in the key name.",
            )
        ]

    def finalize(self) -> List[Complaint]:
        complaints: List[Complaint] = list()

        for key, item in self.all_keys.items():

            # @Robustness: Let's add quotations around the key, so it doesn't match some random word inside the text (hopefully).
            nkey = f'"{key}"'
            # Find index in the original 'strings.json' text.
            found_at = self.text.find(nkey)
            # Create a span that shows it.
            span = (found_at + 1, found_at + len(nkey) - 1)

            # This will generate complaints about unused keys and 'strings.json' file.
            if not item["used"]:
                complaints.append(Complaint(
                    cls=type(self),
                    file_spans={Path(self.strings_fp).absolute(): {span: None}},
                    description=f"The key {nkey} could not be found anywhere in the code!",
                    severity = Severity.WARNING,
                    help="Start using it or remove it from the file completely.",
                ))

            # Here we can check for validity of all keys according to 'camelCase' ('\b([a-z][a-z0-9]+([A-Z])?)+[a-z0-9]*?\b').
            # TODO: Note: something similar should be done for the API Schema ('schema.json' file) as well.
            if not self.camel_pattern.fullmatch(key):
                # Here we try to guess and make the key look like camelCase.
                potential_new_key = self.extra_chars.sub(" ", key).title().replace(" ", "")
                potential_new_key = potential_new_key[0].lower() + potential_new_key[1:]

                # Here we additionaly check if camel case changed anything, if not, it's not an error.
                if key != potential_new_key:
                    complaints.append(Complaint(
                        cls=type(self),
                        file_spans={Path(self.strings_fp).absolute(): {span: None}},
                        description="Key does not comply with 'camelCase' style!",
                        severity=Severity.CRITICAL,
                        help="Please, change the key to 'camelCase'! It should look something"
                             f" like this: \"{potential_new_key}\" (note: this might be wrong).",
                    ))

        return complaints
