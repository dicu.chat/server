#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
# TODO: @TBD
# from pyparsing import Literal, QuotedString, nestedExpr, restOfLine, Regex, White, Suppress
# from renag import Complainer, Severity


# class ComplexPrintComplainer(Complainer):
#     """Print statements can slow down code."""
#
#     capture = (
#         (Regex(r"p{1,2}rint") + nestedExpr("(", ")"))
#         .ignore("#" + restOfLine)
#         .ignore(Regex("([^\s]+)(p{1,2})rint") + restOfLine)
#         .ignore(QuotedString('"', multiline=False))
#         .ignore(QuotedString("'", multiline=False))
#         .ignore(QuotedString('"""', multiline=True))
#         .ignore(QuotedString("'''", multiline=True))
#     )  # An example of pyparsing
#     severity = Severity.WARNING
#     glob = ["*.py"]
#     exclude_glob = ["test_*.py"]
