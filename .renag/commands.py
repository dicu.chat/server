#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from pyparsing import ParseResults, Suppress, Literal, OneOrMore, Optional, Word, printables
from renag import Complainer, Complaint, Severity, Span
from typing import List
from pathlib import Path
import ast

# Note(andrew): Let's enable all internal warnings of pyparsing, because we want to know if our
#     test/static analyzer is doing an illegal pattern. Disabled for now, waiting for release
#     of v3.0.0 on pypi.
# TODO: enable_all_warnings()

"""
TODO(andrew): This fails on CI for __reasons__ that might be worth investigating later.

# Note(andrew): Here we are doing import from the project itself, which is probably not great. But
#     this is probably the best option, until it causes any issues. Then if it does just copy the
#     collecting routine into this file. For commands, however, we do a global regex search, so it
#     depends how much you change things, for this to break. For example, just moving commands into
#     another place would still work. But renaming them into anything else but 'COMMANDS' will break.
import sys
sys.path.append("../server")  # For non-dockerized run.
sys.path.append("/usr/src/app")  # For dockerized run.
from server.states import collect  # noqa: E402
"""


def collect():
    import re
    import os

    class_pattern = re.compile(r"class (\w+)")

    states_path = "server/states"
    for fn in os.listdir(states_path):
        fp = os.path.join(states_path, fn)
        if not os.path.isfile(fp):  continue

        with open(fp) as f:
            r = class_pattern.finditer(f.read())

        for match in r:
            name = match.group(1)
            yield name


# Here we will look for bad tags from the 'strings.json'.
class HandlerCommandsCheckForExistence(Complainer):
    # Helper functions.
    sl = lambda s: Suppress(Literal(s))
    # Helper variables. TODO: migrate to 3.0.0 when it comes out.
    cname = sl('"') + Word(printables + " \n\r\t", excludeChars='"') + sl('"')
    cdata = sl("{") + Word(printables + " \n\r\t", excludeChars="}") + sl("}")

    # Note(andrew): This is a *VERY* arbitrary and fragile capture.
    capture = sl("COMMANDS") + sl("=") + sl("{") + OneOrMore(cname + sl(":") + cdata + Suppress(Optional(",")))

    severity = Severity.CRITICAL
    glob = ["handler.py"]

    def __init__(self):
        # self.states = {s.__name__: s for s in collect()}
        self.states = {s: True for s in collect()}

    def check(self, txt: str, path: Path, capture_span: Span, capture_data: ParseResults) -> List[Complaint]:
        assert len(capture_data) % 2 == 0, "Failed to parse data!"

        complaints = []

        # Here we are assuming (assert above) that data consists of 'key': 'value' pairs,
        # so we iterate over general array grabbing and parsing those key-value pairs.
        for i in range(0, len(capture_data), 2):
            cname, cdata = capture_data[i], capture_data[i + 1]
            # Parsing cdata as valid python dict:
            pcdata = ast.literal_eval(f"{{ {cdata} }}")

            # We are going to check for the state name. Note, we have 'state' key hardcoded here.
            sname = pcdata["state"]
            # If we found a valid state - continue.
            if self.states.get(sname) is not None:  continue

            # Generating an error, because we didn't find a state with given name.
            gstart, gend = capture_span
            scoped_text  = txt[gstart:gend]

            rel_data_start = scoped_text.index(cdata)
            scoped_data    = scoped_text[rel_data_start:]

            rel_state_start = scoped_data.index(sname)
            rel_state_end   = rel_state_start + len(sname)

            abs_state_start = gstart + rel_data_start + rel_state_start
            abs_state_end   = gstart + rel_data_start + rel_state_end

            abs_state_span  = (abs_state_start, abs_state_end)

            complaints.append(Complaint(
                cls=type(self),
                file_spans={path: {abs_state_span: None}},
                description="Command refers to non-existing state!",
                severity=self.severity,
                help=f"Please, make sure that command '{cname}' references a real (existing) state."
                     f" Couldn't find '{sname}' in the list of states from 'server.states.collect'.",
            ))
                
        return complaints
