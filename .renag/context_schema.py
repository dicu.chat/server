#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from pyparsing import ParseResults, Literal, Word, OneOrMore, Suppress, alphas
from renag import Complainer, Complaint, Severity, Span
from pathlib import Path
from typing import List
import json
import re


SCHEMA_JSON_PATH = "server/schema/schema.json"


def read_properties(src: dict, out: dict, keys: list):
    for key in src.get("properties", []):
        out[key] = {}
        keys.append(key)
        if src["properties"][key].get("type") == "object":
            read_properties(src["properties"][key], out[key], keys)


# Here we will look for bad keys from the 'schema.json' and produce errors if there are any.
class PotentialKeyErrorInRequestSchema(Complainer):
    capture = Suppress(Literal("context")) + OneOrMore(Suppress(Literal("[\"")) + Word(alphas + "_-") + Suppress(Literal("\"]")))
    severity = Severity.CRITICAL
    glob = ["*.py"]

    def __init__(self):
        self.schema_fp = SCHEMA_JSON_PATH

        # Here we create a dict that will be populated with keys according to schema.
        self.schema = {}
        # Here we collect all keys from the schema, in one flattened list, for processing later.
        self.keys = []
        # Loading schema file from the disk and parsing it here. We will do this in the __init__
        # statement so it only has to be read from disk once. The path will be relative to the
        # directory `renag` is run in, which should always be project root.
        with Path(self.schema_fp).open("r") as f:
            self.text: str = f.read()

        # Parse schema file to read all properties.
        read_properties(json.loads(self.text), self.schema, self.keys)

        # @Monkeypatch @Robustness: First, we need to monkey patch 'identity' into the user. It's
        # an internal thing, which generated after schema validation.
        self.schema["user"]["identity"] = ""

        # Adding headers.
        self.schema["serviceIn"] = ""
        self.schema["serviceOut"] = ""
        self.schema["viaInstance"] = ""

        # @CopyPasta from '.renag/strings_json.py'

        # This pattern will be used later to check and enforce 'camelCase' rule for json files.
        self.camel_pattern = re.compile(r"\b([a-z0-9]+?[A-Z]?)+[a-z0-9]*?\b")

        # This pattern is for removing '-' and '_' from the string.
        self.extra_chars = re.compile(r"_|-")

    def check(self, txt: str, path: Path, capture_span: Span, capture_data: ParseResults) -> List[Complaint]:
        start, end = capture_span
        matched_text = txt[start:end]

        # Pull schema into the variable, so we can rewrite the variable when the cursor moves.
        context = self.schema
        # Here we go through matched keys, per context expression..
        for ckey in capture_data:
            # ..try using this key to recieving anything from context..
            res = context.get(ckey)
            # .. if we get None, key does not exist in schema. We need to throw an error.
            if res is None:
                # Here we need to figure out absolute span for specific key.
                index = matched_text.index(ckey)
                span = (start + index, start + index + len(ckey))
                return [Complaint(
                    cls = type(self),
                    file_spans = {path: {span: None}},
                    description = f"This key doesn't exist in the '{self.schema_fp}' file, "
                                  "this may lead to a key error at runtime!",
                    severity = self.severity,
                    help="Please, make sure the key is not typo-ed!",
                )]
            # Since we got here, we know that the key is valid, so we move the context cursor.
            context = res

        # Otherwise, all keys matched, so no errors need to be produced.
        return []

    def finalize(self) -> List[Complaint]:
        complaints: List[Complaint] = list()

        # @CopyPasta from '.renag/strings_json.py'

        for key in self.keys:
            # @Robustness: Let's add quotations around the key, so it doesn't match some random word inside the text (hopefully).
            nkey = f'"{key}"'
            # Find index in the original 'strings.json' text.
            found_at = self.text.find(nkey)
            # Create a span that shows it.
            span = (found_at + 1, found_at + len(nkey) - 1)

            # Here we can check for validity of all keys according to 'camelCase' ('\b([a-z][a-z0-9]+([A-Z])?)+[a-z0-9]*?\b').
            # TODO: Note: something similar should be done for the API Schema ('schema.json' file) as well.
            if not self.camel_pattern.fullmatch(key):
                # Here we try to guess and make the key look like camelCase.
                potential_new_key = self.extra_chars.sub(" ", key).title().replace(" ", "")
                potential_new_key = potential_new_key[0].lower() + potential_new_key[1:]

                # Here we additionaly check if camel case changed anything, if not, it's not an error.
                if key != potential_new_key:
                    complaints.append(Complaint(
                        cls=type(self),
                        file_spans={Path(self.schema_fp).absolute(): {span: None}},
                        description="Key does not comply with 'camelCase' style!",
                        severity=Severity.CRITICAL,
                        help="Please, change the key to 'camelCase'! It should look something"
                             f" like this: \"{potential_new_key}\" (note: this might be wrong).",
                    ))

        return complaints
