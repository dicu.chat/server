# syntax=docker/dockerfile:experimental

# Defining variables for different dependencies.
ARG VERSION=3.9-slim-buster

# Builder for the wheels
FROM python:${VERSION} as builder
# Installing dependencies for building wheels and/or for containers.
RUN apt-get -y update \
 && apt-get -qqy --no-install-recommends install \
    # Installing git for installing python packages from github directly. \
    git \
    # Installing tidy library, which is a dependency for tidy python package for our tests. \
    tidy \
    # Installing curl, which we use to install 'dotenv-linter'. \
    curl
# Installing both requirements at the same time into different wheel directories.
COPY requirements.txt .
COPY test-requirements.txt .
RUN pip install -U pip wheel setuptools \
 && mkdir /wheels \
 && pip wheel -r requirements.txt -w /wheels \
 && mkdir /test-wheels \
 && pip wheel -r test-requirements.txt -w /test-wheels
# Installing dotenv-linter binary, which we will use for testing.
RUN curl -sSfL https://raw.githubusercontent.com/dotenv-linter/dotenv-linter/master/install.sh | sh -s


# Main stuff
FROM python:${VERSION} as main
WORKDIR /usr/src/app
# Copying pre-built staticly-compiled binary of FFMPEG from another container. We need it for audio transcoding.
COPY --from=mwader/static-ffmpeg:4.4.0 /ffmpeg /usr/local/bin/
# Temporarily mount /wheels dir and install wheels from there. This is possible due to the 'experimental' syntax on the line 1.
RUN --mount=type=bind,source=/wheels,target=/wheels,from=builder pip install --no-cache-dir /wheels/*
# Copying main server files.
COPY server server
# Rasa countries list file, required for mapping valid values (country_languages.json).
COPY rasa/languages/csv/raw_data/country_languages.json rasa/languages/csv/raw_data/country_languages.json
# Launch.
CMD ["python3", "-m", "server"]


# Tester build.
FROM main as test
# Copying tidy library from the builder.
COPY --from=builder /usr/lib/x86_64-linux-gnu/libtidy.so.5deb1.6.0 /usr/lib/x86_64-linux-gnu/libtidy.so
# Copying 'dotenv-linter' binary from builder.
COPY --from=builder /bin/dotenv-linter /usr/local/bin/
# Temporarily mount 'test-wheels' directory and install wheels from there.
RUN --mount=type=bind,source=/test-wheels,target=/test-wheels,from=builder pip install --no-cache-dir /test-wheels/*
# Copying all the files that are not ignored (check .dockerignore).
COPY . .
# Run tests.
ENTRYPOINT [ "bash", "test.sh" ]
