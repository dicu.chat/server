# Static code analysis with flake8.
echo "Running flake8 on all the code.. Elapsed time $SECONDS seconds."
flake8 || exit $?

# Static code analysis with mypy.
echo "Running mypy on '.renag' files.. Elapsed time $SECONDS seconds."
mypy --strict-equality .renag || exit $?

echo "Running mypy on 'server' package.. Elapsed time $SECONDS seconds."
mypy --strict-equality -p server || exit $?
# Analyze testing code.
echo "Running mypy on 'tests' package.. Elapsed time $SECONDS seconds."
mypy --strict-equality -p tests || exit $?

# Call custom analyzers on the main code.
echo "Running renag on 'server' package.. Elapsed time $SECONDS seconds."
renag --load_module=.renag --analyze_dir=server -n=0 --include_untracked || exit $?

echo "Running dotenv-linter in the default mode.. Elapsed time $SECONDS seconds."
dotenv-linter --recursive -e .venv -e rasa -e gpt2 || exit $?
echo "Compare our base .env files to make sure everything is up to date.. Elapsed time $SECONDS seconds."
dotenv-linter compare .env.example .env.test .env || exit $?
