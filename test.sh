./test_analyze.sh || exit $?

# Running pytest..

# Make sure to prepare .env file for the tests.
if [ ! -f .env ]; then
    cp .env.test .env
fi

# Run Pytest via our own entry point code:
echo "Running pytest invocation script.. Elapsed time $SECONDS seconds."
PYTHONDEVMODE=1 python invoke_tests.py || exit $?

# TODO: Create scripts for end-to-end testing.
# python3 ./tests/end2end/entry.sh

echo "Total elapsed time: $SECONDS seconds."
