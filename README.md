<h1 align="center">
  <br>
  <a href="https://www.morphysm.com/"><img src="./assets/morph_logo_rgb.png" alt="Morphysm" ></a>
  <br>
  <h5 align="center"> Morphysm is a community of engineers, designers and researchers
contributing to security, cryptography, cryptocurrency and AI.</h5>
  <br>
</h1>

<h1 align="center">
 <img src="https://img.shields.io/badge/Python-3.9-red" alt="python badge">
 <img src="https://img.shields.io/badge/docker-20.10-blue" alt="docker badge">
 <img src="https://img.shields.io/badge/version-1.1.8-orange" alt="version badge">
 <img src="https://img.shields.io/gitlab/pipeline-status/dicu.chat/server?branch=master" alt="docker build">
</h1>


# Dicu ChatBot

Dicu is chatbot that allows for predefined chat flows with live updates and provides a conversational AI integration, which allows for holding discussions about various topics, such as love and democracy.

# Table of Contents

<!--ts-->

- [Getting Started](#getting-started)
- [Prerequisites](#prerequisites)
- [Installation](#usage)
- [Development](#development)
- [Test](#test)
- [Production](#production)
- [Update](#update)
- [Demo](#demo)
- [TroubelShoting](#troubelShoting)
- [Code Owners](#code-owners)
- [License](#license)
- [Contact](#contact)
<!--te-->

# 1. Getting Started

This repository contains the server-side heart of our Dicu chatbot, containing everything related to the AI models that we utilized to make our bot intelligent.

# 2. Prerequisites

Please make sure that your system has the following platforms:

- `docker`
- `docker-compose`
- `python3.9`

To download docker-compose & docker you can run these commands. For more info you can click [here!](https://docs.docker.com/desktop/):

```bash
sudo snap install docker
sudo apt  install docker-compose
```

Then connect your user with the docker by running this command:

```bash
sudo usermod -aG docker $USER
```

To make sure everything that the docker has been installed please run:

```bash
docker ps
```

# 3. Installation

1. First get the code by either run this command through SSH:

   ```bash
   git clone git@gitlab.com:dicu.chat/server.git
   ```

   or through HTTPS:

   ```bash
   git clone https://gitlab.com/dicu.chat/server.git
   ```

2. Please Update the submodules by running this command:

   ```bash
   cd server
   git submodule update --init --recursive
   ```

3. Setup the .env file by compying the .env.example:

   ```bash
   cp .env.example .env
   ```

4. Fill the `.env` file with the proper tokens Please check the documentation [here!]('https://gitlab.com/dicu.chat/server/-/blob/readme/doc/env.md')

# 4. Development

These steps will assist you in running the server without the use of Docker.

1. Pull the database image :

   ```bash
   docker-compose pull db
   ```

2. Run database image:
   ```bash
   docker network create caddynet
   docker-compose up -d db
   ```

**_Note before running the server_**: in the following section alias `python` is used for `python3`. Change it accordingly to your python (Windows `py`, Linux `python3` etc).

3. Install environment:

   ```bash
   virtualenv -p python3.9 .venv && source .venv/bin/activate
   # or, if you don't have 'virtualenv' util installed:
   # python -m venv .venv && source .venv/bin/activate
   ```

4. Install dependencies:

   ```bash
   python -m pip install -U pip wheel setuptools
   python -m pip install -r requirements.txt
   ```

5. Start the server:
   ```bash
   python -m server
   ```

The server will function as follows:

![Server](./assets/serverWorking.png)

To begin a conversation with your bot that you created using botsociety, go to the [brdige-telegram](https://gitlab.com/dicu.chat/bridge-telegram) repo and follow the steps for running it, then go to your telegram bot channel and begin your adventure.Please click [here!](https://gitlab.com/dicu.chat/server/-/blob/readme/doc/telegramBot.md) if you are lost after this.

# 5. Test

Please download the Additional dependencies to run tests locally for testing purposes of the development part.
On ubuntu:

```bash
python -m pip install -r test-requirements.txt
sudo apt-get install tidy
```

if you dont have [dotenv-linter](https://github.com/dotenv-linter/dotenv-linter) you can download it by running this command:

```bash
curl -sSfL https://raw.githubusercontent.com/dotenv-linter/dotenv-linter/master/install.sh | sh -s
```

then run the test file:

```bash
./test.sh
```

# 6. Production

For production setup look into [examples](./examples/all) directory, which contains full dicu.chat stack setup example.

# 7. Update

To get the lastest versions of submodules like rasa and gpt2 please run this command:

```bash
git submodule update --recursive --remote
```

# 8. Demo:

https://gitlab.com/-/ide/project/dicu.chat/server/edit/readme/-/assets/DicuChat_Short.mp4

# 9. Troubleshooting

If you have encountered any problems while running the code, please open a new issue in this repo and label it bug, and we will assist you in resolving it.

# 10. Code Owners

@morphysm/team :sunglasses:

# 11. License

Our reposiorty is licensed under the terms of the GPLv3 see the license [here!](https://gitlab.com/dicu.chat/server/-/blob/master/LICENSE)


# 12. Contact

If you'd like to know more about us [Click here!](https://www.morphysm.com/), or You can contact us at "contact@morphysm.com".

