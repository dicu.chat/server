# Static code analyzer.
flake8==3.9.2
flake8-quotes==3.3.0
flake8-bugbear==21.4.3

# Static code analyzer.
mypy==0.910

# mypy stubs (external static type checking info).
types-ujson==0.1.1
types-pytz==2021.1.2
types-aiofiles==0.1.9
types-requests==2.25.6
types-emoji==1.2.4

# File analyzer, allows for custom rules (code).
git+https://github.com/kittyandrew/renag.git

# Pytest - Python Testing Framework.
pytest==6.2.4
pytest-sanic==1.8.1
pytest-asyncio==0.15.1
pytest-dependency==0.5.1
# Additional pytest-like testing things.
asynctest==0.13.0

# Other
pytidylib==0.3.2  # HTML parser, for verifying page rendering.
requests==2.26.0  # Used in tests to get headers (HEAD) without downloading files.
