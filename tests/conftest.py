#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from server.__main__ import handler as _handler, public, api  # , routines
from server.db import Database, PermissionLevel, User
from server.states.ai_state import AIState
from server.settings import tokens, Config
from server.definitions import Context
from datetime import datetime
from secrets import token_hex
from copy import deepcopy
from typing import List
from sanic import Sanic
import pytest
import pytz

from .db_mock import Mock


# Ugh.
_handler.db.aiodynamo = Mock()
_handler.db.users = Mock()
_handler.db.check_backs = Mock()
_handler.db.access_tokens = Mock()
_handler.db.sessions = Mock()
_handler.db.string_items = Mock()
_handler.db.external_services = Mock()


@pytest.fixture
def db():
    db = Database()
    db.aiodynamo = Mock()
    db.users = Mock()
    db.check_backs = Mock()
    db.access_tokens = Mock()
    db.sessions = Mock()
    db.string_items = Mock()
    db.external_services = Mock()
    yield db


@pytest.fixture
def test_app(loop, sanic_client, db):
    app = Sanic(name=token_hex(16), strict_slashes=False)

    # app.add_task(_handler.db.init)

    app.blueprint(api.init(_handler, db))
    app.blueprint(public.init(_handler, db))
    # app.blueprint(routines.init(_handler, db))

    return loop.run_until_complete(sanic_client(app))


@pytest.fixture(scope="function")
def source_headers():
    # Test session:
    tokens["Test Dummy Bot"] = Config("TEST_BOT_1111", "http://dummy_url")

    return {
        "viaInstance": "Test Dummy Bot",
        "serviceIn": "WhatsApp",
        "serviceOut": None,
    }


@pytest.fixture(scope="function")
def headers(source_headers):
    return {
        "Instance-Name": source_headers["viaInstance"],
        "Service-In": source_headers["serviceIn"],
        "Authorization": "Bearer TEST_BOT_1111",
    }


@pytest.fixture(scope="function")
def source_data():
    return {
        "user": {"userId": "id-1232131"},
        "chat": {"chatId": "id-2131321"},
        "message": {"text": ""},
    }


@pytest.fixture(scope="function")
def parsed_data():
    return {
        "request": {
            "user": {
                "userId": "id-1232131",
                "firstName": None,
                "lastName": None,
                "username": None,
                "langCode": None,
                "identity": "ad81acd60da06f9f3ca84526b5a3f2db200c3b5c65702599797fadaff1cae57b",
            },
            "chat": {"chatId": "id-2131321", "name": None, "username": None},
            "message": {"text": "", "voice": None},
            "files": [],
            "buttons": [],
            "context": {},
            # Headers that we add in the end, because they are added in the end to the context,
            # so since we are comparing objects 1-to-1, this is important..
            "viaInstance": "Test Dummy Bot",
            "serviceIn": "WhatsApp",
            "serviceOut": None,
        }
    }


@pytest.fixture(scope="function")
def context(source_data, source_headers) -> Context:
    ctx = Context.from_json(source_data, source_headers)
    assert ctx.validated

    ctx_obj: Context = ctx.object
    assert ctx_obj

    return ctx_obj


@pytest.fixture(scope="function")
def user() -> User:
    assert hasattr(PermissionLevel, "DEFAULT")
    return {
        "user_id": "unique-id-123",
        "service": "telegram",
        "identity": "super-unique",
        "via_instance": "telegram-1",
        "first_name": "tester",
        "last_name": "rose",
        "username": "rose_tester",
        "language": "en",
        "created_at": datetime.now(tz=pytz.utc).isoformat(),
        "last_active": datetime.now(tz=pytz.utc).isoformat(),
        "answers": dict(),
        "files": dict(),
        "states": ["ENDState"],
        "permissions": PermissionLevel.DEFAULT,
        "context": dict(),
        "location": None,
        "conversation": None,
    }


@pytest.fixture(scope="function")
def handler():
    return _handler


@pytest.fixture(scope="function")
def ai_state(user, handler):
    _ai_state = AIState(
        db        = handler.db,
        tr        = handler.translator,
        nlu       = handler.nlu,
        strings   = handler.strings,
        bots_data = handler.bots_data,
        user      = user,
    )
    _ai_state.set_language("en")
    return _ai_state


@pytest.fixture(scope="function")
def users(user) -> List[User]:
    # First user is base user
    user1 = deepcopy(user)

    user2 = deepcopy(user)
    user2["user_id"]    = "new id 00010"
    user2["service"]    = "facebook"
    user2["identity"]   = "axf1tv1br1n"
    user2["first_name"] = "broadcaster"
    user2["last_name"]  = "tyson"

    assert hasattr(PermissionLevel, "ADMIN")
    user2["permissions"] = PermissionLevel.ADMIN

    user3 = deepcopy(user)
    user3["user_id"]    = "fake id 1111111"
    user3["service"]    = "whatsapp"
    user3["identity"]   = "new cool identity"
    user3["first_name"] = "me"
    user3["last_name"]  = "(that guy)"

    assert hasattr(PermissionLevel, "ADMIN")
    user3["permissions"] = PermissionLevel.ADMIN

    return [user1, user2, user3]


@pytest.fixture(scope="function")
def setup_headers():
    return {
        "Authorization": "Bearer TEST_TOKEN_123",
    }


@pytest.fixture(scope="function")
def setup_data():
    return {
        "url": "https://test.com",
    }


@pytest.fixture(scope="function")
def raw(source_data):

    raw_data = deepcopy(source_data)
    return raw_data
