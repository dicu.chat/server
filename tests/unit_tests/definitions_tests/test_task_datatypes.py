#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
import server.definitions
import pytest


@pytest.mark.dependency(name="attr_dtypes")
@pytest.mark.parametrize("dtype", ["SenderTask", "ExecutionTask"])
def test_contains_required_task_datatypes(dtype):
    assert hasattr(server.definitions, dtype)


@pytest.mark.dependency(depends=["attr_dtypes"])
@pytest.mark.parametrize("dtype, attributes", [
    ("SenderTask", ["service", "request"]),
    ("ExecutionTask", ["func", "args", "kwargs"])
])
def test_contains_required_according_task_attributes(dtype, attributes):
    datatype = getattr(server.definitions, dtype)

    for attr in attributes:
        assert hasattr(datatype, attr)
