#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from server.definitions import Context
import logging
import pytest


bad_data = [
    {"nothing": "empty"},
    {
        "viaInstance": "UNIQUE_ID",
        "serviceIn": "WhatsApp",
        "serviceOut": "Facebook",
        "user": {},
    },
    {
        "user": {"user_id": 10000000},
        "chat": {"chat_id": 99999999}
    }
]


# Actual tests


@pytest.mark.dependency(name="attr_context")
def test_context_has_expected_values(source_data, source_headers):
    result = Context.from_json(source_data, source_headers)

    assert hasattr(result, "validated")
    assert hasattr(result, "object")
    assert hasattr(result, "error")


@pytest.mark.dependency(name="ctx_from_json", depends=["attr_context"])
def test_context_from_json_success(source_data, source_headers):
    result = Context.from_json(source_data, source_headers)

    assert result.validated is True


@pytest.mark.dependency(name="ctx_returns_obj", depends=["attr_context", "ctx_from_json"])
def test_context_from_json_success_and_returned_object(source_data, source_headers, parsed_data):
    result = Context.from_json(source_data, source_headers)
    assert result.validated is True

    obj = result.object
    logging.info("parsed:\n%s\n\nexpected:\n%s", str(obj), str(parsed_data))
    # Compare representation as strings, because different types
    assert str(obj) == str(parsed_data)


@pytest.mark.dependency(depends=["attr_context", "ctx_from_json"])
@pytest.mark.parametrize("data", bad_data)
def test_context_from_json_unvalidated_on_bad_data(data, source_headers):
    result = Context.from_json(data, source_headers)

    assert result.validated is False


# Getting correct source but wrapped into weird types


@pytest.mark.dependency(depends=["attr_context", "ctx_from_json"])
def test_context_from_json_unvalidated_string_type(source_data, source_headers):
    result = Context.from_json(str(source_data), source_headers)

    assert result.validated is False


@pytest.mark.dependency(depends=["attr_context", "ctx_from_json"])
def test_context_from_json_unvalidated_tuple_type(source_data, source_headers):
    result = Context.from_json((source_data,), source_headers)

    assert result.validated is False
