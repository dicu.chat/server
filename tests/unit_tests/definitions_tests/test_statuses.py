#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from server.definitions.status import BaseStatus
import server.definitions
import pytest


@pytest.mark.dependency(name="attr_statuses")
@pytest.mark.parametrize("status", ["OK", "END", "BACK", "GO_TO_STATE"])
def test_all_required_statuses_exist(status):
    assert hasattr(server.definitions, status)


@pytest.mark.dependency(depends=["attr_statuses"])
@pytest.mark.parametrize("status", ["OK", "END", "BACK", "GO_TO_STATE"])
def test_all_statuses_inherit_from_base(status):
    status_class = getattr(server.definitions, status)

    assert issubclass(status_class, BaseStatus)
