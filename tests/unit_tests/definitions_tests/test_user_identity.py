#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from server.definitions import UserIdentity
import pytest


@pytest.mark.dependency(name="attr_identity")
def test_useridentity_has_hash_function():
    assert hasattr(UserIdentity, "hash")


@pytest.mark.dependency(depends=["attr_identity"])
@pytest.mark.parametrize("user_id, service", [
    ("id-12345", "unique-1"),
    ("user-id-11111", "service-9128"),
    (9991811245, "telegram"),
])
def test_useridentity_hash_returns_consistent(user_id, service):
    val1 = UserIdentity.hash(user_id, service)
    val2 = UserIdentity.hash(user_id, service)

    assert val1 == val2


@pytest.mark.dependency(depends=["attr_identity"])
def test_useridentity_hash_returns_unique():
    values = [
        ("id-12345", "unique-1"),
        ("user-id-11111", "service-9128"),
        (9991811245, "telegram"),
        ("id_super_unique", "website #1"),
    ]
    results = [UserIdentity.hash(usr, service) for usr, service in values]

    assert len(values) == len(results)
