#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from server.utils.parser import parse_api, get_text
import logging
import pytest


# TODO: maybe some default (e.g. expected_types) might be an enum
#       at some point, then we can make sure it is one of the values
default_values = {
    "id": bool,  # make sure it's not "" or None
    "type": lambda o: o and type(o) == str,
    "is_left_side": lambda o: type(o) == bool,
    "buttons": lambda o: type(o) == list,
    "free_answer": lambda o: type(o) == bool,
    "multichoice": lambda o: type(o) == bool,
    "text_key": lambda o: o and type(o) == str,
    "commands": lambda o: type(o) == dict,
    "expected_type": lambda o: o and type(o) == str,
    "text": lambda o: type(o) == str,
    "is_first_message": lambda o: type(o) == bool,
}
expected_keys = default_values.keys()
raw_data = (
    {
        "id": "1",
        "is_left_side": True,
        "text": "First Message.",
        "type": "text",
        "next_message": "2",
        "is_first_message": True,
    },
    {
        "id": "2",
        "is_left_side": True,
        "text": None,
        "type": "quickreplies",
        "next_message": None,
        "is_first_message:": False,
        "quickreplies": [
            {"text": "text1", "next_message": "3"},
            {"text": "text2", "next_message": "3"},
        ],
    },
    {
        "id": "3",
        "is_left_side": True,
        "text": "Message with command: #special",
        "type": "text",
        "next_message": "4",
        "is_first_message": False,
        "buttons": [
            {"text": "textX", "next_message": "4"},
        ],
    },
    {
        "id": "4",
        "is_left_side": False,
        "text": "Some User Input",
        "type": "text",
        "next_message": "41",
        "is_first_message": False,
    },
    {
        "id": "41",
        "is_left_side": True,
        "text": "My special buttons go next #special_answer=score",
        "type": "text",
        "next_message": "42",
        "is_first_message": False,
    },
    {
        "id": "42",
        "is_left_side": True,
        "text": None,
        "type": "quickreplies",
        "next_message": None,
        "is_first_message": False,
        "quickreplies": [
            {"text": "answer1 #p(score=10)", "next_message": None},
            {"text": "answer2 #p(score=27)", "next_message": None},
            {"text": "answer3 #p(score=-2)", "next_message": None},
        ],
        "command": {
            "cmd1": {
                "arg1": 1,
                "arg2": 2
            },
            "cmd2": {
                "arg3": 3,
                "arg4": 4
            }
        }
    },
)

parsed_data = parse_api(raw_data)
parsed_ids = [msg["id"] for msg in parsed_data]
all_parsed_btns = list()
for msg in parsed_data:
    all_parsed_btns.extend(msg.get("buttons", []))
all_unique_objects = parsed_data + all_parsed_btns

command_parser_values = (
    (
        "",
        (
            "",
            False,
            {},
        )
    ),
    (
        "test",
        (
            "test",
            False,
            {},
        )
    ),
    (
        "command #test",
        (
            "command",
            True,
            {
                "test": []
            },
        )
    ),
    (
        "multiple commands #ai #warning #record",
        (
            "multiple commands",
            True,
            {
                "ai": [],
                "warning": [],
                "record": [],
            },
        )
    ),
    (
        "command with value #wait(seconds=60)",
        (
            "command with value",
            True,
            {
                "wait": [
                    {
                        "seconds": 60,
                    },
                ],
            },
        )
    ),
    (
        "one command multiple values #run(task=add, arg1=2, arg2=81)",
        (
            "one command multiple values",
            True,
            {
                "run": [
                    {
                        "task": "add",
                        "arg1": 2,
                        "arg2": 81,
                    },
                ],
            }
        )
    ),
    (
        "bunch of everything #stop #run(task=do_x,x='something') #set_mode(mode='play')",
        (
            "bunch of everything",
            True,
            {
                "stop": [],
                "run": [
                    {
                        "task": "do_x",
                        "x": "something",
                    },
                ],
                "set_mode": [
                    {
                        "mode": "play",
                    },
                ]
            }
        )
    )
)
emoji_data = (
    (":heart:", "❤️"),
    ("Hello :slightly_smiling_face:!", "Hello 🙂️!"),
    ("Very cool :exploding_head::face_with_tears_of_joy::face_with_tears_of_joy:"
     ":face_with_tears_of_joy::rolling_on_the_floor_laughing::rolling_on_the_floor_laughing:"
     ":smiling_face:!!!", "Very cool 🤯️😂️😂️😂️🤣️🤣️☺️!!!"),
)


@pytest.fixture
def get_all_unique_objects():
    return all_unique_objects


# Actual tests


@pytest.mark.parametrize("text, expected", command_parser_values)
def test_get_text_parsing_function(text, expected):
    result = get_text(text)
    assert result == expected


@pytest.mark.parametrize("text, expected", emoji_data)
def test_get_text_test_emojies(text, expected):
    result, *_ = get_text(text)
    logging.error("\nResult: '%s'\nExpect: '%s'", result.encode(), expected.encode())
    assert result == expected


@pytest.mark.dependency(name="attr_parsed_api")
@pytest.mark.parametrize("parsed_msg", parsed_data)
@pytest.mark.parametrize("expected_key", expected_keys)
def test_all_messages_have_expected_keys(parsed_msg, expected_key):
    # Check if the key is there
    assert expected_key in parsed_msg


@pytest.mark.dependency(depends=["attr_parsed_api"])
@pytest.mark.parametrize("parsed_msg", parsed_data)
@pytest.mark.parametrize("key, check", default_values.items())
def test_all_messages_have_expected_keywords_value_and_type(parsed_msg, key, check):
    # Check if the default value is what expected
    assert check(parsed_msg[key])


@pytest.mark.dependency(depends=["attr_parsed_api"])
@pytest.mark.parametrize("raw_msg", raw_data)
def test_all_messages_have_correctly_been_parsed(raw_msg):
    # Check if second message (type = quickreplies) is merged
    if raw_msg["type"] == "quickreplies":
        assert raw_msg["id"] not in parsed_ids
    # Check if user input was parsed correctly
    if not raw_msg["is_left_side"]:
        assert raw_msg["id"] not in parsed_ids
    # TODO: anything else?


@pytest.mark.dependency(name="attr_btns_pass", depends=["attr_parsed_api"])
@pytest.mark.parametrize("btn", all_parsed_btns)
def test_added_keys_to_the_buttons(btn):
    assert "text_key" in btn


@pytest.mark.dependency(depends=["attr_parsed_api", "attr_btns_pass"])
def test_all_keys_are_unique(get_all_unique_objects):
    # Collect all unique keys
    cache_list = list()
    for obj in get_all_unique_objects:
        cache_list.append(obj["text_key"])
    # Post-processing
    cache_set = set(cache_list)

    # Check for unique-ness
    assert len(cache_list) == len(cache_set)
