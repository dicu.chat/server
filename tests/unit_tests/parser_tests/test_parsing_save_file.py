#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from server.utils import save_file
import tempfile
import pytest
import shutil
import time
import json
import os


# All data to write.
files_data = [
    {},
    # Generate many multiple-value files
    *({f"key-#{n}": f"value-#{n}" for n in range(i)} for i in range(10)),
]


@pytest.fixture(scope="module")
def serve_temp_dir_for_save_file():
    tmp_path = tempfile.mkdtemp()
    yield tmp_path
    shutil.rmtree(tmp_path)


# Actual tests


@pytest.mark.parametrize("data", files_data)
def test_save_file_parser_function_writes_to_latest(data, serve_temp_dir_for_save_file):
    # Write the file
    save_file(data, path=serve_temp_dir_for_save_file)
    # Read new file
    with open(os.path.join(serve_temp_dir_for_save_file, "latest.json")) as latest:
        written_data = json.load(latest)

    # @Note: we have botsociety namespace hardcoded here.
    assert written_data == {"main": data}


@pytest.mark.parametrize("data", files_data)
def test_save_file_parser_function_dumps_up_to_5_files(data, serve_temp_dir_for_save_file):
    # Write the file
    save_file(data, path=serve_temp_dir_for_save_file)
    # Read file list
    history = os.listdir(serve_temp_dir_for_save_file)

    # Total length is 5 or less.
    assert len(history) <= 5
    # It still must contain "latest.json".
    assert "latest.json" in history
    # Add sleep for the sake of avoiding rewriting the same file.
    time.sleep(0.001)
