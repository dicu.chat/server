#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from server.settings import TEST_ALLOW_NETWORK_LOAD
from asynctest import CoroutineMock, patch
from server.strings import TextPromise
from server.base import BaseState
from typing import Dict,  Union
from copy import deepcopy
from time import time
import requests
import logging
import shutil
import pytest
import os

from . import isstatus


class CoolState(BaseState):
    pass


btn_data = {
    "my_text_test": "my superficial strings number 1",
    "ok_test": "Ok",
    "no_test": "Not",
    "okie_dokie_test": "Yeah. Okie. Fine",
}
connections: Dict[str, Dict[str, Union[str, bool]]] = {
    "wikipedia": {
        "url": "https://www.wikipedia.org/portal/wikipedia.org/assets/img/Wikipedia-logo-v2.png",
        "status": False,
    },
    "speedtest": {
        "url": "http://speedtest.tele2.net/1MB.zip",
        "status": False,
    },
    "vodafone": {
        "url": "http://212.183.159.230/5MB.zip",
        "status": False,
    },
    "fastest.fish": {
        "url": "http://ipv4.download.thinkbroadband.com/5MB.zip",
        "status": False,
    },
}
for name in connections:
    try:
        requests.head(connections[name]["url"], timeout=(1, 2))  # type: ignore [arg-type]
        connections[name]["status"] = True
    except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
        logging.warning("Couldn't connect to %s!", name)

test_files_url = []

if connections["wikipedia"]["status"]:
    test_files_url.extend([
        ("wikipedia.png", connections["wikipedia"]["url"]),  # 15 KB
    ])

if TEST_ALLOW_NETWORK_LOAD and connections["speedtest"]["status"]:
    test_files_url.extend([
        ("1MB.zip", "http://speedtest.tele2.net/1MB.zip"),  # 1 MB
        ("10MB.zip", "http://speedtest.tele2.net/10MB.zip"),  # 10 MB
        ("100MB.test", "http://speedtest.tele2.net/100MB.zip"),  # 100 MB
        ("1000MB.zip", "http://speedtest.tele2.net/1GB.zip"),  # 1 GB
    ])
# Fallback mirror number 1
elif TEST_ALLOW_NETWORK_LOAD and connections["vodafone"]["status"]:
    test_files_url.extend([
        ("5MB.zip", "http://212.183.159.230/5MB.zip"),  # 5 MB
        ("10MB.zip", "http://212.183.159.230/10MB.zip"),  # 10 MB
        ("100MB.test", "http://212.183.159.230/100MB.zip"),  # 100 MB
        ("1000MB.zip", "http://212.183.159.230/1GB.zip"),  # 1 GB
    ])
# Fallback mirror number 2
elif TEST_ALLOW_NETWORK_LOAD and connections["fastest.fish"]["status"]:
    test_files_url.extend([
        ("5MB.zip", "http://ipv4.download.thinkbroadband.com/5MB.zip"),  # 5 MB
        ("10MB.zip", "http://ipv4.download.thinkbroadband.com/10MB.zip"),  # 10 MB
        ("100MB.test", "http://ipv4.download.thinkbroadband.com/100MB.zip"),  # 100 MB
        ("1000MB.zip", "http://ipv4.download.thinkbroadband.com/1GB.zip"),  # 1 GB
    ])

if not test_files_url:
    logging.warning(
        "Seems like all connections over network are unreachable. Download test will be aborted."
    )


@pytest.fixture(scope="function")
def test_state(user, handler):
    u = deepcopy(user)
    # Overriding to test session
    u["via_instance"] = "Test Dummy Bot"
    ts = CoolState(
        db        = handler.db,
        tr        = handler.translator,
        nlu       = handler.nlu,
        strings   = handler.strings,
        bots_data = handler.bots_data,
        user      = u,
        handler   = handler,
        free_tas  = None,
        tas       = None,
        gtasks    = handler.delayed_tasks,
    )
    ts.set_language("en")
    # Make sure to monkeypatch real data
    ts.STRINGS.cache["en"] = {key: {"text": value} for key, value in btn_data.items()}
    # Monkeypatch file path
    test_fp = "./test_media_temp_dir"
    try:
        shutil.rmtree(test_fp)
    except FileNotFoundError:
        pass
    os.mkdir(test_fp)
    ts.media_path = test_fp
    yield ts
    shutil.rmtree(test_fp)


# Actual tests


@pytest.mark.asyncio
async def test_base_state_entry_method(test_state, context):
    logging.info("state: %s, context: %s", test_state, context)
    status = await test_state.entry(context)
    assert isstatus(status)


@pytest.mark.asyncio
async def test_base_state_process_method(test_state, context):
    status = await test_state.process(context)
    assert isstatus(status)


@pytest.mark.parametrize("text", ["Dumb text", "My test", "K"])
def test_base_parse_button_no_truncation_bad_text(test_state, text):
    btn = test_state.parse_button(text, verify = (TextPromise("en", key) for key in btn_data))
    assert btn.key is None


@pytest.mark.parametrize("text", ["Very loooooooooong dumb text", "No", "Yeah."])
def test_base_parse_button_has_truncation_bad_text(test_state, text):
    btn = test_state.parse_button(
        text, truncated=True, truncation_size=10,
        verify = (TextPromise("en", key) for key in btn_data)
    )
    assert btn.key is None


@pytest.mark.parametrize("text", btn_data.values())
def test_base_parse_button_no_truncation_custom_obj(test_state, text):
    btn = test_state.parse_button(text, verify = (TextPromise("en", key) for key in btn_data))

    assert btn.key
    expected = btn_data.get(btn.key, "")
    assert expected == text


@pytest.mark.parametrize("text", btn_data.values())
def test_base_prase_button_has_truncation_custom_obj(test_state, text):
    btn = test_state.parse_button(
        text, truncated=True, truncation_size=10,
        verify = (TextPromise("en", key) for key in btn_data)
    )

    assert btn.key
    expected = btn_data.get(btn.key, "")
    assert expected == text


@pytest.mark.asyncio
@pytest.mark.parametrize("fp, file_url", test_files_url)
async def test_base_download_by_url_method(test_state, fp, file_url):
    # Get file size of the file
    headers = requests.head(file_url).headers
    url_size = int(headers["content-length"])
    # Actually download file, returns new filepath
    downloaded_fp = await test_state.download_by_url(file_url, fp)
    # Read filesize from the disk
    downloaded_size = os.stat(downloaded_fp).st_size

    assert url_size == downloaded_size


@pytest.mark.asyncio
@pytest.mark.parametrize("event_name", ["bad", "random", "test"])
async def test_base_send_event_not_implemented(test_state, context, event_name):
    with pytest.raises(NotImplementedError, match=r"^Event \w+ is not implemented!$"):
        await test_state.send_event(event_name, context)


@pytest.mark.asyncio
@pytest.mark.parametrize("event_name", ["typing", "recording_voice"])
async def test_base_send_event_default_message_amount(test_state, context, event_name):
    with pytest.raises(AssertionError):
        await test_state.send_event(event_name, context)


@pytest.mark.asyncio
@pytest.mark.parametrize("event_name", ["typing", "recording_voice"])
async def test_base_send_event_bad_message_amount(test_state, context, event_name):
    with pytest.raises(AssertionError):
        await test_state.send_event(event_name, context, message_count=-1)

    with pytest.raises(AssertionError):
        await test_state.send_event(event_name, context, message_count= -(len(event_name)))


@pytest.mark.asyncio
@pytest.mark.parametrize("event_name", ["typing", "recording_voice"])
@patch("aiohttp.ClientSession.post")
async def test_base_send_event_good_typing_like(mock_post, test_state, context, event_name):
    mock_post.return_value.__aenter__.return_value.status = 200
    mock_post.return_value.__aenter__.return_value.json = CoroutineMock()
    mock_post.return_value.__aenter__.return_value.json.return_value = {"time": time(), "status": 200}

    await test_state.send_event(event_name, context, message_count=1)

    await test_state.send_event(event_name, context, message_count=len(event_name))

    assert mock_post.call_count == 2
    assert mock_post.return_value.__aenter__.return_value.json.call_count == 2
