#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from server.states.authorization_state import AuthorizationListTokensState
import pytest


test_data = (
    ("Identity(2)",   0, 4),
    ("Identity(4)",   2, 2),
    ("Identity(0)",   1, 20),
    ("Identity(12)",  7, 5),
    ("Identity(999)", 24, 15),
)


@pytest.fixture(scope="function")
def access_tokens():
    return [{"identity": f"Identity({i})", "token": f"Token({i})"} for i in range(250)]


@pytest.fixture(scope="function")
def atm_state(user, handler):
    state = AuthorizationListTokensState(
        db        = handler.db,
        tr        = handler.translator,
        nlu       = handler.nlu,
        strings   = handler.strings,
        bots_data = handler.bots_data,
        user      = user,
        handler   = handler,
        free_tas  = None,
        tas       = None,
        gtasks    = handler.delayed_tasks,
    )
    state.set_language("en")
    return state


# Actual tests


@pytest.mark.asyncio
@pytest.mark.parametrize("identity, page, page_size", test_data)
async def test_access_tokens_menu_state_get_page_method_empty_data(atm_state, monkeypatch, user, identity, page, page_size, handler):
    # Patched check_websession
    async def patched_qat_empty(identity, page, page_size):
        return [], 0

    # Monkeypatch db object
    monkeypatch.setattr(handler.db, "query_access_tokens", patched_qat_empty)

    user["context"]["tokens_menu"] = {
        "current_page": {},
    }
    text, total, buttons = await atm_state.get_page(identity, page, page_size)

    assert text is None
    assert total == 0


@pytest.mark.asyncio
@pytest.mark.parametrize("identity, page, page_size", test_data)
async def test_access_tokens_menu_state_get_page_method_with_data(atm_state, access_tokens, monkeypatch, user, identity, page, page_size, handler):
    # Patched check_websession
    async def patched_query_access_tokens(identity, page, page_size):
        return access_tokens[page*page_size : page*page_size + page_size], len(access_tokens)  # noqa: E203, E226

    # Monkeypatch db object
    monkeypatch.setattr(handler.db, "query_access_tokens", patched_query_access_tokens)

    user["context"]["tokens_menu"] = {
        "current_page": {},
    }
    text, total, buttons = await atm_state.get_page(identity, page, page_size)

    if text:
        assert text.key == "authorizedListToken"
        assert page * page_size < total
    else:
        assert page * page_size > total
    assert total == 250
