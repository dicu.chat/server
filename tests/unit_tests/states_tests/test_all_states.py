#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from . import isstatus, states, texts
from copy import deepcopy
import itertools
import pytest


@pytest.fixture
def merged_data(users):
    return list(itertools.product(users, texts))


@pytest.mark.asyncio
@pytest.mark.parametrize("state_name, state", states)
async def test_all_states_entry_return_status_codes(state_name, state, merged_data, context, db, handler):
    ctx = deepcopy(context)
    assert ctx

    assert state_name

    for user, text in merged_data:
        ctx["message"]["text"] = text
        state_obj = state(
            db        = handler.db,
            tr        = handler.translator,
            nlu       = handler.nlu,
            strings   = handler.strings,
            bots_data = handler.bots_data,
            user      = user,
            handler   = handler,
            free_tas  = None,
            tas       = None,
            gtasks    = handler.delayed_tasks,
        )

        # check if user has language
        assert "language" in user
        state_obj.set_language(user["language"])

        status = await state_obj.entry(ctx)
        assert isstatus(status)


@pytest.mark.asyncio
@pytest.mark.parametrize("state_name, state", states)
async def test_all_states_process_returns_status_codes(state_name, state, users, context, db, handler, monkeypatch):
    ctx = deepcopy(context)
    assert ctx

    assert state_name

    for user in users:

        # Patch methods/functions:
        async def _mp_get_response(self, prompt: str, context, key, history):
            context["message"]["text"] = f"Prompt({prompt})"

        monkeypatch.setattr(state, "handle_invalid_answer", _mp_get_response)

        state_obj = state(
            db        = handler.db,
            tr        = handler.translator,
            nlu       = handler.nlu,
            strings   = handler.strings,
            bots_data = handler.bots_data,
            user      = user,
            handler   = handler,
            free_tas  = None,
            tas       = None,
            gtasks    = handler.delayed_tasks,
        )

        assert "language" in user
        state_obj.set_language(user["language"])

        status = await state_obj.entry(ctx)
        assert isstatus(status)

        for text in texts:
            ctx["message"]["text"] = text
            status = await state_obj.process(ctx)
            assert isstatus(status)
