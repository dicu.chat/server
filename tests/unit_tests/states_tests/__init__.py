#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from server.definitions.status import BaseStatus
import server.states as states_module
import inspect


def isstatus(obj) -> bool:
    if not inspect.isclass(obj):
        obj = type(obj)
    return issubclass(obj, BaseStatus)


all_states = {state.__name__: state for state in states_module.collect()}
assert all_states
states = list()

ordered_states = [
    "StartState",
    "QAState",
    "GetIdState",
    "ENDState"
]
for st in ordered_states:
    assert st in all_states
    state_class = all_states[st]
    states.append((st, state_class))

texts = [
    "start",
    "Accept",
    "Yes",
    "Yes",
    "Yes",
    "Yes",
    "No",
    "Yes",
    "/start",
    "Hi",
    "Back",
    "Stop",
    "Some random medium size text that should not cause any problems",
    "AAAAA" * 25,
    "бабушка",
    "\xac\x48\xfd\xd1\x70\x4c\x40\xf5\xe9\x40\xc4\x0a\xe5\x5c\x2c\x2c\xec\x03\x56\x96\xc8",
    "Are you human?",
    "What languages do you know",
    "Can you talk fast?",
    "My WPM is 420 words by the way. You know that it's great sign of intelligence?",
    "Don't matter if I remember anything tho",
    "Anyway, you are boring, bye",
]
