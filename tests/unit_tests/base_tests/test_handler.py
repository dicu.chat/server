#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from copy import deepcopy
import pytest

from server.db import PermissionLevel


@pytest.mark.asyncio
async def test_handler__get_state_existing_state(handler, user):
    real_state = "QAState"
    success, state, state_name = handler._Handler__get_state(real_state, user)

    assert success is True
    assert state.__class__.__name__ == real_state
    assert state_name == real_state


@pytest.mark.asyncio
async def test_handler_get_state_non_existing_state(handler, user):
    bad_state = "ThisDefinitelyWon'tExistState"
    success, state, state_name = handler._Handler__get_state(bad_state, user)

    assert success is False
    assert state.__class__.__name__ == "StartState"
    assert state_name == "StartState"


@pytest.mark.asyncio
@pytest.mark.parametrize("name", ("StartState", "AIState", "UnknownState", "MYCOOLSTATE", ""))
async def test_handler_get_state_multiple(handler, name, user):
    success, state, state_name = handler._Handler__get_state(name, user)
    # Set accurate name, 'name' is the state was real, otherwise - start state.
    accurate_name = name if success else "StartState"

    # Success should be true if state is a valid one.
    assert success is (name in handler._Handler__states)
    assert state.__class__.__name__ == accurate_name
    assert state_name == accurate_name


@pytest.fixture(scope="function")
def phandler(handler, monkeypatch, user):

    async def get_user(identity):
        if identity == "TEST_GREEN":
            return user

    monkeypatch.setattr(handler.db, "get_user", get_user)
    return handler


@pytest.mark.asyncio
async def test_handler_get_or_register_user_existing_user(phandler, context):
    context = context.deepcopy()

    context["user"]["identity"] = "TEST_GREEN"
    user = await phandler._Handler__get_or_register_user(context)

    # This will work well until 'user' fixture is changed.
    assert user["user_id"] == "unique-id-123"
    assert user["service"] == "telegram"
    assert user["identity"] == "super-unique"
    assert user["via_instance"] == "telegram-1"
    assert user["username"] == "rose_tester"
    assert user["language"] == "en"
    assert user["first_name"] == "tester"
    assert user["last_name"] == "rose"
    assert user["answers"] == {}
    assert user["files"] == {}
    assert user["context"] == {}
    assert user["states"] == ["ENDState"]
    assert user["permissions"] == 1

    assert user["location"] is None
    assert user["conversation"] is None

    # TODO: maybe also parse this as datetime
    assert "created_at" in user
    assert "last_active" in user


@pytest.mark.asyncio
async def test_handler_get_or_register_user_new_user(phandler, context):
    context = context.deepcopy()

    context["user"]["identity"] = "TEST_RED"
    user = await phandler._Handler__get_or_register_user(context)

    # This will work well until 'context' fixture is changed.
    assert user["user_id"] == "id-1232131"
    assert user["service"] == "WhatsApp"
    assert user["identity"] == "TEST_RED"
    assert user["via_instance"] == "Test Dummy Bot"
    assert user["username"] is None
    assert user["language"] == "en"
    assert user["first_name"] is None
    assert user["last_name"] is None
    assert user["answers"] == {}
    assert user["files"] == {}
    assert user["context"] == {}
    assert user["states"] == ["ENDState"]
    assert user["permissions"] == 0

    assert user["location"] is None
    assert user["conversation"] is None

    # TODO: maybe also parse this as datetime
    assert "created_at" in user
    assert "last_active" in user


@pytest.mark.asyncio
async def test_handler_get_command_state_expected_states(handler, context, user):
    for command, value in handler.commands.items():
        # @Robustness: if user doesn't have correct access rights for this, just skip forward.
        if value["owner_only"] and user["permissions"] != PermissionLevel.MAX:  continue

        context = deepcopy(context)

        context["message"]["text"] = command
        state_res = handler.get_command_state(user, context)

        assert state_res == value["state"]

        context["message"]["text"] = command + " some additional argument to the command that must be allowed"
        state_res = handler.get_command_state(user, context)

        assert state_res == value["state"]


@pytest.mark.asyncio
@pytest.mark.parametrize("text", ("/THIS_COMMAND_DOES_NOT_EXIST", "sample text", "cats are cute"))
async def test_handler_get_command_state_no_states(handler, context, user, text):
    context = deepcopy(context)

    context["message"]["text"] = text
    assert handler.get_command_state(user, context) is None


@pytest.mark.asyncio
@pytest.mark.parametrize("states", ([], ["ENDState"], ["StartState", "AIState"], ["AIState", "UnknownState"]))
async def test_handler_last_state_method(handler, user, states):
    user = deepcopy(user)

    user["states"] = states
    state = await handler.last_state(user)

    expected_state = states[-1] if states else "StartState"
    assert state == expected_state
