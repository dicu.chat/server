#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from server.external import TranslationAPI
import pytest


test_src = [
    ["ru", {
        "msg1": "Hello",
        "msg2": "Bye"
    }, {}],
    ["es", {
        "ok": "OK",
        "yes": "Yes",
        "no": "No",
        "stop": "Stop"
    }, {}],
    ["uk", {
        "string": "S"
    }, {}],
]


for each in test_src:
    for key in each[1]:
        each[2][key] = f"{each[1][key]} [en->{each[0]}]"  # type: ignore [index]


test_mltpl_src = [
    [("pl"), {
        "greet": "Hello"
    }],
    [("de", "he"), {
        "key_1": "Text One",
        "key_2": "Text Two",
        "key_3": "text Three",
    }],
]


for each in test_mltpl_src:
    each.append({lang: {} for lang in each[0]})
    for lang in each[0]:
        for key in each[1]:
            each[2][lang][key] = f"{each[1][key]} [en->{lang}]"  # type: ignore [index]


@pytest.fixture
def translator(monkeypatch):
    tr = TranslationAPI()
    # Replace translation method with mock
    monkeypatch.setattr(tr, "translate_text", tr.shim_translate_text)
    return tr


# Actual tests


@pytest.mark.asyncio
@pytest.mark.parametrize("lang, texts, correct_dict", test_src)
async def test_translator_translate_dict(lang, texts, correct_dict, translator):
    result = await translator.translate_dict(lang, texts)
    assert result == correct_dict


@pytest.mark.asyncio
@pytest.mark.parametrize("langs, texts, correct_dicts", test_mltpl_src)
async def test_translator_multiple_dicts(langs, texts, correct_dicts, translator):
    # Do translations
    result = await translator.translate_to_multiple_languages_dict(langs, texts)
    assert result == correct_dicts
