#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from . import bad_data
import pytest
import json


@pytest.mark.parametrize("data", bad_data)
async def test_sending_different_bad_data(test_app, data, setup_headers):
    response = await test_app.post("/api/setup", data=json.dumps(data), headers=setup_headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "expected json"


async def test_sending_no_token(test_app, setup_data, setup_headers):
    del setup_headers["Authorization"]
    response = await test_app.post("/api/setup", data=json.dumps(setup_data), headers=setup_headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "token unauthorized"


async def test_sending_empty_token(test_app, setup_data, setup_headers):
    setup_headers["Authorization"] = ""
    response = await test_app.post("/api/setup", data=json.dumps(setup_data), headers=setup_headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "token unauthorized"


async def test_sending_no_url(test_app, setup_data, setup_headers):
    del setup_data["url"]
    response = await test_app.post("/api/setup", data=json.dumps(setup_data), headers=setup_headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "expected json"


async def test_sending_empty_url(test_app, setup_data, setup_headers):
    setup_data["url"] = ""
    response = await test_app.post("/api/setup", data=json.dumps(setup_data), headers=setup_headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "url invalid"
