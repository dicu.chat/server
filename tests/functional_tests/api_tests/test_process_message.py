#
# Copyright (C) 2020 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
from . import bad_data
import pytest
import json


@pytest.mark.parametrize("data", bad_data)
async def test_process_message_sending_different_bad_data(test_app, data, headers):
    response = await test_app.post("/api/process_message", data=json.dumps(data), headers=headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "expected json"


async def test_process_message_send_bad_auth(test_app, source_data, headers):
    headers["Authorization"] = "Bearer token_test"
    response = await test_app.post("/api/process_message", data=json.dumps(source_data), headers=headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "token unauthorized (bad token)"


async def test_process_message_send_good_auth_and_bad_arg_service_in(test_app, raw, headers):
    # Prepare data
    del headers["Service-In"]
    # Make request
    response = await test_app.post("/api/process_message", data=json.dumps(raw), headers=headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "You must add 'Service-In' header, in which you must specify your specific service name. Like 'telegram' or 'web-widget'."


async def test_process_message_send_good_auth_and_bad_arg_user(test_app, raw, headers):
    # Prepare data
    del raw["user"]
    # Make request
    response = await test_app.post("/api/process_message", data=json.dumps(raw), headers=headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "'user' is a required property"


async def test_process_message_send_good_auth_and_bad_arg_user_id(test_app, raw, headers):
    # Prepare data
    del raw["user"]["userId"]
    # Make request
    response = await test_app.post("/api/process_message", data=json.dumps(raw), headers=headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "'userId' is a required property"


async def test_process_message_send_good_auth_and_bad_arg_chat(test_app, raw, headers):
    # Prepare data
    del raw["chat"]
    # Make request
    response = await test_app.post("/api/process_message", data=json.dumps(raw), headers=headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "'chat' is a required property"


async def test_process_message_send_good_auth_and_bad_arg_chat_id(test_app, raw, headers):
    # Prepare data
    del raw["chat"]["chatId"]
    # Make request
    response = await test_app.post("/api/process_message", data=json.dumps(raw), headers=headers)
    data = response.json()

    assert data["status"] == 403
    assert data["message"] == "'chatId' is a required property"


async def test_process_message_send_good_auth_and_good_minimum_data(test_app, raw, headers):
    # Make request
    response = await test_app.post("/api/process_message", data=json.dumps(raw), headers=headers)
    data = response.json()

    assert data["status"] == 200
    assert data == {"status": 200}


async def test_process_message_send_good_auth_and_good_data_and_additional_data(test_app, raw, headers):
    # Prepare data
    raw["files"] = []
    # Make request
    response = await test_app.post("/api/process_message", data=json.dumps(raw), headers=headers)
    data = response.json()

    assert data["status"] == 200
    assert data == {"status": 200}


async def test_process_message_send_good_auth_and_good_data(test_app, raw, headers):
    # Prepare data
    raw["message"]["text"] = "/start"
    raw["files"] = []
    # Make request
    response = await test_app.post("/api/process_message", data=json.dumps(raw), headers=headers)
    data = response.json()

    assert data["status"] == 200
    assert data == {"status": 200}
