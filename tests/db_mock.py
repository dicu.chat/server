#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#


# Note(andrew): Some of the things are deprecated since boto3 removal from the project.
class Mock:
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

        self.last_attr = None
        self.call_args = None
        self.call_kwargs = None

    def __getattr__(self, attr_name):
        self.last_attr = attr_name
        return self

    def __call__(self, *args, **kwargs):
        self.call_args = args
        self.call_kwargs = kwargs
        return self

    def __bool__(self):
        return False

    def __contains__(self, attr_name):
        # Returning false to avoid infinite loop in the methods that do 'while "LastEvaluatedKey" in Mock'.
        if attr_name == "LastEvaluatedKey":
            return False

        else:
            raise ValueError(f"Unhandled attr_name in the mock __contains__: {attr_name}")

    def __getitem__(self, attr_name):
        # Mocking all iters through Items in the db request response.
        if attr_name == "Items":
            return []

        # Mocking all checks for amount of 'Items' in the response.
        elif attr_name == "Count":
            return 0

        elif isinstance(attr_name, int):
            return self

        else:
            raise ValueError(f"Unhandled attr_name in the mock __getitem__: {attr_name}")

    def __iter__(self):
        return self

    def __next__(self):
        raise StopIteration

    def __aiter__(self):
        return self

    async def __anext__(self):
        raise StopAsyncIteration

    def __await__(self):
        return self

    def __str__(self):
        return "Mock(" \
            f"last_attr={self.last_attr}, " \
            f"call_args={self.call_args}, " \
            f"call_kwargs={self.call_kwargs}" \
            ")"
