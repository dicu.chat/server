#
# Copyright (C) 2021 dicu.chat
# This file is part of dicu.chat https://gitlab.com/dicu.chat
#
# dicu.chat is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3, as published
# by the Free Software Foundation.
#
# dicu.chat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# version 3 along with dicu.chat.  If not, see <http://www.gnu.org/licenses/>.
#
import pytest
import sys


if __name__ == "__main__":
    sys.exit(pytest.main([
        # Note(andrew): Disable all 'deprecation' warnings (we don't care about the
        #     deprecation in the testing framework(s), until we do). To enable all
        #     warnings use '-Wall'.
        "-W", "ignore::DeprecationWarning",
        # Note(andrew): Tests are mostly being ran on the CI (container), as a fresh
        #     new setup. So having different context on the CI and in the local setup
        #     is bad and confusing, because this leads to non-reproducable (or hardly
        #     reproducable) issues. Additionaly, this kind of caching does not provide
        #     enough positive impact (i.e. speed-up) to matter anyways.
        "-p", "no:cacheprovider",
        # Show top 5 the slowest setup/test functions that we executed in the test run.
        "--durations=5",
        # Use 'INFO' everywhere instead of the 'DEBUG', because some libraries (like boto3,
        # a amazon library that is used for DynamoDB) are too noisy on the 'DEBUG' level.
        "--log-level=INFO",
        # Generate a report that can be used in gitlab and displayed for CI information.
        "--junit-xml=report.xml",
        # Our testing folder.
        "tests",
    ]))
