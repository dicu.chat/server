token=$(tr -dc A-Za-z0-9 </dev/urandom | head -c $1 || echo Error..)
echo -e "New token:\n\t$token"
