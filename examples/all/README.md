# Deploy

This instruction assumes clean ubuntu20.04 or later.


### Requirements
You will need `docker` and `docker-compose`.
```bash
sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get -qqy install docker.io docker-compose
# Add yourself to the docker group
sudo usermod -aG docker $USER
# Now you need to re-login into the server, with whatever methods you want, example:
newgrp docker
```


### Preparation
Clone the repo and open this directory:
```bash
git clone --recurse-submodules https://gitlab.com/dicu.chat/server.git
cd server/examples/all
```


### Configuration
Create `.env` file from sample - and fill it by opening it with your editor:
```bash
cp .env.sample .env
```
Note: If you need a secure password/token, you can use the `generate_token.sh` script.  
  
Then use `Caddyfile.sample`, change domain address if needed:
```bash
cp Caddyfile.sample Caddyfile
```
Note: Skip this step if you don't need botsociety or anything web related (or you just don't have a domain/botsociety account).  


### Start
```bash
docker-compose up -d
```


### Update
To run latest images run
```bash
docker-compose pull
docker-compose up -d
```

