# Deployment
First clone ai repos:
```bash
git clone git@gitlab.com:dicu.chat/rasa.git
git clone git@gitlab.com:dicu.chat/gpt2.git
# or via https
git clone https://gitlab.com/dicu.chat/rasa.git
git clone https://gitlab.com/dicu.chat/gpt2.git
```
Start containers:
```bash
docker-compose up -d --build
```
