After you've started the server, please follow these steps to get your intelligent telegram bot 	:robot: :

1. Create a BotScoiety Account click [here!](https://app.botsociety.io/signup).

2. Build a Flow conversation chat by going to The Drafts section, then click new design, make sure to pick the legacy of Facebook messanger.

![botSociety chat](/assets/legacy.png)

3. Design the conversation flow you preferred.

![botSociety account](/assets/chat.png)


4. Get the BotSociety API. By going to your account, then click API, then under the API section Click generate new API.
![API](/assets/api.png)

5. Go to .env file fill `BOTSOCIETY_API_KEY` with your API key.

6. Get a webserver domain, either by booking a domain name for your website to include the bot or you can  try a free tool like [ngrok](https://dashboard.ngrok.com/get-started/setup), after download the ngrok in your OS run this command `./ngrok http 8080` to get the domain URL.

7. Go back to your botSociety account under the API section theres a Webooks section. Fill the URL with your domain add to it` /api/webhooks/botsociety` and then click create.

![webhook](/assets/webhook.png)



8. Go to [Telegram bridge repo](https://gitlab.com/dicu.chat/bridge-telegram) and follow the steps of running it.


9. Go to your draft design click on the build button, then select the "My own code base(API)" option then under the Export using webhooks section click send.

![build](/assets/build.png)

10. Go to your Telegram application and your bot channel and enjoy your conversation! :wink:


