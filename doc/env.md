# All .env Variables are explained as follows:


### ~~~~~~ General secrets/urls ~~~~~~

**SERVER_SECURITY_TOKEN**:
This token must be filled with a random string (secret). You can generate your secure token using [this tool](https://www.random.org/strings/). This token should also match the one in the bridge-telegram `SERVER_TOKEN=<token>` [here](https://gitlab.com/dicu.chat/bridge-telegram/-/blob/master/.env.example#L7) or any other bridge.

**CLOUD_TRANSLATION_API_KEY**:
This API key allows access to the Google Cloud Translation API. The API handles the translation of messages to different languages. To obtain the API key log into your Google account or click [here](https://console.cloud.google.com/). If you do not wish to enable this feature in the server, leave it blank. See ### ~~~~~~ Google credentials ~~~~~~




**DATABASE_URL**:
This is the database url. For development, fill it with `http://localhost:8000`, but for production, fill it with `http://container-name:8000`.

**RASA_URL**:
Set it to `http://localhost:5005` for development. For production set it to the `http://container-name:5005`.


**SERVER_HOST**:
You can leave it blank for development purposes. However, for production, the server host must be filled with the webserver domain. You can purchase a domain and enter the domain http here to generate links used for advanced functionality such as Google calendar intergreation.

**SERVER_PORT**: 
Keep it `8080` unless you have something else is running.


**AWS_ACCESS_KEY_ID**:
The AWS key id, but since we are using local database container, it can be any random string (but it has to be filled).

**AWS_SECRET_ACCESS_KEY**: 
The AWS key, but since we are using local database container, it can be any random string (but it has to be filled).


**BOTSOCIETY_API_KEY**:
Create a botsociety account [here](https://app.botsociety.io), then go to your account /API, generate a new API key, copy it, and then paste it here.


### ~~~~~~ General configurations ~~~~~~

**DEBUG**:
Enables some extra logging, keep it 'False' unless you know what you are doing.

**N_CORES**:
Represents the number of cores to run sanic on. *Not stable* for value bigger than 1.


**BOTSOCIETY_LANGUAGE**:
Type the language in which the botsociety flow is written in. Value can be `English` or `en`.

**DEFAULT_LANGUAGE**:
If you want to ask the user for his language, leave this option blank, otherwise, type the default language with which the bot will speak.


**DISABLE_COMMANDS**:
If you want to disable commands like '/language,/start' simply list them here (comma-separated).

**DISABLE_ACCESS_TOKENS:**
Access tokens are enabled by default, which means admin with superuser rights will have to create tokens and "invite" all other users.
Set to `True` to disable this.

**OWNER_HASH**:
When the bot is running type /id and bot will respond with the hash. If you fill this option, you will superuser access privileges in the bot. Leave it empty on the start.


**MAIN_BOTSOCIETY_FLOW**:
If you are only using one flow for production, keep it as 'main', if you want to combine multiple flows, this will decide which flow server treats as main (first, initial) one.


**TEST_ALLOW_CPU_LOAD**:
Makes calls into gpt2 during testing, keep it as `False` unless you know what you are doing.

**TEST_ALLOW_NETWORK_LOAD**:
Using files bigger than 1MB to test download functions, keep it as `False` unless you know what you are doing.


### ~~~~~~ Google credentials ~~~~~~

**CREDENTIALS**:
File (e.g. `credentials.json`) that contains google credentials file, which is needed for authorization for google calendar.

**SERVICE_CREDENTIALS**:
File (e.g. `service_credentials.json`) that contains service google credentials, which is needed for authorization if you want to use paid Google API for Voice API or Speech-To-Text.

### ~~~~~~ AI stuff ~~~~~~

**AI_URL**:`http://dicu-ai:8080`
**BLENDER_URL**:`http://dicu-model-blender:8080`
**DIALOGPT_URL**:`http://dicu-model-dialogpt:8080`

 
**AI_DEFAULT_MODEL**:
Default API to handle responses in AIState. Available options: gpt2, gpt3, blender, dialogpt.

**AI_INVALID_ANSWER_HANDLING**:
Enable smart error handling with GPT2 (invalid answer's propagated to the AI).


**OPENAI_API_KEY:** 
Leave this empty if you are not planning to use openai gpt3 model. You can get the API key from openai website.

**OPENAI_MAX_TOKENS**:
the recommanded response size for AI bot. Recomended `1120` token - 4 tokens is ~1 english language character.

**OPENAI_MODEL**: 
You can pick one of the openAI models either davinci or davinci-instruct-beta.


**AI_ENABLE_PROMPT_TRANSLATION**:
Set to 'True' to try translating user prompts to AI. Note, you will have to configure Google Translation API token for this to work.

**AI_ENABLE_RESPONSE_TRANSLATION**:
Set to 'True' to try translating ai responses. Note, you will have to configure Google Translation API token for this to work.

**AI_PRIMARY_LANGUAGE**: `en`

**AI_SUPPORTED_LANGUAGES**:
A list of supported languages (lang codes), separated by comma, for example: 'en,de,ru'. If you are using dicu.chat self-hosted AI models, set it to `en`.

**DEFAULT_FREE_TEXT_AND_SPEECH_API**:
Unless you have google credentials, keep it as `False`.

