# You should run flake before building container,
# the last thing you want is to build a testing
# image and see trailing space on line 42
# For this to work you have to create virtualenv in ".venv"
# and install "test-requirements.txt"
source .venv/bin/activate && ./test_analyze.sh || exit $?

# Buildkit is a Docker 18.06+ feature to speed up image building.
DOCKER_BUILDKIT=1 docker build -t dicu-server-test --target=test .
# Running test image
docker run --rm -it \
    --env-file=.env.test \
    dicu-server-test
